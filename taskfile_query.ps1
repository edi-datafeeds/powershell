﻿
#* FileName: taskfile_query.ps1
#*=============================================================================
#* Script Name: [taskfile query]
#* Created: [15/08/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: n/a
# Created: [15/08/2013]
# Author: Damar Johnson
# Arguments: Embedded
# =============================================================================
# Purpose: Query task file for all .vbs wrapped running/non-running programs within the OP's
# =============================================================================

### 89 folders 
$Opsdir=gci -Recurse '\\192.168.12.4\ops\AUTO\Tasks\' | ?{
    $_.mode -like '*d*'} | ?{
    $_.name -notlike '*FTP*'}

### 123 folders
$Opsdirb=gci -Recurse '\\192.168.12.4\ops\AUTO\Tasks\' | ?{
    $_.mode -like '*d*'}

## 794 files## 
$opsfiles=gci $Opsdir.fullname | ?{
    $_.Name -notlike 'FTP'}

## 330 files
$opsfilesb=gci $Opsdir.fullname 

## 3445 files containing exe commands found
gci $opsfiles.fullname -Recurse | sls -Pattern ".exe"

## 12420 files containing vbs commands found
gci $opsfiles.fullname -Recurse | sls -Pattern ".vbs"

## 15825
$files=gci $opsfiles.fullname -Recurse | sls -Pattern ".vbs",".exe"

$files | Out-File c:\tasksearch.txt
$sfiles = gc c:\tasksearch.txt

$result= $files[1] -match 'o:*.vbs'

$regex="o:\\\D+..\.vbs"
$vbs='o:\auto\scripts\vbs\generic\ftpCheck.vbs'

$values=@()
foreach ($x in $files){
    if ($x -match $regex){
    $values+=$Matches | %{
        $_.values} | %{$_.tolower()};Write-Host 'Success'
        }
}

$valuesA=($values | Sort-Object | Get-Unique)

$valuesC=$valuesA[0..43]+$valuesA[45..($valuesA.Count)]

$substrA="[a-z]+:\\\w+\\.+\.exe"
$substrB="[a-z]+:\\\w+\\.+\.jar"
$substrC="[a-z]+:\\\w+\\*"

$exetest=gc $valuesC 

### test
$teststr='J:\java\Prog\Y.laifa\NB6\J2SE\SendFile\dist\SendFile.jar'

#------------------------------------------ EXE/JAR pull ------------------------------------------#
$grab= gc $valuesC

$jar=$null
$exe=$null

$grab= gc $valuesC
$exe=@()
$jar=@()
$list=@()

foreach ($y in $grab){
    if ($y -match $substrA){
    $exe+=$Matches.Values;Write-Host 'EXE Looking good Jack!'
    }
    elseif ($y -match $substrB){
        $jar+=$Matches.Values;Write-Host 'JAR Looking good Jack!'
        }
    else{ write-host 'Fail'
    }
}

$jarA=$jar | %{
    $_.tolower()} | Sort-Object | Get-Unique

$exeA=$exe | %{
    $_.tolower()} | Sort-Object | Get-Unique

for ($i=0; $i -le $exeA.Count; $i++){
        $list+="" | Select-Object -Property @{
            label='exe';expression={
                $exeA[$i]}
                    },@{
                        label='jar';expression={$jarA[$i]}}
            }
#$list | Select-Object -Property exe,jar | ft | ConvertTo-Csv -NoTypeInformation 
$list | Select-Object -Property exe,jar | epcsv -NoTypeInformation C:\RAW\ops.csv

#$jar=@()
#foreach ($y in $grab){
    #switch($y){
    #{$_ -match $substrB}{$jar+=$matches.Values; Write-Host 'Success'}
    #{$y -match $substrB}{$jar+=$Matches.Values;Write-Host 'Looking good Jack!'}
    #default {Write-Host 'Nothing'}
    #}
    #}