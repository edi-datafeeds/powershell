' DJ - 26/02/2013
' This parses a directory, including sub directories and searching for files newer than 1 hour
' Files are then copied across to output path

Option explicit

dim i,objShell, src, dest,numArgs

src = WScript.Arguments.Item(0)
dest = WScript.Arguments.Item(1)

Set objShell = CreateObject("Wscript.Shell")


i=objShell.Run("C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noexit O:\AUTO\Scripts\Powershell\copyhourly.ps1 " & src & " " & dest,1,true)

if i = 1 then
	msgbox "Failed to format files"
else
	wscript.quit(0)
end if 
