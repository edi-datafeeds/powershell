param(
    [parameter(Mandatory=$true)][string] $dirpath )
#$dirpath = "d:\ivantest\"
 
function CheckSwiftFile {
	param(
		[parameter(Mandatory=$true)][string] $fpath )
	
	$found = $false #set this to true if there is possibility of error on next line
	#$fpath = "d:\ivantest\edi_20170308_corrupted.txt"
	#$fpath = "d:\ivantest\edi_20170308_corrected.txt"
	$lcount = 0
	$flcount = (Get-Content -Path "$fpath" -ReadCount 1).Count #count number of lines in file
	
	Get-Content "$fpath" | foreach-object{
		if ( $found ) { # we only go in here because previous line ended with -}$
			$found = $false       # unset found in case this line is ok
			if (!( $_.StartsWith("{")) ) {  # check if first char is NOT { which is an error
				$Emsg = "Corruption near line $lcount in $fpath. `n`rIllegal char following -}$ `n`r{ should begin next line`n`rPlease re run process"
				(get-date).ToString() + " $fpath is corrupt" >> log
				#[System.Reflection.Assembly]::LoadWithPartialName(“System.Windows.Forms”)
				[Windows.Forms.MessageBox]::Show("$Emsg", "15022 File Check", "OK", "Error")
			   
				exit
			}
		}
		$lcount++  #keep counter so we know what line we are on
		if ( $_ | Select-String "-}\`$" ) {  # note \` escaping required for $ char
			$found = $true # set found so we can check beginning of next line for }
			{continue} # go to next iteration of foreach
		}
		if ( $flcount -eq $lcount ) {  #we only do this on last line of file
			if ( $_.ToString().Length -eq 3 -And  $_ -eq "-}$" ) {
				echo "last line is -}$ and length is 3"
			} else {
				$emsg = "Corruption on last line $lcount in $fpath. Please re run process"
				(get-date).ToString() + " $fpath is corrupt" >> log
				#[System.Reflection.Assembly]::LoadWithPartialName(“System.Windows.Forms”)
				[Windows.Forms.MessageBox]::Show("$Emsg", "15022 File Check", "OK", "Error")
				#(get-date).ToString() + " $fpath is corrupt" >> log
				exit
			}
		}
	}
	(get-date).ToString() + " $fpath is OK" >> log
}
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
Get-ChildItem $dirpath -Filter "*.txt" |
Where-Object { $_.Attributes -ne "Directory" } |
ForEach-Object {
    #echo $_.BaseName
    echo $_.FullName  #seb
    CheckSwiftFile -fpath $_.FullName
    echo $?
}