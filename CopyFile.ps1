# Generic script t copy contents of directory to destination
# Source and destination supplied via CL arguments

#test directory
#$path='C:\users\d.johnson\Desktop\source\*.*'
#$destination='C:\Users\d.johnson\Desktop\dest\'

#working directories
$path=$args[0]

$goods=gci $path -filter *.csv
$destination=$args[1]

# Execute copy command
cd $path; cpi $goods $destination