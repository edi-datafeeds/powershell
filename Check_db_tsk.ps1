﻿#* FileName: SLA_check.ps1
#*=============================================================================
#* Script Name: [Check_db_tsk]
#* Created: [20/11/2014]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose:  28/08/2013 - Periodically check DB for feed SLA/ETA state
#*          *25/11/2014 - Validate DB load in taksfile before proceeding
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [28/08/2013]
#* Time: [12:00]
#* Issue: 
#* Solution: 
#  Date: [25/11/2014]
#* Time: [09:00]
#* Issue: Taskfile running generation without correct load
#* Solution: Validate load before progressing
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Determine which incrment to apply
#
#
# =============================================================================

## Run step below in testing
#powershell.exe O:\AUTO\Scripts\Powershell\test\Check_db_tsk.ps1 -h HAT_MS_Sqlsvr1 -dbs wca2 -tbl dbo.tbl_opslog -alert y -logfile test

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$false,
ParametersetName='DataBase'
)]
#[switch]

[Parameter(
mandatory=$true,Position=0
)]
[string]$h='MyDev', #Server to Check

[Parameter(
mandatory=$true,Position=1
)]
[string]$dbs='smf4', #Table to Check

[Parameter(
mandatory=$true,Position=2
)]
[string]$tbl='tbl_opslog', #Database to Check

[Parameter(
mandatory=$false#Position=3
)]
[string]$field, #Field Check *Not implemented

[parameter(
mandatory=$false
)]
[string]$i, #Incremental

[parameter(Mandatory=$false)]
[string]$logfile='OPS-test1', #Log file to create or append to

[parameter(
mandatory=$false
)]
[string]$alert # Message box popup on error

)

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

#------------------------- Check Argument -------------------------#
    If ((!($h))){
            Write-Host 'No DB Server Specified!'
                    msgbox 'No DB Server Specified!'
    }
    ElseIf ((!($dbs))){
            Write-Host No Database specified!
                    msgbox 'No Database Specified!'
                    }
    ElseIf ((!($tbl))){
            Write-Host No Table specified!
                    msgbox 'No Table Specified!'
    }
    Else{
    }
#------------------------- Set alert function -------------------------#


#$time=(get-date).hour
$time=(get-date).ToShortTimeString()
$hmtime=$time.Split(':')
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'
$global:class='ok'
$y=(get-date).year

### Logs step and output of entire script
#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

$global:logvalue=@()

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       #switch ($logfile) {
       switch ($h) {
                #------------------------- *OLD STREET* WCA incrementals values -------------------------#
                'HAT_MY_Diesel'
                   {switch ($dbs){
                        smf4{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '12:00' -and $_ -le '17:59'} {$global:inc='_1'}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_2'}
                                }
                            }
                        wca{switch ($time) {
                            #{switch ($hmtime[0]) {
                                { $_ -ge '13:20' -and $_ -le '17:19'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                #{$_ -ge '18:35' -and $_ -le '08:20'} {;$global:inc='_3';}
                                {$_ -ge '17:20' -and $_ -le '23:59'} {;$global:inc='_3';}
                                #{$_ -ge '00:00' -and $_ -le '08:19'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wca2{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '15:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wol{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '16:30' -and $_ -le '00:00'} {;$global:inc='_3';}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        }
                    }
                #------------------------- *OLD STREET* MSSQL WCA incrementals values -------------------------#
                'HAT_MS_Sqlsvr1'
                   {switch ($dbs){
                        smf4{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '12:00' -and $_ -le '17:59'} {$global:inc='_1'}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_2'}
                                }
                            }
                        wca{switch ($time) {
                            #{switch ($hmtime[0]) {
                                { $_ -ge '13:20' -and $_ -le '17:19'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                #{$_ -ge '18:35' -and $_ -le '08:20'} {;$global:inc='_3';}
                                {$_ -ge '17:20' -and $_ -le '23:59'} {;$global:inc='_3';}
                                #{$_ -ge '00:00' -and $_ -le '08:19'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wca2{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '15:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wol{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '16:30' -and $_ -le '00:00'} {;$global:inc='_3';}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        }
                    }
                #------------------------- *HORSHAM* WCA incrementals values -------------------------#
                'HSM_MY_Diesel'
                    {switch ($dbs){
                        smf4{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '12:00' -and $_ -le '17:59'} {$global:inc='_1'}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_2'}
                                }
                            }
                        wca{switch ($time) {
                            #{switch ($hmtime[0]) {
                                { $_ -ge '13:25' -and $_ -le '17:19'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                #{$_ -ge '18:35' -and $_ -le '08:20'} {;$global:inc='_3';}
                                {$_ -ge '17:20' -and $_ -le '23:59'} {;$global:inc='_3';}
                                #{$_ -ge '00:00' -and $_ -le '08:19'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wca2{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '15:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wol{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '16:30' -and $_ -le '00:00'} {;$global:inc='_3';}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'iCloud_MySql'
                    {switch ($dbs){
                        smf4{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '12:35' -and $_ -le '17:59'} {$global:inc='_1'}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_2'}
                                }
                            }
                        wca{switch ($time) {
                            #{switch ($hmtime[0]) {
                                { $_ -ge '13:30' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '15:00' -and $_ -le '18:19'} {$global:inc='_2'}
                                #{$_ -ge '18:20' -and $_ -le '08:00'} {;$global:inc='_3';}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wca2{switch ($time) {
                            #{switch ($hmtime[0]) {
                                { $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '15:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wol{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '16:30' -and $_ -le '00:00'} {;$global:inc='_3';}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'Liquid_MySql'
                    {switch ($dbs){
                        smf4{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '12:35' -and $_ -le '17:59'} {$global:inc='_1'}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_2'}
                                }
                            }
                        wca{switch ($time) {
                            #{switch ($hmtime[0]) {
                                { $_ -ge '13:30' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '15:00' -and $_ -le '18:19'} {$global:inc='_2'}
                                #{$_ -ge '18:20' -and $_ -le '08:00'} {;$global:inc='_3';}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wca2{switch ($time) {
                            #{switch ($hmtime[0]) {
                                { $_ -ge '13:30' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '15:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wol{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '16:30' -and $_ -le '00:00'} {;$global:inc='_3';}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'MyDev'
                    {switch ($dbs){
                        smf4{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '12:35' -and $_ -le '17:59'} {$global:inc='_1'}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_2'}
                                }
                            }
                        wca{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '15:00' -and $_ -le '18:19'} {$global:inc='_2'}
                                {$_ -ge '18:20' -and $_ -le '08:00'} {;$global:inc='_3';}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wca2{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                { $_ -ge '15:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        wol{switch ($time) {
                            #{switch ($hmtime[0]) {
                                #{ $_ -ge '13:25' -and $_ -le '17:29'} {$global:inc='_2'}
                                #{ $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                                {$_ -ge '16:30' -and $_ -le '00:00'} {;$global:inc='_3';}
                                #{$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                                default {$global:inc='_1'}
                                }
                            }
                        }
                    }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        #{ $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        #{$_ -ge '14:45' -and $_ -le '17:00'} {$global:inc='_2'}
                        #{$_ -ge '17:30' -and $_ -le '23:59'} {$global:inc='_3'}
                        { $_ -ge '14:35' -and $_ -le '18:29'} {$global:inc='_2'}
                        {$_ -ge '18:35' -and $_ -le '23:59'} {;$global:inc='_3';}
                        #default {Write-host 'default inc1';$global:inc='_1'}
                        default {$global:inc='_1'}
                        }
                    }
                     
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       #default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       default {$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
                }
#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        write-output "$d $displaytxt" #;exit
        #$hold.count
        $global:logvalue
    }

#################################################################
### Set Connection to MySQL DB and execute query ###

## Set MySQL Connectiion
[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection

#------------------------- Secure NET FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith('DOTNET')}
#$c=$b.split("`t")
#$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
#$NETpass=$d
#$NETcred=New-Object System.Management.Automation.PsCredential “channel7“,$NETpass

#------------------------- NET FTP Credentials -------------------------#
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith('HAT_MS_Sqlsvr1')}
#$c=$b.split("`t")
#$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
#$NETpass=$d
#$NETcred=New-Object System.Management.Automation.PsCredential “channel7“,$NETpass

#------------------------- COM FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith("$s")}
$b=gc '\\192.168.2.163\ops\auto\Configs\DBServers.cfg' | ?{$_.startswith("$h")}
$c=$b.split("`t")
$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
$COMpass=$d
#$COMcred=New-Object System.Management.Automation.PsCredential $d[2],$d[3]
$server=$c[4]
$id=$c[2]
$pwd=$c[3]
$stype=$c[1]

if($h -eq 'Liquid_MySql'){$id='damar'
    $pwd='p1n3'
}
else{}
#$server='192.168.12.160'

#$server='192.168.12.109'

#$server='67.227.167.146'
#$db='OpsSLA'
#$db='smf4'
#$id='sa'
#$id='auto_ops'
#$pwd='AK47ops;'
#$pwd='K376:lcnb'

#------------------------- MySQL DB connections -------------------------#

## Default
$myconnection.Connectionstring="server=$server;database='';Persist Security Info=false;user id=$id;pwd=$pwd"

#------------------------- MSSQL DB connections -------------------------#

$sqlcmd= Get-Command sqlcmd

#------------------------- DB Credentials -------------------------#
#$b=gc '\\192.168.2.163\ops\auto\Configs\DBServers.cfg' | ?{$_.startswith("$h")}
#set-alias sqlcmd "$env:ProgramFiles\Microsoft SQL Server\90\Tools\binn\SQLCMD.exe"


#------------------------- Test DB connections -------------------------#
## OTRS
#$myconnection.Connectionstring = "server=192.168.12.202;database=test;Persist Security Info=false;user id=otrs;pwd=hot"
## Diesel
#$myconnection.Connectionstring="server=$server;database=$db;Persist Security Info=false;user id=$id;pwd=$pwd"
## LiquidWeb
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"
## iCLOUD
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"

#$myconnection.open()

#Set query command
#$Command= New-Object MySql.Data.MySqlClient.MySqlCommand
#$Command.Connection=$myconnection

#------------------------- Test Queries -------------------------#

$show_all= "SHOW TABLES"
#$SMF="SELECT * FROM smf4.tbl_opslog order by acttime desc;"
$SMF="SELECT Feeddate FROM smf4.tbl_opslog order by acttime asc;"
$WCA="SELECT feeddate FROM wca.tbl_opslog order by acttime asc;"
$test="SELECT feeddate,seq FROM wca.tbl_opslog order by acttime asc;"
#$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"

$command=$myconnection.CreateCommand()
#$command.CommandText=$join
#$command.CommandText=$SMF
#$command.CommandText=$WCA
#$command.CommandText=$test

#$command.CommandText=$run
#------------------------- Output Results -------------------------#

#$reader=$Command.ExecuteReader()

$dataset=New-Object System.Data.DataSet
$dataset.Tables[0]


#MySQL DB result collection
$collection=@()

#MsSQL DB result collection
$ms_collection=@()


switch($stype){
    'mssql'{
        switch($dbs){
            'wca'{
                #$run="`"select feeddate,seq from $dbs.dbo.$tbl`"";break
                $run="`"select feeddate,seq from $dbs.$tbl`"";break
                }
            'wca2'{
                #$run="`"select feeddate,seq from $dbs.dbo.$tbl`"";
                $run="`"select feeddate,seq from $dbs.$tbl`"";
                }
            'smf4'{
                #$run="`"select feeddate from $dbs.dbo.$tbl order by feeddate asc`"";
                $run="`"select feeddate from $dbs.$tbl order by feeddate asc`"";
                }
            'wol'{
                #$run="`"select feeddate,seq from $dbs.dbo.$tbl`"";
                $run="`"select feeddate,seq from $dbs.$tbl`"";
                }
            'wfi'{
                #$run="`"select feeddate,seq from $dbs.dbo.$tbl`"";
                $run="`"select feeddate,seq from $dbs.$tbl`"";
                }
            default{
                #$run='`"select feeddate,seq from $db.dbo.$tbl`"';
                $run="`"select feeddate,seq from $dbs.$tbl`"";
                }
            }
        }
    'mysql'{
        switch($dbs){
            'wca'{$run="SELECT feeddate,seq FROM $dbs.$tbl order by acttime asc;"
                $myconnection.open()
                $command.CommandText=$run;$reader=$Command.ExecuteReader()
                while ($reader.Read()) {
                    for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                      $collection+=$reader.GetValue($ii).ToString().Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
                            }
                    }$myconnection.Close()
                           switch($tbl){
                                default{$run="SELECT feeddate,seq FROM $dbs.$tbl order by acttime asc";
                                    $feedate=$collection[-3];break;
                                        switch($field){
                                            default{'No fields selected';break;}
                                            }
                                    }
                            }
                        }
            'wca2'{$run="SELECT feeddate,seq FROM $dbs.$tbl order by acttime asc;"
                $myconnection.open()
                $command.CommandText=$run;$reader=$Command.ExecuteReader()
                while ($reader.Read()) {
                    for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                      $collection+=$reader.GetValue($ii).ToString().Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
                            }
                    }$myconnection.Close()
                        switch($tbl){
                            default{$run="SELECT feeddate,seq FROM $dbs.$tbl order by acttime asc";
                                $feedate=$collection[-3];break;
                                    switch($field){
                                        'comment'{write-host 'Comment field'$run="SELECT $field FROM $dbs.$tbl order by acttime asc";break;exit
                                            }
                                        default{'No fields selected';break;}
                                        }
                                }
                        }
                    }
            'smf4'{$run="SELECT feeddate FROM $dbs.$tbl order by FeedDate asc;";
                $myconnection.open()
                $command.CommandText=$run;$reader=$Command.ExecuteReader()
                while ($reader.Read()) {
                    for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                      $collection+=$reader.GetValue($ii).ToString().Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
                            }
                    }$myconnection.Close()
                        $feedate=$collection[-2];$seq=$collection[-1]
                            switch($collection[-1]){
                                '12:00:00'{$feed_inc='12:00:00';break;}
                                '18:00:00'{$feed_inc='18:00:00';break;}
                                    }
                    }
            #'wol'{$run="SELECT acttime FROM $dbs.$tbl where acttime like '2014%' order by acttime asc;";
            'wol'{$run="SELECT acttime FROM $dbs.$tbl where acttime like",(get-date).year,'order by acttime asc;';
                $myconnection.open()
                $command.CommandText=$run;$reader=$Command.ExecuteReader()
                while ($reader.Read()) {
                    for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                      $collection+=$reader.GetValue($ii).ToString().Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
                            }
                    }$myconnection.Close()
                        switch ($tbl){
                            'cab'{$run="SELECT acttime FROM $dbs.$tbl acttime like",(get-date).year,'order by acttime asc;';
                                $feedate=$collection[-2];break;
                                }
                            'sec'{$run="SELECT acttime FROM $dbs.$tbl where acttime like",(get-date).year,'order by acttime asc;';
                                $feedate=$collection[-2];break;
                                }
                            'dir'{$run="SELECT acttime FROM $dbs.$tbl where acttime like",(get-date).year,'order by acttime asc;';
                                $feedate=$collection[-2];break;
                                }
                            }
                    }
            'wfi'{switch($tbl){
                    default{$run="SELECT feeddate,seq FROM $dbs.$tbl order by acttime asc";
                    $myconnection.open()
                    $command.CommandText=$run;$reader=$Command.ExecuteReader()
                    while ($reader.Read()) {
                        for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                          $collection+=$reader.GetValue($ii).ToString().Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
                                }
                    }$myconnection.Close()
                        $feedate=$collection[-3];break;exit
                            switch($field){
                                'comment'{$run="SELECT $field FROM $dbs.$tbl order by acttime asc";break;
                                    }
                                default{'No fields selected';break;}
                                }
                            }
                        }
                    }
            default {$run="SELECT acttime FROM $dbs.$tbl where acttime like '2014%' order by acttime asc;";
                    $myconnection.open()
                    $command.CommandText=$run;$reader=$Command.ExecuteReader()
                    while ($reader.Read()) {
                        for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                          $collection+=$reader.GetValue($ii).ToString().Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
                                }
                    }$myconnection.Close()
                    break;exit
                }
        }
    }
}
$today=(get-date -Format g).Split(' ')
Get-Increment

#start-sleep -Seconds 30
## MSSQL 
#switch ($dbs){
    #'wca'{
        #$ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
        #$ms_collection+=($ms_collection).split(' ')
        #$ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}
        #$feedate=$ms_collection[-3];break}
    #'wca2'{
        #$ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
        #$ms_collection+=($ms_collection).split(' ')
        #$ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}    
        #$feedate=$ms_collection[-3];break}
    #'smf4'{
        #$ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
        #$ms_collection+=($ms_collection).split(' ')
        #$ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}
        #$feedate=$ms_collection[-2];$seq=$ms_collection[-1]
        #switch($ms_collection[-1]){
            #'12:00:00'{$feed_inc='12:00:00';break;}
            #'18:00:00'{$feed_inc='18:00:00';break;}
                #}
            #}
    #'wfi'{
        #$ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
        #$ms_collection+=($ms_collection).split(' ')
        #$ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}    
        #$feedate=$ms_collection[-3];break}
    #'wol'{
        #$ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
        #$ms_collection+=($ms_collection).split(' ')
        #$ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}
        #$feedate=$ms_collection[-3];break}
#}
switch($stype){
    'mssql'{$collection=$ms_collection;$today=(get-date -Format u).Split(' ')
        switch ($dbs){
            'wca'{
                $ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
                $ms_collection+=($ms_collection).split(' ')
                $ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}
                $feedate=$ms_collection[-3];break}
            'wca2'{
                $ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
                $ms_collection+=($ms_collection).split(' ')
                $ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}    
                $feedate=$ms_collection[-3];break}
            'smf4'{
                $ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
                $ms_collection+=($ms_collection).split(' ')
                $ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}
                $feedate=$ms_collection[-2];$seq=$ms_collection[-1]
                switch($ms_collection[-1]){
                    '12:00:00'{$feed_inc='12:00:00';break;}
                    '18:00:00'{$feed_inc='18:00:00';break;}
                        }
                    }
            'wfi'{
                $ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";$cmdOutput = sqlcmd 2>&1
                $ms_collection+=($ms_collection).split(' ')
                $ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}    
                $feedate=$ms_collection[-3];break}
            'wol'{
                $ms_collection=&cmd /c "$sqlcmd -U$id -P$pwd -S $server -Q $run";
                $ms_collection+=($ms_collection).split(' ')
                $ms_collection=$ms_collection | ?{$_ -notlike "$null"} | ?{$_ -notlike "*[a-z]*"} | ?{$_ -notlike '*(*'}
                $feedate=$ms_collection[-3];break}
                    }
                    switch ($feedate){
                        $today[0] {
                            switch($dbs){
                                'wca'{#switch($x){
                                        #{$ms_collection[-1] -eq $feed_inc}{#$testi=($collection[1]);
                                            write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:"$feedate "Seq:"$ms_collection[-1];
                                            LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$ms_collection[-1];
                                                $global:class='Validate';Get-Increment;Set-Logpath;break
                                               } 
                                'wca2'{#switch($x){
                                        #{$collection[-1] -eq $feed_inc}{#$testi=($collection[1]);
                                            write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:"$feedate "Seq:"$ms_collection[-1];
                                            LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$ms_collection[-1];
                                                $global:class='Validate';Get-Increment;Set-Logpath;break
                                            }
                                'smf4'{#switch($x){
                                    #{$collection[-1] -eq $feed_inc}{#$testi=($collection[1]);
                                        write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:"$feedate "Seq:"$ms_collection[-1];
                                        LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$ms_collection[-1];
                                            $global:class='Validate';Get-Increment;Set-Logpath;break
                                            }
                                'wfi'{#switch($x){
                                    #{$collection[-1] -eq $feed_inc}{#$testi=($collection[1]);
                                        write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:"$feedate "Seq:"$ms_collection[-1];
                                        LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$ms_collection[-1];
                                            $global:class='Validate';Get-Increment;Set-Logpath;break
                                            }
                                'wol'{#switch($x){
                                    #{$collection[-1] -eq $feed_inc}{#$testi=($collection[1]);
                                        write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:"$feedate "Seq:"$ms_collection[-1];
                                        LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$ms_collection[-1];
                                            $global:class='Validate';Get-Increment;Set-Logpath;break
                                            }
                                default {$atime=$feedate;
                                            write-host "DB OK - $h/$dbs/$tbl Database upto date - DB FeedDate:$atime",Seq:,$ms_collection[-1]
                                            LogThis "DB OK - $h/$dbs/$tbl Database upto date - DB FeedDate:$atime",Seq:,$ms_collection[-1];
                                                Set-Logpath;exit 0
                                                }
                                            }
                                    }
                        default {write-host "Error! $h/$dbs Database is not Upto date - DB Feeddate:"$feedate "Seq:"$ms_collection[-1]
                                LogThis "Error! $h/$dbs Database is not Upto date - DB Feeddate:$feedate",Seq:,$ms_collection[-1]
                                    Msgbox "Error! $h/$dbs Database is not Upto date - DB Feeddate:$feedate",Seq:,$ms_collection[-1]; 
                                    $global:class='failed';Get-Increment;Set-Logpath;break
                                    }
                            }
                        }
    'mysql'{
        switch ($feedate){
            $today[0] {switch($dbs){
                        'smf4'{switch($x){
                                {$collection[-1] -eq $feed_inc}{#$testi=($collection[1]);
                                    write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:"$feedate "Seq:"$collection[-1];
                                    LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$collection[-1];
                                        $global:class='Validate';Get-Increment;Set-Logpath;break
                                            }
                                default{
                                    write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1]
                                    LogThis "Errror! Incorrect Increment: DB Feeddate:$feedate",Increment:,$collection[-1]
                                    Msgbox "Errror! Incorrect Increment: DB Feeddate:$feedate",Increment:,$collection[-1];
                                        $global:class='failed';Get-Increment;Set-Logpath;exit 2
                                        }
                                    }
                                }
                        'wca'{switch($collection[-1]){
                                {$_ -eq $Global:inc[1]}{
                                    write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]
                                    LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$collection[-1];
                                        $global:class='Validate';Set-Logpath;break
                                            }
                                 default{
                                    write-host "Errror! Incorrect Increment: DB Feeddate:$feedate",Increment:,$collection[-1]
                                    logthis -displaytxt "Errror! Incorrect Increment: DB Feeddate:$feedate",Increment:,$collection[-1] 
                                    msgbox "Errror! Incorrect Increment: DB Feeddate:$feedate",Increment:,$collection[-1];
                                        $global:class='failed';Get-Increment;Set-Logpath;exit 2
                                        }
                                    }
                                }
                        'wca2'{switch($collection[-1]){
                                {$_ -eq $Global:inc[1]}{
                                    write-host "DB OK - $h/$dbs Database upto date - DB FeedDate: $feedate",Seq:,$collection[-1]
                                    LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$collection[-1];
                                        $global:class='Validate';Set-Logpath;break
                                            }
                                default{
                                    write-host "Errror! Incorrect Increment:DB Feeddate:$feedate",Increment:,$collection[-1]
                                    LogThis "Errror! Incorrect Increment:" "DB Feeddate:$feedate",Increment:,$collection[-1]
                                    Msgbox "Errror! Incorrect Increment:DB Feeddate:$feedate",Increment:,$collection[-1];
                                        #$global:class='failed';Get-Increment;Set-Logpath;exit 2}
                                        $global:class='failed';Get-Increment;Set-Logpath;break
                                        }
                                }
                            }
                        'wfi'{switch($collection[-1]){
                                {$_ -eq $Global:inc[1]}{
                                    write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:$feedate",Seq:,$collection[-1];
                                    LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate,Seq:,$collection[-1];
                                        Set-Logpath;exit 0
                                            }
                                 default{
                                    write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1]
                                    LogThis "Errror! Incorrect Increment:DB Feeddate:$feedate",Increment:,$collection[-1]
                                    Msgbox "Errror! Incorrect Increment:DB Feeddate:$feedate",Increment:,$collection[-1] ;
                                        $global:class='failed';Get-Increment;Set-Logpath;exit 2
                                        }
                                    }
                                }
                        default {$atime=$feedate;
                                write-host "DB OK - $h/$dbs/$tbl Database upto date - DB FeedDate:$atime",Seq:,$collection[-1]
                                LogThis "DB OK - $h/$dbs/$tbl Database upto date - DB FeedDate:$atime",Seq:,$collection[-1];
                                    Set-Logpath;exit 0}
                                #Msgbox "DB OK - $h/$dbs/$tbl Database upto date - DB FeedDate:" $atime "Seq:"$collection[-1]; exit 0}

                        }
                    }

            default {write-host "Error! $h/$dbs Database is not Upto date - DB Feeddate:"$feedate "Seq:"$collection[-1]
                        LogThis "Error! $h/$dbs Database is not Upto date - DB Feeddate:$feedate",Seq:,$collection[-1]
                         Msgbox "Error! $h/$dbs Database is not Upto date - DB Feeddate:$feedate",Seq:,$collection[-1]; 
                            #$global:class='failed';Get-Increment;Set-Logpath;exit 2}
                            $global:class='failed';Get-Increment;Set-Logpath;break
                            }

            }
    }    
}
#------------------------- Null Results -------------------------#
$collection=$null
#$dbs=$null
#$ii=$null
#$pwd=$null
#$run
#$feedate=$null
#$feed_inc=$null

 exit $lastexitcode