$labelFont1 = New-Object System.Drawing.Font("Lucida Sans", 11, [System.Drawing.FontStyle]::Regular)
$Icon = [system.drawing.icon]::ExtractAssociatedIcon($PSHOME + "\powershell.exe")

[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

$PopupBox = New-Object System.Windows.Forms.Form
$PopupBox.Size = New-Object System.Drawing.Size(250,130)
$PopupBox.Text = "Prices Alert"
$PopupBox.StartPosition = "CenterScreen"
$PopupBox.MinimizeBox = $False
$PopupBox.MaximizeBox = $False
$PopupBox.TopMost = $True
$PopupBox.Icon = $Icon
$PopupBox.FormBorderStyle = 'Fixed3D'

$AlertLabel = New-Object System.Windows.Forms.Label
$AlertLabel.Text = "ALERT: Please check prices"
$AlertLabel.Location = New-Object System.Drawing.Size(10,15)
$AlertLabel.Size = New-Object System.Drawing.Size(250,20)
$AlertLabel.Font = $labelFont1
$PopupBox.Controls.Add($AlertLabel)

$OKButton = New-Object System.Windows.Forms.Button
$OKButton.Location = New-Object System.Drawing.Size(75,45)
$OKButton.Size = New-Object System.Drawing.Size(80,30)
$OKButton.Text = "OK"
$OKButton.Font = $labelFont1
$OKButton.Add_Click({
	[void] $PopupBox.Close()
})
$PopupBox.Controls.Add($OKButton)

#Alert every half an hour
do{
	$time = Get-Date -Format "mm"
	if($time -eq "15" -or $time -eq "45")
	{
		$PopupBox.Add_Shown({$PopupBox.Activate()})
		[void] $PopupBox.ShowDialog()
		Start-Sleep 60
	}
	Start-Sleep 5
	$x = 1
}until($x -eq 0)