﻿
################################################### FTP script currently in use ###################################################

$FTP_dir=gci '\\192.168.12.4\ops\AUTO\Tasks\' -Directory FTP -Recurse

$FTP_file=gci -Recurse (%{$FTP_dir.fullname}) | ?{
        $_.mode -eq '-a---' -and $_.Extension -eq '.tsk'
        }

$FTP_fileitem=gc (%{$FTP_File.fullname})

$New_FTP=%{
$FTP_fileitem | ?{
    $_.Contains('o:\auto\scripts\vbs\generic\ftps.vbs')
    }
        } #409 files as of ---02/08/13---

$FTP=%{$New_FTP | ?{$_ -notlike '#*'} | %{$_ -replace '\\','/'} | %{$_.tolower()}}

$New_FTP > 'C:\newftp_0208.csv' #409 files as of ---02/08/13--- ; Contains (#FTP) and (check steps)
$FTP_0108=gc 'C:\newftp_0208.csv' #382 files as of ---02/08/13---

$Current_FTP=%{
    $New_FTP | ?{$_.startswith('FTP') # incomplete listing ommits check steps. Check steps needed for end result  ## DO NOT USE
    }
}

$FTPs=$FTP_fileitem | ?{
    $_.Contains('o:\auto\scripts\vbs\generic\ftps.vbs')
}        
################################################### FTP scripts no longer in use ###################################################

$legacy=$FTP_fileitem | ?{
    $_.contains('o:\auto\scripts\vbs\generic\ftp.vb')
    } 

$Dead=$FTP_fileitem | ?{
    $_.contains('o:\auto\scripts\vbs\generic\pushftp.vbs')
    }

$check=%{
    $New_FTP | ?{$_.startswith('Check')
    }
}

################################################### FTP Child Item ###################################################

$cur_parent= (
    $FTP | Split-Path -NoQualifier | Split-Path -Parent
        ) -split " " | ?{
            $_ -ne "\auto\scripts\vbs\generic\ftps.vbs" 
                } # 763 records as of ---01/08/13---

$cur_child=(-split (
    Split-Path $FTP -Leaf | tee -Variable child) | %{
        $_ -split ".",0, "simplematch"
        } | %{
            $_ -split '(:)'
                } # 3494 records as of ---01/08/13---
    )

$Filename=@(
    foreach ($x in $FTP){
        -split (Split-Path $x -Leaf) | %{
            $_ -split ".",0, "simplematch"
                } | %{
                    $_ -split '(:)'
                        } |
                            Select-Object -Index 0
                                }
)

$Ext=@(
    foreach ($x in $FTP){
    -split (Split-Path $x -Leaf) | %{
        $_ -split ".",0, "simplematch"
            } | %{
                $_ -split '(:)'
                    } |
                        Select-Object -Index 1
                            }
)

$Prefix=@(
    foreach ($x in $FTP){
            -split (Split-Path $x -Leaf) | %{
        $_ -split ".",0, "simplematch"
            } | %{
                $_ -split '(:)'
                    } |
                        Select-Object -Index 5
                            }
)

$Suffix=@(
    foreach ($x in $FTP){
        -split (Split-Path $x -Leaf) | %{
            $_ -split ".",0, "simplematch"
                } | %{
                    $_ -split '(:)'
                        } |
                            Select-Object -Index 7
                                }
)

$children=@()

for ($i=0; $i -lt $FTP.Count; $i++){
    
    $children+= "" | Select-Object -Property  @{
        label='Filename';expression={$Filename[$i]}
            },@{
                label='Ext';expression={$Ext[$i]}
                    },@{
                        label='Pre';expression={$Prefix[$i]}
                            },@{label='Suf';expression={$Suffix[$i]}
                                }
}

$children | epcsv -NoTypeInformation -path c:\childout_010813.csv

################################################### FTP Parent/Path Item ###################################################

#$x=$null
#$y=$null
#$source=$null
#$remote=$null

$source=@(
    foreach ($y in $FTP){
        ($y |Split-Path -Parent | Split-Path -NoQualifier)-split " " | ?{
            $_ -ne "\auto\scripts\vbs\generic\ftps.vbs"
                } | %{$_ -replace '\\','/'} | #%{$_.tolower()} |
                    Select-Object -Index 0 
    }
) # 382 records as of ---01/08/13---

$remote=@(
    foreach ($x in $FTP){
        ($x | Split-Path -Parent | Split-Path -NoQualifier)-split " " | ?{
            $_ -ne "\auto\scripts\vbs\generic\ftps.vbs" +"/"
                } | Select-Object -Index 2 | %{$_ -replace '\\','/'} | %{$_ +"/"} 
        } 
) # 382 records as of ---01/08/13---

$parents=@() # 382 records as of ---01/08/13---

for ($i=0; $i -lt $FTP.Count; $i++){
 
  $parents+= "" | Select-Object -Property  @{
    label='Source';expression={$source[$i]}
        },@{
            label='Remote';expression={$remote[$i]}
                }
}

$parents | epcsv -NoTypeInformation -path c:\parentout_010813.csv

################################################### Complete FTP listing ###################################################

$list=@() # 382 records as of ---01/08/13---

for ($i=0; $i -lt $FTP.Count; $i++){

  $list+= "" | Select-Object -Property  @{
    label='Source';expression={
        $source[$i]}
            },@{
                label='Remote';expression={$remote[$i]}
                    },@{
                        label='Filename';expression={$Filename[$i]}
                            },@{
                                label='Ext';expression={$Ext[$i]}
                                    },@{
                                        label='Prefix';expression={$Prefix[$i]}
                                            },@{
                                                label='Suffix';expression={$Suffix[$i]}
                                                    }
}

#$list | %{$_ -replace '\\','/'} | %{$_.tolower()} | Select-Object -Property Source,Remote,Ext,Prefix,Suffix| epcsv -NoTypeInformation -path c:\ftplistout_020813.csv
$list | Select-Object -Property Source,Remote,Filename,Ext,Prefix,Suffix| epcsv -NoTypeInformation -path c:\ftplistout_020813.csv

$source=$null
$parents=$null
#$list=$null