﻿### Script Move's File from one location to another ###

Start-Transcript O:\AUTO\Scripts\Powershell\Logs\Move-File.log -Append -Force

$goods=$args[0]
$dest=$args[1]
$type=$args[2]

#$goods='C:\PS\Test'
#$dest='C:\PS\TestEnv'
#$type='bat'

cd $goods
mi (gci $goods -Filter "*.$type") $dest -Force

Stop-Transcript

"*.$type"