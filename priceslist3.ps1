﻿
## Script output path & static path to Prices files
$prices='c:\prices_list.csv'
$prices_dir='Y:\FeedProc\P04'

## Regular expressions used
$prefixregex=[regex]"(\d)"
$fileregex=[regex]"[A-Z_.]"

## Prices Arrays
$complete_list=@()
$dir_list=@()
$price_list=@()

#(gci -recurse $prices_dir) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice'} | %{$_.fullname}
#(gci -recurse $prices_dir) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice'} | %{$_.fullname} | epcsv -NoTypeInformation $prices

### Grab files only
(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice*'} | %{$_.fullname}


## Complete results for Full list -  Append properties to imported as csv headers
$complete_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | %{$_.FullName} | ?{$_ -notlike '*\LivePrice\*'} | ?{$_ -notlike '*\ScottTest\*'}
$complete_list | %{$_ | Add-Member -MemberType NoteProperty -Name Localpath -Value ($_ | Split-Path -Parent)}
$complete_list | %{$_ | Add-Member -MemberType NoteProperty -Name File -Value ($fileregex.Split(($_ | Split-Path -Leaf))[-2])}
$complete_list | %{$_ | Add-Member -MemberType NoteProperty -Name Extension -Value (($_ | Split-Path -Leaf).Split('.')[1])}
$complete_list | %{$_ | Add-Member -MemberType NoteProperty -Name Prefix -Value ($prefixregex.Split(($_ | Split-Path -Leaf))[0])}

## Split prefix $ filename
#$regex=[regex]"(\d)"
#$complete_list | Split-Path -Leaf


## Export of Filelist to csv format
$complete_list | Select-Object -Property ID,Prefix,File,Suffix,Extension,Localpath,Remote | epcsv -NoTypeInformation $prices