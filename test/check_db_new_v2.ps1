﻿#* FileName: SLA_check.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose:  Periodically check DB for feed SLA/ETA state
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [28/08/2013]
#* Time: [12:00]
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Determine which incrment to apply
#
#
# =============================================================================

#C:\'Program Files'\NSClient++\scripts\Check_db_new.ps1 -h HAT_MY_Diesel -dbs wca -tbl tbl_opslog

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$false,
ParametersetName='DataBase'
)]
#[switch]

[Parameter(
mandatory=$true,Position=0
)]
[string]$h='MyDev', #Server to Check

[Parameter(
mandatory=$true,Position=1
)]
[string]$dbs='tbl_opslog', #Table to Check

[Parameter(
mandatory=$true,Position=2
)]
[string]$tbl='smf4', #Database to Check

[parameter(
mandatory=$false
)]
[string]$i, #Increment

[parameter(Mandatory=$false)]
[string]$logfile='OPS-test',

[parameter(
mandatory=$false
)]
[string]$alert

)

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

#------------------------- Check Required Parameters -------------------------#
switch ($x){
    {(!($h))}{Write-Host 'No DB Server Specified!'
        msgbox 'No DB Server Specified!'}
    {(!($dbs))}{Write-Host No Database specified!
        msgbox 'No Database Specified!'}
    {(!($tbl))}{Write-Host No Table specified!
        msgbox 'No Table Specified!'}
}

#------------------------- Set alert function -------------------------#

#$time=(get-date).hour
$time=(get-date).ToShortTimeString()
$hmtime=$time.Split(':')
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'
$global:class='ok'

### Logs step and output of entire script
#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

$global:logvalue=@()

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       #switch ($logfile) {
       switch ($h) {
       
                #------------------------- WCA incrementals values -------------------------#
                'HAT_MY_Diesel'
                    {switch ($time) {
                    #{switch ($hmtime[0]) {
                        { $_ -ge '14:25' -and $_ -le '18:29'} {$global:inc='_2'}
                        {$_ -ge '18:30' -and $_ -le '23:59'} {;$global:inc='_3';}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'iCloud_MySql'
                    {switch ($time) {
                    #{switch ($hmtime[0]) {
                        { $_ -ge '14:35' -and $_ -le '19:29'} {$global:inc='_2'}
                        {$_ -ge '19:30' -and $_ -le '23:59'} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'Liquid_MySql'
                    {switch ($time) {
                    #{switch ($hmtime[0]) {
                        { $_ -ge '14:35' -and $_ -le '19:29'} {$global:inc='_2'}
                        {$_ -ge '19:30' -and $_ -le '23:59'} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'MyDev'
                    {switch ($time) {
                    #{switch ($hmtime[0]) {
                        #{ $_ -ge '15:50' -and $_ -le '19:29'} {$global:inc='_2'}
                        { $_ -ge '15:50' -and $_ -le '19:29'} {$global:inc='_2'}
                        {$_ -ge '19:30' -and $_ -le '23:59'} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        #{ $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge '15:45' -and $_ -le '18:00'} {$global:inc='_2'}
                        {$_ -ge '18:30' -and $_ -le '23:59'} {$global:inc='_3'}
                        #default {Write-host 'default inc1';$global:inc='_1'}
                        default {$global:inc='_1'}
                        }
                    }
                     
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       #default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       default {$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        #$hold=@()
        #$global:thestr = $args[0]
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output "$d $displaytxt" #;exit
        #$hold.count
        $global:logvalue
        #if ($args[1] -eq "exit") {exit}
    }


#------------------------- Set DB Connection -------------------------#
$b=gc '\\192.168.12.4\ops\auto\Configs\DBServers.cfg' | ?{$_.startswith("$h")}
$c=$b.split("`t")
$server=$c[4]
$id=$c[2]
$pwd=$c[3]
$myconnection.Connectionstring="server=$server;database='';Persist Security Info=false;user id=$id;pwd=$pwd"

#if ($c[1].startswith('my')){
    [void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll");
    $myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection
    #}
#else{
    #$myconnection=New-Object System.Data.SQLClient.SQLConnection}

#----------Testing Connection----------#
#if($h -eq 'Liquid_MySql'){$id='damar'
#    $pwd='p1n3'
#}
#else{}

#------------------------- NET FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith('DOTNET')}
#$c=$b.split("`t")
#$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
#$NETpass=$d
#$NETcred=New-Object System.Management.Automation.PsCredential “channel7“,$NETpass

#------------------------- COM FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith("$s")}
#$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
#$COMpass=$d
#$COMcred=New-Object System.Management.Automation.PsCredential $d[2],$d[3]

#$id='damar'
#$pwd='p1n3'
#$id='auto_ops'
#$pwd="AK47ops"+'\\;'
#write-host "password:$pwd"
#write-host "username:$id"

#$server='192.168.12.160'

#$server='192.168.12.109'

#$server='67.227.167.146'
#$db='OpsSLA'
#$db='smf4'
#$id='sa'
#$id='auto_ops'
#$pwd='AK47ops;'
#$pwd='K376:lcnb'

#------------------------- DB connections -------------------------#
## Default
#$myconnection.Connectionstring="server=$server;database='';Persist Security Info=false;user id=$id;pwd=$pwd"

## OTRS
#$myconnection.Connectionstring = "server=192.168.12.202;database=test;Persist Security Info=false;user id=otrs;pwd=hot"
## Diesel
#$myconnection.Connectionstring="server=$server;database=$db;Persist Security Info=false;user id=$id;pwd=$pwd"
## LiquidWeb
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"
## iCLOUD
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"

$myconnection.open()

#Set query command
#$Command= New-Object MySql.Data.MySqlClient.MySqlCommand
#$Command.Connection=$myconnection

#------------------------- Queries -------------------------#
$show_all= "SHOW TABLES"
#$SMF="SELECT * FROM smf4.tbl_opslog order by acttime desc;"
$SMF="SELECT Feeddate FROM smf4.tbl_opslog order by acttime asc;"
$WCA="SELECT feeddate FROM wca.tbl_opslog order by acttime asc;"
$test="SELECT feeddate,seq FROM wca.tbl_opslog order by acttime asc;"
#$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"

$command=$myconnection.CreateCommand()
#$command.CommandText=$join
#$command.CommandText=$SMF
#$command.CommandText=$WCA
#$command.CommandText=$test

switch($dbs){
    'wol'{$run="SELECT acttime FROM $dbs.$tbl where acttime like '2014%' order by acttime asc;"
            #$feedate=$collection[-2]
            #$feedate=$collection[-2];
            #$seq=$collection[-1]
                #switch($collection[-1]){
                    #'12:00:00 '{$feed_inc='12:00'}
                    #'18:00:00 '{$feed_inc='18:00'}
                #}
        }
    default {#$run="SELECT feeddate,seq FROM $tbl.$dbs order by acttime asc;"
            #$run="SELECT acttime FROM $dbs.$tbl order by acttime asc;"
            $run="SELECT acttime FROM $dbs.$tbl where acttime like '2014%' order by acttime asc;"
        #$feedate=$collection[-3]
        }
}

switch($tbl){
    'smf4'{$run="SELECT feeddate FROM $tbl.$dbs order by acttime asc;";
            $feedate=$collection[-2];
            $seq=$collection[-1]
                switch($collection[-1]){
                    '12:00:00 '{$feed_inc='12:00'}
                    '18:00:00 '{$feed_inc='18:00'}
                }
        }
    'wca'{$run="SELECT feeddate,seq FROM $tbl.$dbs order by acttime asc;"
            #$feedate=$collection[-3]
        }
    default {#$run="SELECT feeddate,seq FROM $tbl.$dbs order by acttime asc;"
            #$run="SELECT acttime FROM $dbs.$tbl order by acttime asc;"
            $run="SELECT acttime FROM $dbs.$tbl where acttime like '2014%' order by acttime asc;"
        #$feedate=$collection[-3]
        }
}

$command.CommandText=$run

#------------------------- Output Results -------------------------#
$reader=$Command.ExecuteReader()

$dataset=New-Object System.Data.DataSet
#Write-Output $reader.GetValue(0).ToString()

$dataset.Tables[0]
#while($myreader.Read()){$myreader.GetString(0)}

$collection=@()

while 
($reader.Read()) {
    for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
              #$reader.GetValue($i).ToString() | tee -Variable value
              $collection+=$reader.GetValue($ii).ToString().Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
    }
}

$myconnection.Close()

$today=(get-date -Format g).Split(' ')
#$feedate=$value.Split(' ')
#$feedate=$collection[-3]

switch($dbs){
    'wol'{$run="SELECT acttime FROM $dbs.$tbl where acttime like '2014%' order by acttime asc;"
            $feedate=$collection[-2]
            #$feedate=$collection[-2];
            #$seq=$collection[-1]
                #switch($collection[-1]){
                    #'12:00:00 '{$feed_inc='12:00'}
                    #'18:00:00 '{$feed_inc='18:00'}
                #}
        }
    default {#$run="SELECT feeddate,seq FROM $tbl.$dbs order by acttime asc;"
            #$run="SELECT acttime FROM $dbs.$tbl order by acttime asc;"
            $run="SELECT acttime FROM $dbs.$tbl where acttime like '2014%' order by acttime asc;"
        #$feedate=$collection[-3]
        }
}

switch($tbl){
    'smf4'{$run="SELECT feeddate FROM $tbl.$dbs order by acttime asc;";
            $feedate=$collection[-2];
            $seq=$collection[-1]
                switch($collection[-1]){
                    '12:00:00'{$feed_inc='12:00:00'}
                    '18:00:00'{$feed_inc='18:00:00'}
                        }
        }
    'wca'{$run="SELECT feeddate,seq FROM $tbl.$dbs order by acttime asc;"
            $feedate=$collection[-3]
        }
    'sec'{$run="SELECT acttime FROM $tbl.$dbs where acttime like '2014%' order by acttime asc;"
            $feedate=$collection[-2]
        }
    default {#$run="SELECT feeddate,seq FROM $dbs.$tbl order by acttime asc;"
            $run="SELECT acttime FROM $tbl.$dbs order by acttime asc;"
        #$feedate=$collection[-3]
        }
}
Get-Increment
#Set-Logpath

#switch($collection[-1]){
#$global:inc[1]{Write-Host 'correct inc'}
#default {write-host 'No Value'}
#}
#"Info:$collection"
switch ($feedate){
    $today[0] {switch($tbl){
                #$global:inc[1] {write-host 'Dance'}
                #$_{write-host "$s/$dbs Database upto date! DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                'smf4'{switch($x){
                        {$collection[-1] -eq $feed_inc}{write-host "DB OK - $h/$tbl Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0}
                            #write-host "DB OK - $h/$tbl Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                         default{write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1]
                                    write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1] ;exit 2}
                            }
                        }
                'wca'{switch($collection[-1]){
                        {$_ -eq $Global:inc[1]}{write-host "DB OK - $h/$tbl Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                            #write-host "DB OK - $h/$tbl Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0}
                         default{write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1]
                                    write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1] ;exit 2}
                            }
                        }
                'wca2'{switch($collection[-1]){
                        {$_ -eq $Global:inc[1]}{write-host "DB OK - $h/$tbl Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]
                                                    write-host "DB OK - $h/$tbl Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                        default{write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1]
                                    write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1];exit 2}
                #{$_ -eq $Global:inc[1]}{write-host "DB OK - $s/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                #default{write-host 'Feed Inc not upto date'}
                        }
                #default {write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1] ;exit 2}
                    }
                default {$atime=$feedate; write-host "DB OK - $h/$dbs/$tbl Database upto date - DB FeedDate:" $atime "Seq:"$collection[-1]
                            write-host "DB OK - $h/$dbs/$tbl Database upto date - DB FeedDate:" $atime "Seq:"$collection[-1]; exit 0} #$global:inc[1]}}
                }
            }
    #default {write-host "Error! $s/$dbs Database is not Upto date: Loader expects file for -"$today[0]; exit 2}
    #default {write-host "Error! $s/$dbs Database is not Upto date: Last file loaded -"$feedate "Seq -" $collection[-1]; exit 2}
    #default {write-host "Error! $s/$dbs Database is not Upto date - Last file loaded:"$feedate "Seq:"$collection[-1]; exit 2}
    default {write-host "Error! $h/$tbl Database is not Upto date - DB Feeddate:"$feedate "Seq:"$collection[-1]
                write-host "Error! $h/$tbl Database is not Upto date - DB Feeddate:"$feedate "Seq:"$collection[-1]; exit 2}
}    
        #write-host "WCA Feed successfully updated! DB FeedDate:" $feedate "Seq:"$collection[-1]} #$global:inc[1]}
    #default {write-host "Todays feed not found:"$today[0] "Last Feed available:" $feedate[0] `n"Please Download Today's file before proceeding"
    #default {write-host "Todays feed not found! Last Feed available -" $feedate `n"Please Download Today's feed before proceeding -" $today[0]

#------------------------- Null Results -------------------------#
#$collection=$null
#$dbs=$null
#$ii=$null
#$pwd=$null
#$run
$feedate=$null