﻿#* FileName: Format_Xrates.ps1
#*=============================================================================
#* Script Name: [Format_xrates]
#* Created: [25/02/2014]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Scritp Purpose:  
#* Format Xrates OER & ECB files for import in DB
#* 
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [25/02/2014]
#* Time: 
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/02/2014]
# Author: Damar Johnson
# Arguments: Source, Inc, logfile, type
# =============================================================================
# Purpose: 
#
#
# =============================================================================

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$true,Position=0,
ParametersetName='Action'
)]
#[switch]

[Parameter(
mandatory=$true
)]
#[string]$a, #Action to perform

#[Parameter(
#Mandatory=$true
#)]
[string]$s, #Source path of original file

[Parameter(
Mandatory=$true
)]
[string]$d, #Destination of original file

[parameter(
mandatory=$true
)]
[string]$t, # Type of file 

[int]$age,

[parameter(
mandatory=$false
)]
[string]$i, #Increment

[parameter(Mandatory=$true)]
[string]$logfile='OPS-test',

[parameter(
mandatory=$false
)]
[string]$alert

)
#------------------------- Set alert function -------------------------#

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

$action=$a
$source=$s
$dest=$d
$type=$t
#$age {$age}

$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'
$global:class='ok'

$global:logvalue=@()
$global:source=$source
$archive=$s

#([string]$xrate).split('') | ?{$_.startswith('OER')}

#########################
### Format Rates File ###
# Need a format step to do the following
# Add 3 columns - 1) Date; from filename#, 2) Source; from filename, 3) Base; always USD.
# Add field hearders; 1) currency, 2) ra#te 3) feeddate, 4) base, 5) src
# Add Euro legacy currencies - List:

## Set path variables
#------------------ Live Path ------------------#
#$csvpath="O:\Datafeed\Xrates\"
#$legacy= "O:\Datafeed\Xrates\USD-Legacy-Curr.txt"
#$csvpath=$args[0]

#Format Xrates ECB+OER=powershell.exe O:\AUTO\Scripts\Powershell\Format_xrates.ps1 -t csv -s O:\Datafeed\Xrates\test -logfile xrates

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                't15022_inc' 
                #'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 13 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                            }
                #------------------------- t15022 incrementals values -------------------------#
                #'t15022_inc' 
                    #{
                        #switch ($time) {
                            #{ $_ -ge 13 -and $_ -le 17} {$global:inc='_2';$global:fileinc='153000'}
                            #{$_ -ge 18 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            #default {$global:inc='_1';$global:fileinc='083000'}
                                #}
                                #$global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                #$global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                        #}
                            #}
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 13 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        write-output "$d $displaytxt" #;exit
        $global:logvalue
    }

#------------------ Test Path ------------------#
#$csvpath="c:\pstest\Datafeed\Xrates\"
#$source="c:\pstest\Datafeed\Xrates\"
#$legacy= "c:\pstest\Datafeed\Xrates\USD-Legacy-Curr.txt"

#------------------ Live Path ------------------#
#$csvpath="O:\Datafeed\Xrates\"
#$legacy= "O:\Datafeed\Xrates\USD-Legacy-Curr.txt"
#$csvpath=$args[0]

#Append file content if sourcename matches stated 
#$base="USD"
$ID=""
#$ap=(gc $legacy)
$hold='O:\Datafeed\TholdSec\hold\holdfile.txt'
$feed='O:\Datafeed\TholdSec\hold\feed.txt'
$c=$a.BaseName

$tholdsec=gci $source -Filter *.txt #| %{$a=$_.BaseName; $b=$_.fullname};$fn=$a.split("_")#;write-output $($fn[0]) ($fn[1])}


$total=$tholdsec.count

LogThis "$total`: File(s) pending format | Source:$source";$global:class='ok';Get-Increment;Set-Logpath
switch (($tholdsec)){
    {$_.basename.endswith('nasdaqth')}{$b=$_.fullname;$a=$_.basename;$fn=$a.split("_");$dh=(gc $b)
        $dh= $dh[1..($dh.count)] | ?{$_ -ne $dh[-1]}
        #$dh > $b
        $dh > $hold
        #$c=$a.BaseName
        $dest=($d+'\'+$a+'.csv')
                
        #Append full format to csv file
        $fc= $hold | ipcsv -Header "Symbol","Security Name","Markit Category","Code" -Delimiter '|'
        #$fc | % {$_.src=$fn[0];$_.Feeddate=$fn[1];$_.base=$base;$_.Time=$fn[2];$_."Act Flag"='I'};
        
        ##----------28/04/14 - DJ - Code Hardcoded to USNASD ----------##
        $fc | %{$_.Code='USNASD'}
        $fc | epcsv -NoTypeInformation -Path $dest -Delimiter "`t"
        $dh2= (gc $dest).replace('"','') #| sc 'C:\PS\Test\formatted\testout1.csv'
        $dh2[1..($dh2.count)] | sc $dest
        ## Test Out ##
        #$fc | ?{$_ -ne $fc[0]} | epcsv -NoTypeInformation -path C:\PS\Test\formatted\testout.csv
            LogThis "$a.$t";$Global:class='Report';Set-Logpath
                }
    {$_.basename.endswith('NYSEARCAth')}{$b=$_.fullname;$a=$_.basename;$fn=$a.split("_");$dh=(gc $b)
        $dh= $dh[1..($dh.count)] | ?{$_ -ne $dh[-1]}
        #$dh > $b
        $dh > $hold
        $dest=($d+'\'+$a+'.csv')
                
        #Append full format to csv file
        $fc= $hold | ipcsv -Header "Symbol","Security Name","Markit Category","Code" -Delimiter '|'
        
        ##----------28/04/14 - DJ - Code Hardcoded to USPAC ----------##
        $fc | %{$_.Code='USNASD'}
        $fc | epcsv -NoTypeInformation -Path $dest
        $dh2= gc $dest
        $dh2[1..($dh2.count)] | sc $dest
        ## Test Out ##
        #$fc | ?{$_ -ne $fc[0]} | epcsv -NoTypeInformation -path C:\PS\Test\formatted\testout.csv
            LogThis "$a.$t";$Global:class='Report';Set-Logpath
                }
    {$_.basename.endswith('NYSEth')}{$b=$_.fullname;$a=$_.basename;$fn=$a.split("_");$dh=(gc $b)
        $dh= $dh[1..($dh.count)] | ?{$_ -ne $dh[-1]}
        #$dh > $b
        $dh > $hold
        $dest=($d+'\'+$a+'.csv')
                
        #Append full format to csv file
        $fc= $hold | ipcsv -Header "Symbol","Security Name","Markit Category","Code" -Delimiter '|'
        
        ##----------28/04/14 - DJ - Code Hardcoded to USNYSE ----------##
        $fc | %{$_.Code='USNASD'}
        $fc | epcsv -NoTypeInformation -Path $dest
        $dh2= gc $dest
        $dh2[1..($dh2.count)] | sc $dest
        
        ## Test Out ##
        #$fc | ?{$_ -ne $fc[0]} | epcsv -NoTypeInformation -path C:\PS\Test\formatted\testout.csv
            LogThis "$a.$t";$Global:class='Report';Set-Logpath
                }
    default{'You failed son!!'}
}

#Get-Increment
#Set-Logpath

exit $LASTEXITCODE