[void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data") 
#[void][system.reflection.Assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.8.4\Assemblies\v4.5\MySQL.Data.dll")

$report_id = $args[0]

if (-not($report_id)){
	Throw "You Must supply a Repor identification numer"
	exit
}else{
	<#
		If additional report are required then the following will have to be amended
		1) Create the .SQL in the SQL folder with the report_id (eg. 21.SQL)
		2) Amend the following if statement and append with addition elseif for the report_id
		   and give the newly created report a fullname (no spaces allowed)
	#>
	if ($report_id -eq 60){
		$reportname = "Opportunity_Summary"
	}elseif ( $report_id -eq 52){
		$reportname = "Weekly_Activity"
	}
}
Write-Host Generating PDF report

#Create a variable to hold the connection:
$myconnection = New-Object MySql.Data.MySqlClient.MySqlConnection

#Set the connection string:
$myconnection.ConnectionString = "database=edi_cms;server=67.227.167.146;Persist Security Info=false;user id=i.cornish;pwd=C0pp3rt0p;"

#Call the Connection object�s Open() method:
$myconnection.Open()

#Print the connection properties to the console
#echo $myconnection

#The dataset must be created before it can be used.
$dataSet = New-Object System.Data.DataSet 

# Create a new command so we can execute SQL statement against the server
$command = $myconnection.CreateCommand()

# Read the content of a file into our CommandText as our SQL statement to be executed.
$command.CommandText = [IO.File]::ReadAllText("O:\Apps\JasperStarter\SQL\$report_id.sql")

# Execute the SQL statement agaist the MySQL Database.
$reader = $command.ExecuteReader()

# Get the semi-colon (;) delimited list of email address from the first record returned
# and write them to the recipients.txt file in the reports folder.
$reader.Read()
$recipients = $reader.GetValue(0).ToString();
$recipients | Out-File O:\Apps\JasperStarter\Reports\recipients.txt -encoding ASCII

#Close the database connection before we continue as it is no longer needed
$myconnection.Close()


# Generate the PDF report
$jasper = "O:\Apps\JasperStarter\bin\JasperStarter.exe"
#&$jasper pr -fpdf "O:\Apps\JasperStarter\templates\$reportname.jrxml" -o "O:\Apps\JasperStarter\reports\$reportname" -t mysql -H 67.227.167.146 -u i.cornish -p C0pp3rt0p -n edi_cms
&$jasper pr -fpdf "O:\Apps\JasperStarter\templates\$reportname.jrxml" -o "O:\Apps\JasperStarter\reports\test\$reportname" -t mysql -H 67.227.167.146 -u i.cornish -p C0pp3rt0p -n edi_cms

# Email the report
$gmailer = "C:\Program Files (x86)\Java\jre7\bin\java.exe"
# -- uncomment this for debugginh  -  &$gmailer -jar j:\java\prog\i.cornish\NB6\J2SE\gmailer\dist\gmailer.jar -DEBUG -USER i.cornish@exchange-data.com -PASS c0pp3rt0p -RECP o:\Apps\JasperStarter\reports\recipients.txt -ATTACH O:\Apps\jasperstarter\reports\$reportname.pdf -SUB "$reportname report" -BODY "See attched document."
&$gmailer -jar j:\java\prog\i.cornish\NB6\J2SE\gmailer\dist\gmailer.jar -USER noreply@exchange-data.com -PASS 3xch:ang3 -RECP O:\Apps\jasperstarter\reports\test\recipients.txt -ATTACH O:\Apps\jasperstarter\reports\$reportname.pdf -SUB "$reportname report" -BODY "See attched document."


# The following line is for debugging of the command line arguments sent to the GMailer application
#& "c:\users\Ian Cornish\Documents\powershell\EchoArgs.exe" -jar j:\java\prog\i.cornish\NB6\J2SE\gmailer\dist\gmailer.jar -DEBUG -USER i.cornish@exchange-data.com -PASS c0pp3rt0p -RECP "O:\Apps\JasperStarter\reports\recipients.txt" -ATTACH O:\Apps\jasperstarter\reports\$reportname.pdf











# SIG # Begin signature block
# MIIEBQYJKoZIhvcNAQcCoIID9jCCA/ICAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUQLpwhG9VT+X7VpeyOGyxcEXH
# svagggIfMIICGzCCAYigAwIBAgIQlHR/WAjB/7ZMOP+q8RQyLzAJBgUrDgMCHQUA
# MBwxGjAYBgNVBAMTEUlhbiBEYXZpZCBDb3JuaXNoMB4XDTE0MTEwNzE0MjA0OFoX
# DTM5MTIzMTIzNTk1OVowHDEaMBgGA1UEAxMRSWFuIERhdmlkIENvcm5pc2gwgZ8w
# DQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAN6c5Uic/HwiCC9pV26pedq43ASTBE7L
# lYU0uasSNyJKT8YpCTP9CkZ/GTRZe3b8EUmWAXhDvP2lVAclkoSjmvko+1gQn0Hs
# K14yaptw6Z1Jh/ST+4dOl0d+PeWROmOaUHEXIHCCHE2D2k+M+N8KghyvLZDBWamD
# w7vx0gqo0WiZAgMBAAGjZjBkMBMGA1UdJQQMMAoGCCsGAQUFBwMDME0GA1UdAQRG
# MESAEFbNCvvYm+9f2zXkVC7kvfyhHjAcMRowGAYDVQQDExFJYW4gRGF2aWQgQ29y
# bmlzaIIQlHR/WAjB/7ZMOP+q8RQyLzAJBgUrDgMCHQUAA4GBADRAky7j69E6pWTK
# KoIfCQ0shR2L/7OJP6gsB0L5epsFYqGfMA8v4QCJgasb1b4BTz5GTR0K4ZPpmDsj
# EjCLkN5XDGWrtkPPJwlZTmh30tOkBhPgXNcjKPC0GTaWnVM5XToLSnT4Ybnh0/T8
# PSVr/j04DS/QAV079tHzFSUJ1YWBMYIBUDCCAUwCAQEwMDAcMRowGAYDVQQDExFJ
# YW4gRGF2aWQgQ29ybmlzaAIQlHR/WAjB/7ZMOP+q8RQyLzAJBgUrDgMCGgUAoHgw
# GAYKKwYBBAGCNwIBDDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYBBAGC
# NwIBBDAcBgorBgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0BCQQx
# FgQUwObo8gtWfoeJ1Uta7sODy5XilXYwDQYJKoZIhvcNAQEBBQAEgYDYJD8fklD/
# YkJx157KMY1LBqQ6H8tMXc58QmRQxxWQR+A5Jkp/B4SVQ4xHD7mJRnbn+SHx7LPg
# +4Whx6YE/T7G+B+QPdYk8LUfm1N/7RkunkD+9o3q0HTFbhLR3mXs0KazkrwIzRMw
# MNsqnaIhgenMWrBpuVhX+JORiz30M3sNtg==
# SIG # End signature block
