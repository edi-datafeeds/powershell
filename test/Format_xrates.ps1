﻿#* FileName: Format_Xrates.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Scritp Purpose:  Periodically check DB for Scheduler/Task state
#* Detaied version of check_db.ps1. To be used from scheduler/taskfile
#* check_db is designed for Nagios use - minimal output
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [29/01/2014]
#* Time: [15:00]
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: 
#
#
# =============================================================================

#([string]$xrate).split('') | ?{$_.startswith('OER')}

#########################
### Format Rates File ###
# Need a format step to do the following
# Add 3 columns - 1) Date; from filename#, 2) Source; from filename, 3) Base; always USD.
# Add field hearders; 1) currency, 2) ra#te 3) feeddate, 4) base, 5) src
# Add Euro legacy currencies - List:

## Set path variables
#------------------ Live Path ------------------#
#$csvpath="O:\Datafeed\Xrates\"
#$legacy= "O:\Datafeed\Xrates\USD-Legacy-Curr.txt"
#$csvpath=$args[0]

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                't15022_inc' 
                #'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 13 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                            }
                #------------------------- t15022 incrementals values -------------------------#
                #'t15022_inc' 
                    #{
                        #switch ($time) {
                            #{ $_ -ge 13 -and $_ -le 17} {$global:inc='_2';$global:fileinc='153000'}
                            #{$_ -ge 18 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            #default {$global:inc='_1';$global:fileinc='083000'}
                                #}
                                #$global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                #$global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                        #}
                            #}
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 13 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        write-output "$d $displaytxt" #;exit
        $global:logvalue
    }

#------------------ Test Path ------------------#
$csvpath="c:\pstest\Datafeed\Xrates\"
$legacy= "c:\pstest\Datafeed\Xrates\USD-Legacy-Curr.txt"

#Append file content if sourcename matches stated 
$base="USD"
$ap=(gc $legacy)

$xrate=gci $csvpath -Filter *.csv #| %{$a=$_.BaseName; $b=$_.fullname};$fn=$a.split("_")#;write-output $($fn[0]) ($fn[1])}

#gci $csvpath -Filter *.csv | tee -Variable xrate #| %{$a=$_.BaseName; $b=$_.fullname
        #$fn=$a.split("_")#;write-output $($fn[0]) ($fn[1])
            #} #| tee -Variable xrate

#switch (([string]$xrate).split('')){
switch (($xrate)){
    #{$_.basename.startswith('ECB')}{$b=$_.fullname;$a=$b | split-path -Leaf;$fn=$a.split("_");$dh=(gc $b | %{$_.Split('{')} | ?{$_ -notlike '*}*'} | %{$_.replace(': ',',')})
    {$_.basename.startswith('ECB')}{$b=$_.fullname;$a=$_.basename;$fn=$a.split("_");$dh=(gc $b | %{$_.Split('{')} | ?{$_ -notlike '*}*'} | %{$_.replace(': ',',')})
        $dh= $dh[2..($dh.count)]
        $dh > $b
        ac $b -Value $ap
        #Append full format to csv file
        $fc= $b | ipcsv -header Curr,Rate,Feeddate,base,Src,Time 
        $fc | % {$_.src=$fn[0];$_.Feeddate=$fn[1];$_.base=$base;}#$_.Time=$fn[2]};
        $b=$fc | convertto-csv -notypeinformation | %{$_.Replace('"','')}| out-file $b -force -en ascii
            LogThis "File formatted: $b"
                $Global:class='Report'
                    Set-Logpath
                            }
    #{$_.basename.startswith('OER')}{$b=$_.fullname;$a=$b | split-path -Leaf;$fn=$a.split("_");$dh=(gc $b | %{$_.Split('{')} | ?{$_ -notlike '*}*'} | %{$_.replace(': ',',')})
    {$_.basename.startswith('OER')}{$b=$_.fullname;$a=$_.basename;$fn=$a.split("_");$dh=(gc $b | %{$_.Split('{')} | ?{$_ -notlike '*}*'} | %{$_.replace(': ',',')})
        $dh= $dh[8..($dh.count)]
        $dh > $b
        ac $b -Value $ap
        #Append full format to csv file
        $fc= $b | ipcsv -header Curr,Rate,Feeddate,base,Src,Time
        $fc | % {$_.src=$fn[0];$_.Feeddate=$fn[1];$_.base=$base}#;$_.Time=$fn[2]};
        $b=$fc | convertto-csv -notypeinformation | %{$_.Replace('"','')}| out-file $b -force -en ascii
            LogThis "File formatted: $b"
                $Global:class='Report'
                    Set-Logpath
                            }
    default{'You failed son!!'}
}
