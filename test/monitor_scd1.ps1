﻿#* FileName: SLA_check.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Scritp Purpose:  Periodically check DB for Scheduler/Task state
#* Detaied version of check_db.ps1. To be used from scheduler/taskfile
#* check_db is designed for Nagios use - minimal output
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [28/08/2013]
#* Time: [12:00]
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Determine which incrment to apply
#
#
# =============================================================================

#O:\AUTO\Scripts\Powershell\Check_db_regen_test_11.ps1 -h MyDev -dbs smf4 -action trig -file O:\AUTO\Tasks\TEST.tsk -queue -test

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$false,Position=0,
ParametersetName='DataBase'
)]
#[switch]

[Parameter(
mandatory=$true
)]
[string]$h='HAT_MY_Diesel', #Server to Check

[Parameter(
mandatory=$true,Position=1
)]
[string]$dbs='schedule', #Database to Check

[Parameter(
mandatory=$false
)]
[string]$action='',

[Parameter(
mandatory=$false
)]
[string]$name='',

[parameter(
mandatory=$false
)]
[string]$file,

[parameter(
mandatory=$false
)]
[string]$queue='-'+'',

#[parameter(
#mandatory=$false
#)]
#[string]$chk,

[parameter(
mandatory=$false
)]
[string]$i, #Increment

[parameter(Mandatory=$false)]
[string]$logfile='OPS-test',

[parameter(
mandatory=$false
)]
[string]$alert

)

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

#------------------------- Check Argument -------------------------#
    #If ((!($h))){
            #Write-Host 'No DB Server Specified!'
                    #msgbox 'No DB Server Specified!'
    #}
    #ElseIf ((!($dbs))){
            #Write-Host No Database specified!
                    #msgbox 'No Database Specified!'
    #}
    #ElseIf ((!($action))){
            #Write-Host No Action specified!
                    #msgbox 'No Database Specified!'
    #}
    #Else{#Start-Process echoArgs.exe -ArgumentList \java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar "--config=o:\AUTO\configs\global.cfg" -taskfile "O:\AUTO\Tasks\TEST.tsk" -auxilliary4}
    #}

    #switch($dbs){
    #'smf4'{$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"}
    #'wca'{$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"}
    #default {$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"}
    #}

#------------------------- Set alert function -------------------------#

#$tf='O:\AUTO\Tasks\TEST.tsk'
#$queue='-auxilliary2'

#switch ($action){
#'trig'{j:\java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar "--config=o:\AUTO\configs\global.cfg" -taskfile $file $queue}
#'prod'{o:\AUTO\Scripts\vbs\GENERIC\Prod.vbs a HAT_MS_sqlsvr1 O:\AUTO\Scripts\Sql\Equity\639_incr.sql o:\Datafeed\Equity\639incr\YYYYMMDD.639 : WcaWebload i}
#'prod'{o:\AUTO\Scripts\vbs\GENERIC\Prod.vbs a $server $query $feed}
#'prod'{'Gen'}
#'ftp'{'FTP'}
#'ftp'{o:\auto\scripts\vbs\generic\ftps.vbs $l $r $site $dir $tmp }
#default{'Nothing done'}
#}

#$time=(get-date).hour
$time=(get-date).ToShortTimeString()
$hmtime=$time.Split(':')
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'
$global:class='ok'

### Logs step and output of entire script
#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

$global:logvalue=@()

#$trig='j:\java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar'

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       #switch ($logfile) {
       switch ($h) {
       
                #------------------------- Host incrementals values -------------------------#
                'HAT_MY_Diesel'
                    {switch ($time) {
                        { $_ -ge '13:30' -and $_ -le '17:29'} {$global:inc='_2';break}
                        {$_ -ge '17:30' -and $_ -le '23:59'} {;$global:inc='_3';break}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'iCloud_MySql'
                    {switch ($time) {
                    #{switch ($hmtime[0]) {
                        { $_ -ge '13:35' -and $_ -le '18:29'} {$global:inc='_2';break}
                        {$_ -ge '17:30' -and $_ -le '23:59'} {;$global:inc='_3';break}
                        default {$global:inc='_1';break}
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'Liquid_MySql'
                    {switch ($time) {
                    #{switch ($hmtime[0]) {
                        { $_ -ge '13:35' -and $_ -le '18:29'} {$global:inc='_2';break}
                        {$_ -ge '17:30' -and $_ -le '23:59'} {;$global:inc='_3';break}
                        default {$global:inc='_1';break}
                        }
                    }
                #------------------------- MyDev incrementals values -------------------------#
                'MyDev'
                    {switch ($time) {
                        { $_ -ge '15:00' -and $_ -le '19:29'} {$global:inc='_2';break}
                        {$_ -ge '19:30' -and $_ -le '23:59'} {;$global:inc='_3';break}
                        default {$global:inc='_1';break}
                        }
                    }
                #------------------------- Host incrementals values -------------------------#
                'HSM_MY_Diesel'
                    {switch ($time) {
                        { $_ -ge '13:30' -and $_ -le '17:29'} {$global:inc='_2';break}
                        {$_ -ge '17:30' -and $_ -le '23:59'} {;$global:inc='_3';break}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- Test incrementals values -------------------------#
                #'t15022_inc' 
                'damar-test'
                    {switch ($time) {
                        {$_ -ge '12:00' -and $_ -le '16:00'} {$global:inc='_2';$global:fileinc='153000';break}
                        {$_ -ge '11:00' -and $_ -le '17:00'} {$global:inc='_3';$global:fileinc='203000';break}
                        default {$global:inc='_1';$global:fileinc='083000';break}
                            }
                            $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                            $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                    #}
                        }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        #{ $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge '14:45' -and $_ -le '17:00'} {$global:inc='_2';break}
                        {$_ -ge '17:30' -and $_ -le '23:59'} {$global:inc='_3';break}
                        #default {Write-host 'default inc1';$global:inc='_1'}
                        default {$global:inc='_1';break}
                        }
                    }
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       #default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       default {$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}
#------------------------- DB incrementals values -------------------------#
    switch ($dbs) {
                'wca'
                    {switch ($time) {
                        { $_ -ge '13:30' -and $_ -le '17:00'} {$feed_inc='_2'}
                        {$_ -ge '17:30' -and $_ -le '23:59'} {;$feed_inc='_3'}
                        default {$feed_inc='_1'}
                        }
                    }
                #------------------------- WCA2 incrementals values -------------------------#
                'wca2'
                {switch ($time) {
                    { $_ -ge '13:30' -and $_ -le '17:00'} {$feed_inc='_2'}
                    {$_ -ge '17:30' -and $_ -le '23:59'} {$feed_inc='_3'}
                    default {$feed_inc='_1'}
                    }
                }
                #------------------------- SMF4 incrementals values -------------------------#
                'smf4'
                    {switch ($time) {
                        { $_ -ge '12:10' -and $_ -le '17:59'} {$feed_inc='12:00:00'}
                        #{$_ -ge '17:50' -and $_ -le '00:00'} {$feed_inc='_3'}
                        default {$feed_inc='18:00:00'}
                        }
                    }
                'AutoOPS'{$run="SELECT feeddate,seq FROM $dbs.schedule order by acttime asc;"
                #$feedate=$collection[-3]
        }
                }
#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {#Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { #write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                       ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        #$hold=@()
        #$global:thestr = $args[0]
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue | Out-Null #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output $d,"$displaytxt" | Out-Null #;exit
        #$hold.count
        $global:logvalue | Out-Null
        #if ($args[1] -eq "exit") {exit}
    }

#################################################################
### Set Connection to MySQL DB and execute query ###

## Set MySQL Connectiion
[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection

#------------------------- NET FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith('DOTNET')}
#$c=$b.split("`t")
#$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
#$NETpass=$d
#$NETcred=New-Object System.Management.Automation.PsCredential “channel7“,$NETpass

#------------------------- COM FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith("$s")}
$b=gc '\\192.168.12.4\ops\auto\Configs\DBServers.cfg' | ?{$_.startswith("$h")}
$c=$b.split("`t")
$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
$COMpass=$d
#$COMcred=New-Object System.Management.Automation.PsCredential $d[2],$d[3]
$server=$c[4]
$id=$c[2]
$pwd=$c[3]
#$id='auto_ops'
#$pwd='AK47ops;'
#$server='192.168.12.160'
#$server='192.168.12.109'

#$server='67.227.167.146'
#$db='OpsSLA'
#$db='smf4'
#$id='sa'
#$id='auto_ops'
#$pwd='AK47ops;'
#$pwd='K376:lcnb'

#------------------------- DB connections -------------------------#

## Default
$myconnection.Connectionstring="server=$server;database='';Persist Security Info=false;user id=$id;pwd=$pwd"

## OTRS
#$myconnection.Connectionstring = "server=192.168.12.202;database=test;Persist Security Info=false;user id=otrs;pwd=hot"
## Diesel
#$myconnection.Connectionstring="server=$server;database=$db;Persist Security Info=false;user id=$id;pwd=$pwd"
## LiquidWeb
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"
## iCLOUD
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"

#Do{
$myconnection.open()

#Set query command
#$Command= New-Object MySql.Data.MySqlClient.MySqlCommand
#$Command.Connection=$myconnection

#------------------------- Queries -------------------------#

$show_all= "SHOW TABLES"
#$SMF="SELECT * FROM smf4.tbl_opslog order by acttime desc;"
$SMF="SELECT Feeddate FROM smf4.tbl_opslog order by acttime asc;"
$WCA="SELECT feeddate FROM wca.tbl_opslog order by acttime asc;"
$test="SELECT feeddate,seq FROM wca.tbl_opslog order by acttime asc;"
$command=$myconnection.CreateCommand()
#$command.CommandText=$join
#$command.CommandText=$SMF
#$command.CommandText=$WCA
#$command.CommandText=$test

switch($dbs){
    'smf4'{$run="SELECT feeddate FROM $dbs.tbl_opslog order by acttime asc;";
        }
    'wca'{$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"
            #$feedate=$collection[-3]
        }
    'AutoOPS'{#$run="SELECT run_at,days,last_run,status FROM $dbs.schedule order by acttime asc;"
                $run="SELECT schedulename,days,last_run,status,run_at FROM $dbs.schedule where run_at is not null;"
            #$feedate=$collection[-3]
        }
    default {$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"
        $feedate=$collection[-3]
        }
}

switch($name){
        #'Morning Ops (Daily)'{$run="SELECT schedulename,run_at,days,last_run,status FROM AutoOps.schedule order by acttime asc;"}
        'Morning Ops (Daily)'{$run="SELECT days,run_at,last_run,status FROM $dbs.schedule where schedulename = '$name';"}
    default {'dud!!!!'}
}

$command.CommandText=$run
#------------------------- Output Results -------------------------#

$reader=$Command.ExecuteReader()

$dataset=New-Object System.Data.DataSet
#$dataset = New-Object System.Data.DataSet
#Write-Output $reader.GetValue(0).ToString()

$dataset.Tables[0]
#while($myreader.Read()){$myreader.GetString(0)}

$collection=@()
#$collection1

while 
($reader.Read()) {
    for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
              #$reader.GetValue($i).ToString() | tee -Variable value
              $collection+=$reader.GetValue($ii).ToString()#.Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
              #$collection1=$reader.GetValue($ii).ToString() #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
    }
}
$collection
#$collection1
$sday=$collection[0].Split(',')
$stime=$collection[1]
$sllastrun=$collection[2]
$sstate=$collection[3]
#$myconnection.Close()
#$sday=,2,3,4,5
$today=(Get-Date).DayOfWeek
#$testday=$sday

switch($sday){
'1'{#$day='Sunday'
    switch($today){
        'Sunday'{$day='Sunday';"$day Success1";break}
        default{'fail1 break!!'}
            }
        }
'2'{#$day='Monday'
        switch($today){
        'Monday'{$day='Monday';"$day Success2";break}
        default{'fail2 break!!'}
            }
        }
'3'{#$day='Tuesday'
    switch($today){
        'Tuesday'{$day='Tuesday';"$day Success3";break}
        default{'fail3 break!!'}
            }
        }
'4'{#$day='Wednesday'
    switch($today){
        'Wednesday'{$day='Wednesday';"$day Success4";break}
        default{}
            }
        }
'5'{#$day='Thursday'
    switch($today){
        'Thursday'{;"$day Success5";break}
        default{'fail4 break!!'}
            }
        }
'6'{#$day='Friday'
    switch($today){
        'Friday'{$day='Friday';"$day Success6";break}
        default{'fail5 break!!'}
            }
        }
'7'{#$day='Saturday'
    switch($today){
        'Saturday'{'Saturday';"$day Success7";break}
        default{'fail6 break!!'}
            }
        }
default{'You failed son';break}
}

#switch($sday){
#'1'{$day='Sunday';'Success1'
#        }
#'2'{$day='Monday';'Success2'}
#'3'{$day='Tuesday';'Success3'}
#'4'{$day='Wednesday';'Success4'}
#'5'{$day='Thursday';'Success5'}
#'6'{$day='Friday';'Success6'}
#'7'{$day='Saturday';'Success7'}
#default{'none'}
#    }

#switch($sday){
#{$_.indexof('1')}{$day='Sunday';'Success1'}
#{$_.indexof('2')}{$day='Monday';'Success2'}
#{$_.indexof('3')}{$day='Tuesday';'Success3'}
#{$_.indexof('4')}{$day='Wednesday';'Success4'}
#{$_.indexof('5')}{$day='Thursday';'Success5'}
#{$_.indexof('6')}{$day='Friday';'Success6'}
#{$_.indexof('7')}{$day='Saturday';'Success7'}
#    }

#switch($sday){
#{?{$_.indexof('1')}}{$day='Sunday';'Success1'}
#{?{$_.indexof('2')}}{$day='Monday';'Success2'}
#{?{$_.indexof('3')}}{$day='Tuesday';'Success3'}
#{?{$_.indexof('4')}}{$day='Wednesday';'Success4'}
#{?{$_.indexof('5')}}{$day='Thursday';'Success5'}
#{?{$_.indexof('6')}}{$day='Friday';'Success6'}
#{?{$_.indexof('7')}}{$day='Saturday';'Success7'}
    #}

#%{$sday}{switch($_){
#'1'{$day='Sunday';'Success1'}
#'2'{$day='Monday';'Success2'}
#'3'{$day='Tuesday';'Success3'}
#'4'{$day='Wednesday';'Success4'}
#'5'{$day='Thursday';'Success5'}
#'6'{$day='Friday';'Success6'}
#'7'{$day='Saturday';'Success7'}
    #}
#}

#switch($day){
#(get-date).DayOfWeek{}
#'Tuesday'{}
#'Wednesday'{}
#'Thursday'{}
#'Friday'{}
#}

$date=(get-date -Format g).Split(' ')
#$feedate=$value.Split(' ')
#$feedate=$collection[-3]

switch($dbs){
    'smf4'{$run="SELECT feeddate FROM $dbs.tbl_opslog order by acttime asc;";
            $feedate=$collection[-2];
            #$seq=$collection[-1]
                #switch($collection[-1]){
                    #'12:00:00'{$feed_inc='12:00:00'}
                    #'18:00:00'{$feed_inc='18:00:00'}
                        #}
        }
    'wca'{$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"
            $feedate=$collection[-3]
        }
    default {$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"
        $feedate=$collection[-3]
        }
}
Get-Increment
#Set-Logpath

#switch($collection[-1]){
#$global:inc[1]{Write-Host 'correct inc'}
#default {write-host 'No Value'}
#}

switch ($feedate){
    #$today[0] {switch($dbs){
    $date[0] {switch($dbs){
                #$global:inc[1] {write-host 'Dance'}
                #$_{write-host "$s/$dbs Database upto date! DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                'smf4'{switch($x){
                        {$collection[-1] -eq $feed_inc}{write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];
                           LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath exit 0} #$global:inc[1]}
                         #default{'SMF Fail'}
                        #default{"Error! SMF Has Failed To Load" #Attempt"+$count++;
                        default{$global:class='failed'
                                write-host "Error!"$h/$dbs "Incorrect Increment - DB FeedDate:" $feedate "Seq:" $collection[-1]
                                        LogThis "Error!",$h/$dbs, "Incorrect Increment - DB FeedDate: ",$feedate ,"Seq:" ,$collection[-1];Set-Logpath
#LogThis "Error! ",$h/$dbs,"Database is not Upto date - DB Feeddate:",$feedate,"Seq:",$collection[-1]; Set-Logpath
                            #------------------------- WCA Retry -------------------------#
                            Do{
                            #$myconnection.open()
                            #$myconnection.state
                            #$reader=$Command.ExecuteReader()
                            #$dataset=New-Object System.Data.DataSet
                            #$dataset = New-Object System.Data.DataSet
                            #Write-Output $reader.GetValue(0).ToString()
                            $count++
                            $dataset.Tables[0]
                            switch ($myconnection.state){
                                'Open'{break}
                                'Closed'{$myconnection.Open();$reader=$Command.ExecuteReader();
                                    write-host "$h Connection is $myconnection".state}
                                    }
                            while 
                            ($reader.Read()) {
                                for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                                            $collection+=$reader.GetValue($ii).ToString().Split(' ')
                                                }
                                        }
                            #$myconnection.Close()
                            Get-Increment
                            switch ($feedate){
                                $date[0] {
                                        switch($dbs){
                                            'smf4'{
                                                switch($collection[-1]){
                                                    #{$_ -eq $Global:inc[1]}{write-host `
                                                    {$_ -eq $feed_inc}{write-host `
                                                        "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]
                                                           LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath; exit 0}
                                                        default{$count++
                                                            #write-host "Error! SMF Insert Failed - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count #}
                                                            write-host "$dbs".ToUpper() "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count #}
                                                            #LogThis "Error! SMF Insert Failed - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count;Set-Logpath}
                                                            LogThis "Error! $dbs".ToUpper()," Insert Failed - DB FeedDate:",$feedate,"Seq:",$collection[-1],"- Retry Attempt:"$count
                                                                $global:classs='failed';Set-Logpath}
                                                            }
                                                        }
                                                    }
                                                }
                                default {write-host `
                                    "SMF Insert Failed - Retry Attempt:"$count++ "DB FeedDate:" $feedate "Seq:"$collection[-1]
                                        Logthis "SMF Insert Failed - Retry Attempt:"$count++ "DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath}
                                }    
                        start-sleep -Seconds 05}Until($count -le '3');#Write-Host 'successssssss'
                        #$myconnection.Close();start-sleep -Seconds 05}Until($count -eq '3')
                            }
                        }
                    }
                'wca'{switch($collection[-1]){
                        {$_ -eq $global:inc[1]}{
                            #write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                            write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];
                            LogThis "DB OK -", $h/$dbs, "Database upto date - DB FeedDate:", $feedate, "Seq:",$collection[-1];
                                Set-Logpath;exit 0}
                        #default{"Error! SMF Has Failed To Load"
                        default{$global:class='failed'
                                Write-host "Error!" $h/$dbs":" "Incorrect Increment - DB FeedDate:" $feedate "Seq:" $collection[-1]
                                    LogThis "Error!", $h/$dbs, "Incorrect Increment - DB FeedDate:", $feedate, "Seq:",$collection[-1];Set-Logpath;
                         #------------------------- WCA Retry -------------------------#
                        Do{
                        #$count++
                        #$collection=$null
                        #$reader=$null
                        #$myconnection.open()
                        #$myconnection.state
                        switch ($myconnection.state){
                            'Open'{break}
                            'Closed'{$myconnection.Open();$reader=$Command.ExecuteReader();
                                        write-host "$h Connection is $myconnection".state}
                            }
                            while 
                            ($reader.Read()) {
                                for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                                            $collection+=$reader.GetValue($ii).ToString().Split(' ')
                                                }
                                        }
                            #$myconnection.Close()
                            Get-Increment
                            switch ($feedate){
                                $date[0] {
                                        switch($dbs){
                                            'wca'{
                                                switch($collection[-1]){
                                                    {$_ -eq 2}{write-host `
                                                        "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];
                                                            LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath; exit 0}
                                                        default{$count++
                                                            write-host "$dbs".ToUpper() "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count #}
                                                            #write-host "$dbs".ToUpper() "Insert Failed - Incorrect Increment - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count #}
                                                            #LogThis "Error!" "$dbs".ToUpper() "Insert Failed - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count;Set-Logpath}
                                                            #LogThis "Error! $dbs".ToUpper()," Insert Failed - DB FeedDate:",$feedate,"Seq:",$collection[-1],"- Retry Attempt:"$count;
                                                            LogThis "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count
                                                                $global:classs='failed';Set-Logpath}
                                                            #LogThis ("Error! "+"$h/$dbs "+"Incorrect Increment - DB FeedDate: "+"$feedate Seq: "+"$collection"[-1]);Set-Logpath
                                                            }
                                                        }
                                                    }
                                                }
                                default {write-host `
                                    "$dbs Insert Failed Retry Attempt:"$count++ "DB FeedDate:" $feedate "Seq:"$collection[-1]
                                    LogThis "$dbs Insert Failed Retry Attempt:"$count++ "DB FeedDate:" $feedate "Seq:"$collection[-1]}
                            }    
                        $myconnection.Close();start-sleep -Seconds 05}Until($count -eq '3')
                            }
                        }
                    }
                'wca2'{switch($collection[-1]){
                            {$_ -eq $Global:inc[1]}{write-host `
                                "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];
                                LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath exit 0} #$global:inc[1]}
                        #default{write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1];
                        default{$global:class='failed'
                                Write-Host Error! $h/$dbs Incorrect Increment - DB FeedDate: $feedate Seq: $collection[-1]
                                    LogThis "Error!" ,$h/$dbs, "Incorrect Increment - DB FeedDate:", $feedate, "Seq:", $collection[-1];Set-Logpath;
                        #------------------------- WCA Retry -------------------------#
                                Do{
                            #$myconnection.open()
                            #$myconnection.state
                            #$reader=$Command.ExecuteReader()
                            #$dataset=New-Object System.Data.DataSet
                            #$dataset = New-Object System.Data.DataSet
                            #Write-Output $reader.GetValue(0).ToString()
                            $count++
                            $dataset.Tables[0]
                            switch ($myconnection.state){
                                'Open'{break}
                                'Closed'{$myconnection.Open();$reader=$Command.ExecuteReader();
                                    write-host "$h Connection is $myconnection".state}
                                    }
                            while 
                            ($reader.Read()) {
                                for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                                            $collection+=$reader.GetValue($ii).ToString().Split(' ')
                                                }
                                        }
                            #$myconnection.Close()
                            Get-Increment
                            switch ($feedate){
                                $date[0] {
                                        switch($dbs){
                                            'wca2'{
                                                switch($collection[-1]){
                                                    #{$_ -eq $Global:inc[1]}{write-host `
                                                    {$_ -eq $feed_inc[1]}{write-host `
                                                        "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];
                                                           LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath; exit 0}
                                                        default{$count++
                                                            #write-host "WCA2 Insert Failed - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count}
                                                            write-host "$dbs".ToUpper() "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count #}
                                                            #LogThis "Error! $dbs".ToUpper()," Insert Failed - DB FeedDate:",$feedate,"Seq:",$collection[-1],"- Retry Attempt:"$count;
                                                            LogThis "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count
                                                                $global:classs='failed';Set-Logpath}
                                                            }
                                                        }
                                                    }
                                                }
                                default {write-host `
                                    "WCA2 Insert Failed Retry Attempt:"$count "DB FeedDate:" $feedate "Seq:"$collection[-1]
                                       Logthis "WCA2 Insert Failed Retry Attempt:"$count "DB FeedDate:" $feedate "Seq:"$collection[-1]}
                            }    
                        $myconnection.Close();start-sleep -Seconds 05}Until($count -eq '3')
                            }
                        }
                    }
                }
            }       
    default {$global:class='failed'
                write-host "Error! $h/$dbs Database is not Upto date - DB Feeddate:"$feedate "Seq:"$collection[-1]
                            #LogThis "Error! $h/$dbs Database is not Upto date - DB Feeddate:$feedate Seq:"$collection[-1]; Set-Logpath
                            LogThis "Error! ",$h/$dbs,"Database is not Upto date - DB Feeddate:",$feedate,"Seq:",$collection[-1]; Set-Logpath
                                #------------------------- WCA Retry -------------------------#
                                Do{
                                    #$count++
                                    #$collection=$null
                                    #$reader=$null
                                    #$myconnection.open()
                                    #$myconnection.state
                                    switch ($myconnection.state){
                                        'Open'{break}
                                        'Closed'{$myconnection.Open();$reader=$Command.ExecuteReader();
                                            write-host "$h Connection is $myconnection".state}
                                        }
                                        while 
                                        ($reader.Read()) {
                                            for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
                                                        $collection+=$reader.GetValue($ii).ToString().Split(' ')
                                                            }
                                                    }
                                        #$myconnection.Close()
                                        Get-Increment
                                        switch ($feedate){
                                            $date[0] {
                                                    switch($dbs){
                                                        'smf4'{
                                                            switch($collection[-1]){
                                                                {$_ -eq $feed_inc}{write-host `
                                                                    "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];
                                                                       LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath; exit 0}
                                                                    default{$count++
                                                                        #write-host "SMF Insert Failed - Retry DB FeedDate:" $feedate "Seq:"$collection[-1] "Attempt:"$count}
                                                                        write-host "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count
                                                                           LogThis "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count; Set-Logpath}
                                                                        }
                                                                    }
                                                        'wca'{
                                                            switch($collection[-1]){
                                                                {$_ -eq $global:inc[1]}{write-host `
                                                                    "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];
                                                                       LogThis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath; exit 0}
                                                                    default{$count++
                                                                        #write-host "WCA Insert Failed - Retry DB FeedDate:" $feedate "Seq:"$collection[-1] "Attempt:"$count}
                                                                        write-host "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count
                                                                          Logthis "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count; Set-Logpath }
                                                                        }
                                                                    }
                                                        'wca2'{
                                                            switch($collection[-1]){
                                                                {$_ -eq $global:inc[1]}{write-host `
                                                                    "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];
                                                                       Logthis "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1];Set-Logpath exit 0}
                                                                    default{$count++
                                                                        #write-host "WCA2 Insert Failed - Retry DB FeedDate:" $feedate "Seq:"$collection[-1] "Attempt:"$count}
                                                                        write-host "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count
                                                                           LogThis "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count; Set-Logpath}
                                                                        }
                                                                    }
                                                                }
                                                            }
                                            default {$count++
                                                write-host $dbs "FeedDate Incorrect - DB FeedDate:" $feedate "Seq:"$collection[-1] "Retry Attempt:"$count
                                                   #LogThis $dbs,"FeedDate Incorrect - DB FeedDate:", $feedate, "Seq:",$collection[-1], "Retry Attempt:",$count;$global:class='ok'; Set-Logpath}
                                                   LogThis $h/$dbs,"FeedDate Incorrect - Retry Attempt:",$count;$global:class='ok'; Set-Logpath}
                                                #write-host "Sequence failure  - DB FeedDate:" $feedate "Seq:"$collection[-1] - "Retry Attempt:"$count}
                                        }    
                                    $myconnection.Close();start-sleep -Seconds 05}Until($count -eq '3')
                            }
                    }
#}    
#$myconnection.Close()
#$count=$null
#$action
#$file='O:\AUTO\Tasks\TEST.tsk'
#$queue='-auxilliary4'
#$trig='j:\java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar --config=o:\AUTO\configs\global.cfg -taskfile O:\AUTO\Tasks\TEST.tsk -auxilliary2'
#j:\java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar "--config=o:\AUTO\configs\global.cfg" -taskfile "O:\AUTO\Tasks\TEST.tsk" -auxilliary2
#j:\java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar "--config=o:\AUTO\configs\global.cfg" -taskfile $tf $queue
#j:\java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar "--config=o:\AUTO\configs\global.cfg" -taskfile O:\AUTO\Tasks\TEST.tsk -auxilliary2
#$reader=$null
#}Until($count -eq '3')
#------------------------- Null Results -------------------------#
#$collection=$null
#$dbs=$null
#$ii=$null
#$pwd=$null
#$af=Start-Process

#---------------------- Check Argument -------------------------#
    If ((!($h))){
            Write-Host 'No DB Server Specified!'
                    msgbox 'No DB Server Specified!'
    }
    ElseIf ((!($dbs))){
            Write-Host No Database specified!
                    msgbox 'No Database Specified!'
    }
    ElseIf ((!($action))){
            Write-Host No Action specified!
                    msgbox 'No Database Specified!'
    }
    Else{#Start-Process echoArgs.exe -ArgumentList \java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar "--config=o:\AUTO\configs\global.cfg" -taskfile "O:\AUTO\Tasks\TEST.tsk" -auxilliary4}
        switch ($action){
                'trig'{switch($count){
                     3{j:\java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar "--config=o:\AUTO\configs\global.cfg" -taskfile $file $queue
                     Logthis "Error! Failed to update DB -", $feedate, "Seq:",$collection[-1];$global:class='failed'; Set-Logpath
                     #Logthis "TaskFile: $file triggered to Queue:$queue";$global:class='note'; Set-Logpath}
                     #Logthis "Triggered TF: $file - Queue:$queue";$global:class='note'; Set-Logpath}
                     Logthis "TaskFile Triggered! - TF: $file - Queue:$queue";$global:class='note'; Set-Logpath}
                default{'Nothing done';break}
                    }
                }
            }
    }

$count=$null