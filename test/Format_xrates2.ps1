﻿#* FileName: Format_Xrates.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Scritp Purpose:  Periodically check DB for Scheduler/Task state
#* Detaied version of check_db.ps1. To be used from scheduler/taskfile
#* check_db is designed for Nagios use - minimal output
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [29/01/2014]
#* Time: [15:00]
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: 
#
#
# =============================================================================

#########################
### Format Rates File ###
# Need a format step to do the following
# Add 3 columns - 1) Date; from filename#, 2) Source; from filename, 3) Base; always USD.
# Add field hearders; 1) currency, 2) ra#te 3) feeddate, 4) base, 5) src
# Add Euro legacy currencies - List:

## Set path variables
#------------------ Live Path ------------------#
#$csvpath="O:\Datafeed\Xrates\"
#$legacy= "O:\Datafeed\Xrates\USD-Legacy-Curr.txt"
#$csvpath=$args[0]

#------------------ Test Path ------------------#
$csvpath="c:\pstest\Datafeed\Xrates\"
$legacy= "c:\pstest\Datafeed\Xrates\USD-Legacy-Curr.txt"

#Append file content if sourcename matches stated 
$base="USD"
$ap=(gc $legacy)

#$xrate=gci $csvpath -Filter *.csv | %{$a=$_.BaseName; $b=$_.fullname
        #$fn=$a.split("_")#;write-output $($fn[0]) ($fn[1])
            #}

gci $csvpath -Filter *.csv | tee -Variable xrate | %{$a=$_.BaseName; $b=$_.fullname
        $fn=$a.split("_")#;write-output $($fn[0]) ($fn[1])
            }

switch (([string]$xrate).split('')){
    {$_.startswith('*ECB*')}{$dh=(gc $b.Split('*}*') | ?{$_ -notlike '*}*'} | %{$_.replace(': ',',')})
                            $dh= $dh[6..($dh.count)]
        }
    {$_.startswith('*OER*')}{$dh=(gc $b)
                           $dh= $dh[2..($dh.count)] 
        }
    default{'You failed son!!'}
}
                 
    ac $b -Value $ap
    $dh > $b
                        
    #Append full format to csv file
    $fc= $b | ipcsv -header Curr,Rate,Feeddate,base,Src 
    $fc | % {$_.src=$fn[0];$_.Feeddate=$fn[1];$_.base=$base;};
                        
    ##Export csv/convert to csv without additional formating
    $b=$fc | convertto-csv -notypeinformation | %{$_.Replace('"','')}| out-file $b -force -en ascii