﻿#* FileName: CDSsftp.ps1
#*=============================================================================
#* Script Name: [CDS SFTP]
#* Created: [25/06/2013]
#* Author: Seb Sharr, Damar Johnson
#* Company: Exchange Data International
#* Email: s.sharr@exchange-data.com, d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts: *FTP password encryption set for OPSVR004 ONLY*
#* Keywords:
#*=============================================================================
#* Purpose: Listen for CDS file from .COM FTP server and upload to .NET
#* 
#* Must be run from OPSVR004 ONLY!
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [10/07/2013]
#* Time: [12:00]
#* Issue: Original logging set by Seb from file server, need OP's solution
#* Solution: Ammend logging to point to OP's log centre for Fixed Income
#* Issue: No error handling or retry attempt's
#* Solution: Add retry attempt and correct upload errors monthly vs daily
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: LOGFILE, LOGTHIS, THISFTP, UPLOADFTP, DOWNLOADFTP, PROCESSTHISFILE
# Created: [....]
# Author: Seb Sharr, Damar Johnson
# Arguments:
# =============================================================================
# Purpose: 
#
#* Set OPslog centre logging
#* Create FTP connection
#* Upload File
#* Download File
#* Copy or move file to correct path
# =============================================================================

#Import-Module C:\Users\Administrator\Documents\WindowsPowerShell\Modules\PSFTP\PSFTP.psd1
#Import-Module C:\Users\d.johnson\Documents\WindowsPowerShell\Modules\PSFTP\PSFTP.psd1

#------------------------- CDS Log path -------------------------#

#O:\AUTO\Tasks\Fixed_Income\CDS\log\*.log

#------------------------- Filename /Increment -------------------------#
#$ndump=$args[0]
#$tail=$args[1]
$ndump='CDS'
$tail=''

$d=get-date
$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$yd = (Get-Date).AddDays(-1).tostring( "yyMMdd" )
$yyyyd = (Get-Date).AddDays(-1).tostring( "yyyyMMdd" )
$sep='_'
#$global:logvalue="$wdate | $global:thestr"
$global:basedir='O:\Datafeed\CDS'
$global:logdir='O:\AUTO\Tasks\Fixed_Income\CDS\log'
#$global:basedir='C:\cds'
$monthlydown=(Get-Date).Day

$Aclass='ok'
$Bclass='failed'

#Test dates
$dy='130716'

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logdir")}
    default {break}
    }
}

#------------------------- Mail Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\' | ConvertTo-SecureString
#$a=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\EMAIL.txt' | ConvertTo-SecureString
#$b=ConvertTo-SecureString $a -AsPlainText -Force
#$mailpass=$a
#$mailcred=New-Object System.Management.Automation.PsCredential “d.johnson@exchange-data.com“,$mailpass

#------------------------- NET FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
$b=gc 'O:\auto\Configs\FTPServers.cfg' | ?{$_.startswith('DOTNET')}
$c=$b.split("`t")
$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
$NETpass=$d
$NETcred=New-Object System.Management.Automation.PsCredential “channel7“,$NETpass

#------------------------- COM FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
$b=gc 'O:\auto\Configs\FTPServers.cfg' | ?{$_.startswith('DOTCOM')}
$c=$b.split("`t")
$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
$COMpass=$d
$COMcred=New-Object System.Management.Automation.PsCredential “newarch“,$COMpass


#Set-FTPConnection -Credentials $ftpcred -Server ftp://www.exchange-data.net -session MyTestSession -UsePassive 
#$Session=Get-FTPConnection -Session $MyTestSession

#------------------------- NET FTP Session -------------------------#
Set-FTPConnection -Credentials $NETcred -Server ftp://www.exchange-data.net -session NETSession -UsePassive 
$Session=Get-FTPConnection -Session $NETSession

#------------------------- COM FTP Session -------------------------#
Set-FTPConnection -Credentials $COMcred -Server ftp://www.exchange-data.com -session COMSession -UsePassive 
$Session=Get-FTPConnection -Session $COMSession

#------------------------- List FTP items -------------------------#
#$listFTP=Get-FTPChildItem -Session $MyTestSession -Path /custom/bahar/cds/test | ?{$_.Dir -notlike ('d')}
$listNET=Get-FTPChildItem -Session $NETSession -Path /custom/bahar/cds/test | ?{$_.Dir -notlike ('d')}
$listCOM=Get-FTPChildItem -Session $COMSession -Path /custom/bahar/cds/test | ?{$_.Dir -notlike ('d')}
$NETupfile=$listNET.Name #-eq $downval
$COMupfile=$listCOM.Name #-eq $downval


#Send-MailMessage -To "d.johnson@exchange-data.com" -Subject "Test:CDS File Not Found" -From "d.johnson@exchange-data.com" -SmtpServer "smtp.gmail.com" -Credential $mailcred -UseSsl $true -Port 587
#switch ($LASTEXITCODE) {
#'0' {$class='ok'}
#'1' {$class='failed'}
#}

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
#$time=(get-date).hour
### Webload incrementals
       switch ($ndump) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($tail) {
                       'i' {$global:Fname="$jdate$sep$ndump$inc";$global:logpath="O:\auto\logs\$ndump$inc" ;write-host inc exists!;
                                }
                       '' {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$ndump";$global:logpath="O:\auto\logs\$ndump"}
                       }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
        $global:thestr = $args[0]
        $d = (Get-Date).tostring( "yyyy-MM-dd HH:mm:ss" )
        "$wdate | $thestr" | tee -Variable global:logvalue >> ('O:\AUTO\Tasks\Fixed_Income\CDS\log\'+"$jdate"+'_CDS.log')  #$global:basedir\LogCdsPs.txt
        write-output "$d $thestr"
        if ($args[1] -eq "exit") {exit}
    }
    
    function thisftp()
    {
        $thisfile = $args[0]
        LogThis "$thisfile.gz FTP started"
        if (test-path "$global:logdir\resultftp.txt") { remove-item  "$global:logdir\resultftp.txt"}
        #set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
        set-alias cftp "c:\Program Files\coreftp\corecmd.exe"
        #$a = cFtp -OA -B -u  "$thisfile" "sftp://cds_trans:496h5wFs@109.235.145.77" -log "$global:logdir\resultftp.txt"
        $a = cFtp -OA -B -u  "$thisfile" "ftp://channel7:1nc3p:T1oN@109.235.145.77/Test" -log "$global:logdir\resultftp.txt"
        $x = Select-String -Path $global:logdir\resultftp.txt -pattern "Total uploaded files:  1"
        if ( $x -eq $null ) 
        {
            LogThis "ERROR $thisfile SFTP unsuccessful"
            exit 0
        }
        LogThis "$thisfile SFTP successful"
    }
    #------------------------- Upload FTP Function -------------------------#
    function uploadftp()
    {
        $thisfile = $args[0]
        if (test-path "$global:logdir\resultftp.txt") { remove-item "$global:logdir\resultftp.txt"}
        else {}
        #set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
        set-alias cftp "c:\Program Files\coreftp\corecmd.exe"
        #cFtp -OA -B -u  "$thisfile" "sftp://cds_trans:496h5wFs@109.235.145.77/Test" -log "$global:logdir\resultftp.txt"
        cFtp -OA -B -u  "$thisfile" "ftp://channel7:1nc3p:T1oN@109.235.145.77/Test" -log "$global:logdir\resultftp.txt"
        #cFtp -OA -B -u  "$thisfile" "sftp://cds_trans:496h5wFs@109.235.145.77/" -log "$global:logdir\resultftp.txt"
        #Select-String -Path $global:logdir\resultftp.txt -pattern "Total uploaded files:  1" | tee -Variable global:uploadx
}

#------------------------- Download FTP Function -------------------------#
    function downloadftp() {
        $global:thisfile = $args[0]
            if (test-path "$global:logdir\resultftp.txt") { remove-item "$global:logdir\resultftp.txt"}
            else {}
            #set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
            set-alias cftp "c:\Program Files\coreftp\corecmd.exe"
            cFtp -s -d "ftp://newarch:Pr0T:0coL@exchange-data.com/Custom/Bahar/cds/test/$thisfile" -p 'O:\Datafeed\CDS' -log "$global:logdir\resultftp.txt"
            #cFtp -s -d "sftp://edi-cds:3Qhh243n@exchange-data.com/$thisfile" -p 'O:\Datafeed\CDS' -log "$global:logdir\resultftp.txt"
            #Select-String -Path "$global:logdir\resultftp.txt" -pattern "Total downloaded files:  1" | tee -Variable global:x
    }

# ******************** Main ***************************************************************

#------------------------- Resolves:The remote certificate is invalid according to the validation procedure -------------------------#
#ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate)

#------------------------- Monthly Files Download/Upload -------------------------#
switch ($monthlydown) {
#{$_ -eq 2} {downloadftp (("SBUR_" + $yyyyd + ".TXT.gz")  | tee -variable downval_SBUR)
{$_ -ge 2 -and $_ -le 3} {downloadftp (("SBUR_" + $yyyyd + ".TXT.gz")  | tee -variable downval_SBUR)
            $downres=(Test-Path "$global:basedir\$downval_SBUR")
                switch ($x) {
                    {Test-Path "$global:basedir\$downval_SBUR"}  {logthis 'SBUR/Monthly file Successfully Downloaded'}
                    {(!(Test-Path "$global:basedir\$downval_SBUR"))} {Write-Host (LogThis "$downval_SBUR Download Failed");Do {downloadftp ("SBUR" + $yd + ".gz"); "'Retrying download:'$downval' 'Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 120} until($count -eq '2');
                        #Send-MailMessage -To "d.johnson@exchange-data.com" -Subject "Test:CDS File Not Found" -From "d.johnson@exchange-data.com" -SmtpServer "smtp.gmail.com" -Credential $mailcred -UseSsl $true -Port 587
                        O:\AUTO\Apps\Emailers\gmailer.jar -USER "support@exchange-data.com" -PASS "KASIA1" -TO "d.johnson@exchange-data.com"  -SUB "CDS File Not found" #-BODY "EDI Monthly File Not Found: $downval_SBUR `n Please Check & Resend"
                            Do {downloadftp ("SBUR" + $yd + ".gz"); "'Retrying download:'$downval' 'Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 60} until($count -eq '5');
                            $count=$null
                                }
                }
            downloadftp (("SBCR_" + $yyyyd + ".TXT.gz") | tee -Variable downval_SBCR)
            $downres=(Test-Path "$global:basedir\$downval_SBCR")
                switch ($x) {
                    {Test-Path "$global:basedir\$downval_SBCR"}  {logthis 'SBCR/Monthly file Successfully Downloaded'}
                    {(!(Test-Path "$global:basedir\$downval_SBCR"))} {Write-Host (Logthis "$downval_SBCR Download Failed");Do {downloadftp ("SBCR" + $yd + ".gz"); "Retrying download:'$downval Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 120} until($count -eq '2');
                        #Send-MailMessage -To "d.johnson@exchange-data.com" -Subject "Test:CDS File Not Found" -From "d.johnson@exchange-data.com" -SmtpServer "smtp.gmail.com" -Credential $mailcred -UseSsl $true -Port 587
                        O:\AUTO\Apps\Emailers\gmailer.jar -USER "support@exchange-data.com" -PASS "KASIA1" -TO "d.johnson@exchange-data.com"  -SUB "CDS File Not found" #-BODY "EDI Monthly File Not Found: $downval_SBCR `n Please Check & Resend"
                            Do {downloadftp ("SBCR" + $yd + ".gz"); "Retrying download:'$downval Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 60} until($count -eq '5');
                            $count=$null
                                }
                }
                
            uploadftp ("$global:basedir\$downval_SBUR")
            $x=Get-FTPChildItem -Session $MyTestSession -Path /custom/bahar/cds/ | ?{$_.name -like "$downval_SBUR*"}   
                switch ($x.name.count){
                    1 {logthis "$downval_SBUR file Successfully Uploaded";$global:goodup='Uploads:'+$global:count++}
                    0 {logthis "$downval_SBUR Upload failed!";$calss='failed'}
                }
                
            uploadftp ("$global:basedir\$downval_SBCR")
            $x=Get-FTPChildItem -Session $MyTestSession -Path /custom/bahar/cds/ | ?{$_.name -like "$downval_SBCR*"}   
                switch ($x.name.count){
                    1 {logthis "$downval_SBCR file Successfully Uploaded";$global:goodup='Uploads:'+$global:count++}
                    0 {logthis "$downval_SBCR Upload failed!";$calss='failed'}
                }
        }
default {'No monthly files to be downloaded' | tee -variable downval}
}

#------------------------- Daily Files Download/Upload -------------------------#

downloadftp (("EV7041F_" + $yd + ".gz") | tee -variable downval_EV)
#downloadftp (("EV7041F_130802.gz") | tee -variable downval_EV)
$downres=(Test-Path "$global:basedir\$downval_EV")
switch ($x) {
    {Test-Path "$global:basedir\$downval_EV"}  {logthis "$downval_EV file Successfully Downloaded"}
    {(!(Test-Path "$global:basedir\$downval_EV"))} {write-host (Logthis Download Fail);Do {downloadftp ("EV7041F" + $yd + ".gz"); " (1)Retrying download:$downval_EV Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 120 } until($count -eq '2');
        #------------------------- Resolves:The remote certificate is invalid according to the validation procedure -------------------------#
        #ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate)
        #Send-MailMessage -To "d.johnson@exchange-data.com" -Subject "Test1:CDS File Not Found" -From "d.johnson@exchange-data.com" -SmtpServer "smtp.gmail.com" -Credential $mailcred -UseSsl $true -Port 587
        O:\AUTO\Apps\Emailers\gmailer.jar -USER "support@exchange-data.com" -PASS "KASIA1" -TO "d.johnson@exchange-data.com"  -SUB "CDS File Not found" #-BODY "Daily File Not Found: $downval_EV"
           Do {downloadftp ("EV7041F" + $yd + ".gz"); " Retrying download:$downval_EV Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 120 } until($count -le '5');
           $count=$null                   
                   }
}

downloadftp (("SM7031F_" + $yd + ".gz") | tee -variable downval_SM)
#downloadftp (("SM7031F_130802.gz") | tee -variable downval_SM)
$downres=(Test-Path "$global:basedir\$downval_SM")
switch ($x) {
    {Test-Path "$global:basedir\$downval_SM"}  {logthis "$downval_SM file Successfully Downloaded"}
    {(!(Test-Path "$global:basedir\$downval_SM"))} {write-host (Logthis CDS Download Fail);Do {downloadftp ("SM7031F" + $yd + ".gz"); "(2)Retrying download:$downval_SM Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 120 } until($count -eq '5');
        #Send-MailMessage -To "d.johnson@exchange-data.com" -Subject "Test:CDS File Not Found" -From "d.johnson@exchange-data.com" -SmtpServer "smtp.gmail.com" -Credential $mailcred -UseSsl $true -Port 587
        O:\AUTO\Apps\Emailers\gmailer.jar -USER "support@exchange-data.com" -PASS "KASIA1" -TO "d.johnson@exchange-data.com"  -SUB "CDS File Not found" #-BODY "Daily File Not Found: $downval_SM"
            Do {downloadftp ("SM7031F" + $yd + ".gz"); "Retrying download:$downval_SM Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 120 } until($count -le '5');
            $count=$null
                   }
}

    #uploadftp ("O:\Datafeed\CDS\EV7041F_" + $yd + ".gz")
    uploadftp ("O:\Datafeed\CDS\$downval_EV")
    $x=Get-FTPChildItem -Session $MyTestSession -Path /custom/bahar/cds/ | ?{$_.name -like "$downval_EV*"}   
    switch ($x.name.count){
        1 {logthis "$downval_EV file Successfully Uploaded";$global:goodup='Uploads:'+$global:count++}
        0 {logthis "$downval_EV file failed to Uploaded!"}
    }

    #uploadftp ("O:\Datafeed\CDS\SM7031F_" + $yd + ".gz")
    uploadftp ("O:\Datafeed\CDS\$downval_SM")
    $x=Get-FTPChildItem -Session $MyTestSession -Path /custom/bahar/cds/ | ?{$_.name -like "$downval_SM*"}   
    switch ($x.name.count){
        1 {logthis "$downval_SM file Successfully Uploaded";$global:goodup='Uploads:'+$global:count++}
        0 {logthis "$downval_SM file failed to Uploaded!"}
    }

    #------------------------- Verify Upload -------------------------#
        switch ($goodup[8]) {
            {$_ -ge '1'} {logthis "Both Daily files Successfully Uploaded: $downval_EV | $downval_SM! $downval";$class=$Aclass}
            {$_ -eq '0'} {logthis "1  of 2 Daily files Successfully Uploaded! $downval"; $class=$Bclass}
            default {logthis 'Both Daily files Upload Failed!';$downval;$class=$Bclass}
        }

$goodup=$null

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            { write-host No logpath found! Creating Directory; mkdir $logpath }
        Else
            {Write-Host Current logpath exists}    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                { write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$class'>$logvalue</span></p>"}
                Else
                { write-host Appending to file!; ac -Path "$logpath\$Fname.html" -Value "<p><span class='$class'>$logvalue</span></p>" -Force}
}

Get-Increment
Set-Logpath

exit $LASTEXITCODE