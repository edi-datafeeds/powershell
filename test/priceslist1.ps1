﻿
## Script output path & static path to Prices files
$prices='c:\prices_list.csv'
$prices_dir='Y:\FeedProc\P04'

## Prices Arrays
$complete_list=@()
$dir_list=@()
$price_list=@()

(gci -recurse $prices_dir) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice'} | %{$_.fullname}
#(gci -recurse $prices_dir) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice'} | %{$_.fullname} | epcsv -NoTypeInformation $prices

### Split path from file
(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice*'} | %{$_.fullname}


## Complete results for Full list
$complete_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | %{$_.FullName} | ?{$_ -notlike '*\LivePrice\*'} | ?{$_ -notlike '*\ScottTest\*'}
$complete_list | %{$_ | Add-Member -MemberType NoteProperty -Name Localpath -Value ($_ | Split-Path -Parent) | 
    %{$_ | Add-Member -MemberType NoteProperty -Name File -Value ($_ | Split-Path -Leaf) |
         %{$_ | Add-Member -MemberType NoteProperty -Name Extension -Value (($_ | Split-Path -Leaf).Split('.')) |
            %{$_ | Add-Member -MemberType NoteProperty -Name Prefix -Value (($_ | Split-Path -Leaf).split('\w$'))}
        }
    }
}
$complete_list | Select-Object -Property ID,Prefix,File,Suffix,Extension,Localpath,Filename,Remote | epcsv -NoTypeInformation $prices

## DIR results for parents
#$local=$complete_list | %{$_ | Split-Path -Parent | Add-Member -MemberType NoteProperty -Name Local}
#$dir_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice*'} | %{$_ | Split-Path -Parent} | %{$_.FullName}
#$dir_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike '\LivePrice\*'} | %{$_.FullName}
#$dir_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | %{$_.FullName} | ?{$_ -notlike '*\LivePrice\*'} | ?{$_ -notlike '*\ScottTest\*'}

## Prices results for Files
#$price_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice*'} | %{$_ | Split-Path -leaf}
#$Prices_file=$complete_list | %{$_ | Add-Member -MemberType NoteProperty -Name Filename -Value ($_ | Split-Path -Leaf)}

## Sample from file comparison PS1
#$neutrochild,$NewTotals | Select-Object -property LoginID,Dir,VirtualPath,Inconsistency,'Total Files','Missing from .NET','Extra on .NET' | epcsv -NoTypeInformation $Report
#$neutro | Add-Member -MemberType NoteProperty -Name LoginID -Value $puppy[0]