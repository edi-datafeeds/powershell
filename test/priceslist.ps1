﻿$prices='c:\prices_list.csv'

$prices_dir='Y:\FeedProc\P04'

$complete_list=@()
$dir_list=@()
$price_list=@()

#gci -Recurse $prices_dir | ?{$_.Mode -like 'd*'}

#$prices_dir | %{$_.Name | gci -Recurse}

#(gci $prices_dir) | ?{$_ | gci -Recurse}

#(gci -recurse $prices_dir) | ?{$_.Mode -like '-a--*'} | %{$_.fullname}

(gci -recurse $prices_dir) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice'} | %{$_.fullname}

(gci -recurse $prices_dir) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice'} | %{$_.fullname} | epcsv -NoTypeInformation $prices


### Split path from file
(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice*'} | %{$_.fullname}


## Complete results for Full list
#$dir_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice*'} | %{$_ | Split-Path -Parent} | %{$_.FullName}
$complete_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | %{$_.FullName} | ?{$_ -notlike '*\LivePrice\*'} | ?{$_ -notlike '*\ScottTest\*'}

## DIR results for parents
#$local=$complete_list | %{$_ | Split-Path -Parent | Add-Member -MemberType NoteProperty -Name Local}
$complete_list | %{$_ | Add-Member -MemberType NoteProperty -Name Localpath -Value ($_ | Split-Path -Parent)}
$Prices_file=$complete_list | %{$_ | Add-Member -MemberType NoteProperty -Name Filename -Value ($_ | Split-Path -Leaf)}
$complete_list | Select-Object -Property ID,Prefix,Filename,Suffix,Extension,Localpath,Filename,Remote | epcsv -NoTypeInformation $prices

#$neutrochild,$NewTotals | Select-Object -property LoginID,Dir,VirtualPath,Inconsistency,'Total Files','Missing from .NET','Extra on .NET' | epcsv -NoTypeInformation $Report
#$neutro | Add-Member -MemberType NoteProperty -Name LoginID -Value $puppy[0]

#$dir_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice*'} | %{$_ | Split-Path -Parent} | %{$_.FullName}
#$dir_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike '\LivePrice\*'} | %{$_.FullName}
#$dir_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | %{$_.FullName} | ?{$_ -notlike '*\LivePrice\*'} | ?{$_ -notlike '*\ScottTest\*'}


## Prices results for Files
#$price_list+=(gci $prices_dir -Recurse) | ?{$_.Mode -like '-a--*'} | ?{$_ -notlike 'LivePrice*'} | %{$_ | Split-Path -leaf}