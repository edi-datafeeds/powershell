﻿########## Script to determine if x process has run on schedule ##########

#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\PSloader.log -Append -Force

####### Range & Treshold settings ##################################
#Metric={time},ok={05},warn={10},crit={15},unit={Minute},prefix={}

### Threshold Range ###
$Start=get-date 
$Finish=$Start.AddMinutes(15)

    ### Set functions ###
    function Get-OpsState ($x='wscript') {
            Get-WmiObject win32_process -Filter "Name like '%$x%'"  #| Select-Object CommandLine
    }

### Captures commandline of running process ###
#$Goods=$args[1]
$Goods=Get-OpsState
$Tcommandline=$Goods | Select-Object CommandLine


#$date=(get-date).Hour
##Variable INC
#$INC1=INC_1
#$INC1=(Get-Date).Hour -lt 12 -ge 11
#$INC2=(Get-Date).Hour -lt 14 -ge 13
#$INC3=(Get-Date).Hour -lt 16 -ge 15

### Set increment number ###
#function INC_1 {
#    If ((Get-Date).Hour -lt 12) #{Write-Host success; }#Exit 1}
#    else {writehost flop aroo; exit 1}
#    }

#function INC_2 {
#    If ((Get-Date).Hour -lt 15 -gt 14) {$INC='INC 2'}
#    else {writehost flop aroo2; exit 1}
#    }

#function INC_3 {
#    If ((Get-Date).Hour -lt 18 -gt 17) {$INC='INC 3'}
#    else {writehost flop aroo3; exit 1}
#    }


### threshold functions ###
        function NOVALUE {
            if ((get-date).hour -lt 9 -ge 8) { #{write-host 'No Loader Found'; exit 1}
                    If ($Dur -ge $Start.AddMinutes(2)) {Write-Host 'Test:WCA Loader is delayed'; exit 1}
                    elseif ($Dur -ge $Start.AddMinutes(3)) {Write-Host 'Test:WCA Loader has failed to Initialise'; exit 2}
                        }

            Elseif ((get-date).hour -lt 15 -ge 14) {
                    If ($Dur -ge $Start.AddMinutes(10)) {Write-Host 'Test:WCA Loader is delayed'; exit 1}
                    elseif ($Dur -ge $Start.AddMinutes(14)) {Write-Host 'Test:WCA Loader has failed to Initialise'; exit 2}
                        }

            Elseif ((get-date).hour -lt 19 -ge 18) {
                    If ($Dur -ge $Start.AddMinutes(2)) {Write-Host 'Test:WCA Loader is delayed'; exit 1}
                    elseif ($Dur -ge $Start.AddMinutes(14)) {Write-Host 'Test:WCA Loader has failed to Initialise'; exit 2}
                        }

            #If ($Dur -ge $Start.AddMinutes(05)) {Write-Host 'No Loader Found'; exit 1}
                    #elseif ($Dur -ge $Start.AddMinutes(10)) {Write-Host 'No Loader Found'; exit 2}
                    #else {Write-Host 'TEST:No Loader Found'; exit 2}
                        #}
                                
            Elseif ((get-date).hour -lt 10 -ge 9) {
                    If ($Dur -ge $Start.AddMinutes(10)) {Write-Host 'Test:MyDEV_WCA Loader is delayed'; exit 1}
                    elseif ($Dur -ge $Start.AddMinutes(14)) {Write-Host 'Test:MyDEV_WCA Loader has failed to Initialise'; exit 2}
                        }

            Elseif ((get-date).hour -lt 16 -ge 15) {
                    If ($Dur -ge $Start.AddMinutes(10)) {Write-Host 'MyDEV_WCA Loader is delayed'; exit 1}
                    elseif ($Dur -ge $Start.AddMinutes(14)) {Write-Host 'MyDEV_WCA Loader has failed to Initialise'; exit 2}
                        }

            Elseif ((get-date).hour -lt 20 -ge 19) {
                    If ($Dur -ge $Start.AddMinutes(10)) {Write-Host 'MyDEV_WCA Loader is delayed'; exit 1}
                    elseif ($Dur -ge $Start.AddMinutes(14)) {Write-Host 'MyDEV_WCA Loader has failed to Initialise'; exit 2}
                        }
}

        function WCA {
            #if ((get-date).hour -lt 12 -gt 11) {
                    #If ($Dur -ge $Start.AddMinutes(10)) {Write-Host 'WCA Loader is delayed'; exit 1}
                    #elseif ($Dur -ge $Start.AddMinutes(14)) {Write-Host 'WCA Loader has failed to Initialise'; exit 2}
                    If ($Dur -lt $Start.AddMinutes(14)) {Write-Host 'WCA Loader Successfully Initiated!'; exit $LASTEXITCODE}
                    else {Write-Host 'WCA Loader Successfully Initiated!'; exit $LASTEXITCODE}    
        }
        
        Function MyWCA {
            #If ($Dur -ge $Start.AddMinutes(10)) {Write-Host 'Loader is delayed'; exit 1}
            #elseif ($Dur -ge $Start.AddMinutes(14)) {Write-Host 'Loader has failed to Initialise'; exit 2}
            If ($Dur -lt $Start.AddMinutes(14)) {Write-Host 'MyWCA Loader has successfully Initiated!'; exit $LASTEXITCODE}
            else {Write-Host 'WCA Loader Successfully Initiated!'; exit $LASTEXITCODE}    
        }

        Function MyDEV_WCA {
                    #If ((get-date).hour -lt 10 -gt 9) {
                            #If ($Dur -ge $Start.AddMinutes(10)) {Write-Host 'MyDEV Loader is delayed'; exit 1}
                            #elseif ($Dur -ge $Start.AddMinutes(14)) {Write-Host 'MyDEV Loader has failed to Initialise'; exit 2}
                            If ($Dur -lt $Start.AddMinutes(14)) {Write-Host 'MyDEV Loader has successfully Initiated!'; exit $LASTEXITCODE}
                            else {Write-Host 'MyDEV_WCA Loader Successfully Initiated!'; exit $LASTEXITCODE}    
        }

$Start | Out-Null

Do {
    ### Set process/loader run times ###
    $Dur=Get-Date
          
    #If ($INC1 -eq 'true') {        
           
           if ($Tcommandline -eq $null) {
           NOVALUE}
    else { 
            switch -Wildcard ($Tcommandline) {
                    "*WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i*" {WCA;}# break}
                    "*WCA a MyDev WcaWebload MyDev i*" {MyDEV_WCA;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_1 DIESEL*" {MyWCA_1;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL*" {MyWCA_2;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_3 DIESEL*" {MyWCA_3;}# break}
                    }
                            
            switch -Wildcard ($Tcommandline) {
                    "*WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i*" {WCA;}# break}
                    "*WCA a MyDev WcaWebload MyDev i*" {MyDEV_WCA;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_1 DIESEL*" {MyWCA_1;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL*" {MyWCA_2;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_3 DIESEL*" {MyWCA_3;}# break}
                    }     
            
            switch -Wildcard ($Tcommandline) {
                    "*WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i*" {WCA;}# break}
                    "*WCA a MyDev WcaWebload MyDev i*" {MyDEV_WCA;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_1 DIESEL*" {MyWCA_1;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL*" {MyWCA_2;}# break}
                    "*WCA a HAT_MY_Diesel MyWcaWebload_3 DIESEL*" {MyWCA_3;}# break}
                    }    

}
            ### Verify process/loader is in a running state ### 
            #switch -Wildcard ($Tcommandline) {
            #$null {write-host 'WCA Loader has failed to start';exit 1;break}
            #"*WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i*" {WCA;}# break}
            #"*WCA a MyDev WcaWebload MyDev i*" {MyDEV_WCA;}# break}
            #"*WCA a HAT_MY_Diesel MyWcaWebload_1 DIESEL*" {MyWCA_1;}# break}
            #"*WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL*" {MyWCA_2;}# break}
            #"*WCA a HAT_MY_Diesel MyWcaWebload_3 DIESEL*" {MyWCA_3;}# break}
            #   }
        
Start-Sleep -Seconds 10
}
Until ($Dur -ge $Finish) 

#Stop-Transcript

#switch ($INC) {
#(Get-Date).Hour {write-host 'FLOP AM!!'}
#(Get-Date).AddHours(2) {write-host 'FLOP NOON!!'}
#(Get-Date).AddHours(4) {write-host 'FLOP PM!!'}
#}

#$INC=(Get-Date).DateTime
#if ($INC -match $INC1) {
#Write-Host 'Flavor Flav'
#}
#else
#{
#Write-Host "NO caN DO CAP'N"
#}

#if ((get-date).hour -lt 12 -gt 11)

#$INCtest=(Get-Date).Hour

#SWITCH ($INCtest -) {
#$INC1 {Write-Host success}
#$INC2 {Write-Host success2}
#$INC3 {Write-Host success3}
#default {write-host Dooky}
#}


#$king=(date).Hour
#switch ($king) {
#($_ -lt 15) {Write-Host Bingo}
#($_ -lt 10) {Write-Host Bingo2}
#($_ -lt 13) {Write-Host Bingo3}
#}