﻿#* FileName: Format_THoldSsec.ps1
#*=============================================================================
#* Script Name: [Format_THoldSsec]
#* Created: [25/02/2014]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Scritp Purpose:  
#* Format Format_THoldSsec files for import in DB
#* 
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [13/05/2014]
#* Time: 
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/02/2014]
# Author: Damar Johnson
# Arguments: Source, Inc, logfile, type
# =============================================================================
# Purpose: 
# Convert downloaded file into DB importable format
#
# =============================================================================

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$true,Position=0,
ParametersetName='Action'
)]
#[switch]

[Parameter(
mandatory=$true
)]
#[string]$a, #Action to perform

#[Parameter(
#Mandatory=$true
#)]
[string]$s, #Source path of original file

[Parameter(
Mandatory=$true
)]
[string]$d, #Destination of original file

[parameter(
mandatory=$true
)]
[string]$t, # Type of file 

[int]$age,

[parameter(
mandatory=$false
)]
[string]$i, #Increment

[parameter(Mandatory=$true)]
[string]$logfile='OPS-test',

[parameter(
mandatory=$false
)]
[string]$alert

)
#------------------------- Set alert function -------------------------#

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

$action=$a
$source=$s
#$dest=$d
#$dest=($d+'\'+$c+'.csv')
$type=$t
#$age {$age}

$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'
$global:class='ok'

$global:logvalue=@()
$global:source=$source
$archive=$s

#([string]$tholdsec).split('') | ?{$_.startswith('OER')}

#########################
### Format Rates File ###
# Need a format step to do the following
# Add 3 columns - 1) Date; from filename#, 2) Source; from filename, 3) Base; always USD.
# Add field hearders; 1) currency, 2) ra#te 3) feeddate, 4) base, 5) src
# Add Euro legacy currencies - List:

## Set path variables
#------------------ Live Path ------------------#
#$csvpath="O:\Datafeed\TholdSec\2014\05"
#$csvpath=$args[0]

#Format TholdSec ECB+OER=powershell.exe O:\AUTO\Scripts\Powershell\Format_tholdec.ps1 -t csv -s O:\Datafeed\TholdSec\2014\05\test -logfile tholdsec
#O:\AUTO\Scripts\Powershell\Format_tholdec.ps1 -t csv -s O:\Datafeed\TholdSec\2014\05\test -d O:\Datafeed\TholdSec\2014\05\test\datafeed -logfile tholdsec

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                't15022_inc' 
                #'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 13 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                            }
                #------------------------- t15022 incrementals values -------------------------#
                #'t15022_inc' 
                    #{
                        #switch ($time) {
                            #{ $_ -ge 13 -and $_ -le 17} {$global:inc='_2';$global:fileinc='153000'}
                            #{$_ -ge 18 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            #default {$global:inc='_1';$global:fileinc='083000'}
                                #}
                                #$global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                #$global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                        #}
                            #}
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 13 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        write-output "$d $displaytxt" #;exit
        $global:logvalue
    }

#------------------ Test Path ------------------#
#$source="O:\Datafeed\TholdSec\2014\05\test"
#$source="C:\PS\TholdSec\2014\05\test"

#------------------ Live Path ------------------#
#$source="O:\Datafeed\TholdSec\2014\05"
#$source=$args[0]
#$file='O:\Datafeed\TholdSec\formatted\tmp2\holdfile.csv'
$file='O:\Datafeed\TholdSec\formatted\tmp2\holdfile.txt'

#$tholdsec=@()
$tholdsec=gci $source -Filter *.txt #| %{$a=$_.BaseName; $b=$_.fullname};$fn=$a.split("_")#;write-output $($fn[0]) ($fn[1])}

$total=$tholdsec.count

LogThis "$total`: File(s) Pending format... | Source:$source";$global:class='ok';Get-Increment;Set-Logpath

$tholdsec | %{$_.fullname |gci | tee -Variable a | gc | tee -Variable b;
    $conta=gc $a
    #$conta=$conta[1..($conta.count)]
    $conta=$conta | ?{$_ -ne $conta[-1]} | ?{$_ -ne $conta[0]}
    #$conta > $a
    $c=$a.BaseName
    $a=$a.FullName
    
    #$desta=($d+'\'+$c+'.csv')
    #$desta=($d+'\'+$c+'_countA.txt')
    #$destb=($d+'\'+$c+'_countB.txt')
    
    #$desta=($d+'\'+$c+'.csv')
    $desta=($d+'\'+$c+'.txt')
    $destb=($d+'\'+$c+'1.txt')
    
    ipcsv $a -Delimiter '|' -Header "Symbol","Security Name","Markit Category" | epcsv -Path $file -NoTypeInformation
    #$contb=gc $a
    #$contb=$contb | ?{$_ -ne $contb[0]}
    #$contb > $a | epcsv -Path $dest -NoTypeInformation

    $contb=gc $file
    #$contb=$contb | ?{$_ -ne $contb[0]} #| ?{$_.trim() -ne "\n"}
    #$contb > $file
    #$contb | %{$_ -replace ',',"`t"} | %{$_ -replace '"',""} | sc $destb
    
    #$dest | epcsv -Path $dest -NoTypeInformation
    #ipcsv $dest | epcsv -Path O:\Datafeed\TholdSec\2014\05\test\datafeed\thisfile2.csv -NoTypeInformation
    #ipcsv $file | epcsv -Path $dest -NoTypeInformation
    
    #$contb | ?{$_.Replace("|","`t")} |sc $destb
    #rm $file
    #$contb > O:\Datafeed\TholdSec\2014\05\test\datafeed\thisfile.csv
    #ipcsv O:\Datafeed\TholdSec\2014\05\test\datafeed\thisfile.csv -Delimiter '|' -Header "Symbol","Security Name","Markit Category" |
        #epcsv -Path O:\Datafeed\TholdSec\2014\05\test\datafeed\thisfile2.csv -NoTypeInformation
    
    #$conta | sc $desta
    #$conta | %{$_.replace("|","`t")} | sc $destb

    $obj=New-Object psobject

    #$index=%{($conta).split("|")}
    
    #$conta | %{$_.replace("|","`t")} | %{$_.split()}
    
    #[System.Collections.ArrayList] $contc=%{$conta}
    #[System.Collections.ArrayList] $contc=%{$index}

    #[System.Collections.ArrayList] $test3 = gc O:\Datafeed\TholdSec\formatted\tmp3\20140523_nasdaqth.txt
    
    $conta | %{$_.replace("|","`t")} | sc $desta                   

    [System.Collections.ArrayList] $test3 = gc $desta

    foreach ($x in $test3) {$x.Split("`t") | tee -variable testvar2;[string]$testvar2[0..2] >> $destb}

    #$puppy=%{($test2.InputObject).split("`t")}

    #$contb | ?{$_ -ne $contb[0]} | %{$_ -replace ',',"`t"} | %{$_ -replace '"',""} | sc $destb
    #$cont | ConvertTo-Csv
    #$cont | epcsv -Delimiter '|' -Path $dest -NoTypeInformation
    #ipcsv $a -Delimiter '|' -Header " "," "," " | epcsv -Path $dest -NoTypeInformation
    #$a | epcsv -Path 'C:\PS\TholdSec\2014\05\test\datafeed\Nametest.csv'
    #$csv=$a | ipcsv -Path 'O:\Datafeed\TholdSec\2014\05\test\2014-05-12.txt'
    LogThis "$c.$type formatted => $destb";$global:class='Report';Get-Increment;Set-Logpath
    }
    #ipcsv 'O:\Datafeed\TholdSec\2014\05\test\2014-05-12.txt' -delimiter '|' -Header 1,2,3,4,5,6,7 | epcsv -Path 'C:\PS\TholdSec\2014\05\test\datafeed\Nametest4.csv' -NoTypeInformation

LogThis "$total`: File(s) successfully formatted | Source:$source";$global:class='ok';Get-Increment;Set-Logpath

#$neutro | Add-Member -MemberType NoteProperty -Name LoginID -Value $puppy[0]

#$dest=$d+'\'+$c+'.csv'
#$tholdsec=$null
#$cont=$null
#$a=$null
#$b=$null
#$c=$null
$a
$b
$c
$cont
$conta
$contb
#Get-Increment
#Set-Logpath

exit $LASTEXITCODE