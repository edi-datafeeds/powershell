# 05/10/2012 - DJ - Copy Source to destination based on file created or modified today

#test path variable
#$csvpath="C:\Users\d.johnson\Desktop\Xrates\test\"
#$legacy= "C:\Users\d.johnson\Desktop\Xrates\archive"

##Path & destination passed by line arguments
$a=arg[0]
$b=arg[1]

#Copy contents if lastwritetime is today
gci $a | ? {$_.lastwritetime -gt (get-date).date} | cpi -destination $b