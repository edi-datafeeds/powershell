    $cty = 'O:\Datafeed\ECO\CTY\'
    $ref = 'O:\Datafeed\ECO\REF\'
    $chg = 'CHG\'
    $hst = 'HST\'
    $zip = 'ZIP\'
    #$today = $(Get-Date �f "yyyyMMdd")
    $feedday = '20170208'
    $ssource = $cty + $CHG +'*.txt'
    $sdest = $cty + $zip + "eco_" + $today + "_CTY_CHG.zip"
    If(Test-path $sdest) {Remove-item $sdest}  #zip file must NOT already exist
    echo $ssource
    echo $sdest
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe" #creates an alias to location of 7zip so you can use it without specifying that horrendous path  
    $a = sz a -tzip "$sdest" "$ssource"  # $a stores result of running 7zip
                                         # sz use alias created above. 
                                         # a=create archive. 
                                         # -tzip type=zip 
                                         # dest and source are in reverse order for some unknown reason
    $x = Select-String -InputObject $a -pattern "Everything is Ok"
    if ( $x -ne $null ) {
        echo "Everything is Ok found at bottom of program output"
    } else {
        echo "zip failed"
    } 
