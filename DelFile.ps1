 #   G e n e r i c   s c r i p t   t   c o p y   c o n t e n t s   o f   d i r e c t o r y   t o   d e s t i n a t i o n  
 #   S o u r c e   a n d   d e s t i n a t i o n   s u p p l i e d   v i a   C L   a r g u m e n t s  
  
 # t e s t   d i r e c t o r y  
 $ p a t h = ' C : \ u s e r s \ d . j o h n s o n \ D e s k t o p \ t s o u r c e \ '  
 # $ d e s t i n a t i o n = ' C : \ U s e r s \ d . j o h n s o n \ D e s k t o p \ d e s t \ '  
  
 # w o r k i n g   d i r e c t o r i e s  
 $ p a t h = $ a r g s [ 0 ]  
  
 $ s o u r c e m o n e y = g c i   $ p a t h   - f i l t e r   * . t x t  
 $ d e s t i n a t i o n = $ a r g s [ 1 ]  
  
 #   E x e c u t e   c o p y   c o m m a n d  
 c d   $ p a t h ;   d e l   $ s o u r c e m o n e y