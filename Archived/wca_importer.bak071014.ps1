﻿#* FileName: Daily_importer.ps1
#*=============================================================================
#* Script Name: [File Handler]
#* Created: [06/10/2014]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: Import Bahar WCA EOD file
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [06/10/2014]
#* Time: [12:00]
#* Issue: 
#* Solution:
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function:
# Created: [06/10/2014]
# Author: Damar Johnson
# Arguments: Source, Destination, Filetype, Fileage
# =============================================================================
#
#
# =============================================================================

#powershell.exe O:\AUTO\Scripts\Powershell\wca_importer.ps1 dd/MM/yyyy -logfile WCA_DailyImport

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$true,Position=0,
ParametersetName='Action'
)]
#[switch]

[Parameter(
mandatory=$true
)]
[string]$d, #Set date for files to import

[parameter(Mandatory=$false)]
[string]$logfile='OPS-test1'

)

#------------------------- Set alert function -------------------------#

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

#------------------------- DB Logging -------------------------#
## Set MySQL Connectiion
[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection

$server='192.168.12.109'
$id='sa'
$pwd='K376:lcnb'
$db='AutoOps'
$tbl='opslog'

#------------------------- DB connections -------------------------#
## Default
$myconnection.Connectionstring="server=$server;database='AutoOps';Persist Security Info=false;user id=$id;pwd=$pwd"

$myconnection.open()
#------------------------- Queries -------------------------#
$command=$myconnection.CreateCommand()
$command.CommandText=$run
#------------------------- Static variables -------------------------#
$action=$a
$source=$s
$date=$d #| ?{$_ -replace('YYYYMMDD',$jdate)}
$type=$t

$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$ddate=(get-date).tostring("yyyy-MM-dd")
$date=(get-date).ToString("dd/MM/yyyy")
$wdate=get-date -Format G
$sep='_'
$global:class='ok'

$d=$date.Replace('dd/MM/yyyy',"$date")

$global:logvalue=@()

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                't15022_inc' 
                #'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 13 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                            }
                #------------------------- t15022 incrementals values -------------------------#
                'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 13 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                            }
                #'t15022_inc' 
                    #{
                        #switch ($time) {
                            #{ $_ -ge 13 -and $_ -le 17} {$global:inc='_2';$global:fileinc='153000'}
                            #{$_ -ge 18 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            #default {$global:inc='_1';$global:fileinc='083000'}
                                #}
                                #$global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                #$global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                        #}
                            #}
                #------------------------- CABTrans incrementals values -------------------------#
                'xdes' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1';$global:fileinc='07'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2';$global:fileinc='10'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3';$global:fileinc='15'}
                        default {$global:inc='_1';$globalfile:inc='07'}
                        }
                    }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 13 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc";$global:outfile='y' ;write-host inc exists!;
                            }
                       default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        $logdate = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        write-output "$d $displaytxt" #;exit
        $global:logvalue
    }

gci '\\192.168.12.4\share_1\wca' | ?{$_.Extension -eq "*$date.EOD"}

new-psdrive -name Z -PSProvider FileSystem -Root \\192.168.12.4\share_1\wca
#Invoke-Item z:
cd z:
Set-Alias daily_imp "z:\dailyimp.exe"
daily_imp $date
LogThis 
#remove-psdrive Z