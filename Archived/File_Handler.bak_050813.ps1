#* FileName: File_Handler.ps1
#*=============================================================================
#* Script Name: [File Handler]
#* Created: [25/06/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: All-in-one script for processing of any file types
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [25/06/2013]
#* Time: [12:00]
#* Issue: 
#* Solution:
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: OPS-Copy, OPS-Move, OPS-Tidy, OPS-Zip
# Created: [22/06/2013]
# Author: Damar Johnson
# Arguments: Source, destination, filetype, fileage
# =============================================================================
# Purpose: Copy files, Move files, Delet files, Zip files
#
#* To Copy file-    powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 clone C:\PS\Test C:\PS\Test2 tsk 5 (File age i.e 5 days old)
#* To Move file-    powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 shift C:\PS\Test C:\PS\Test2 tsk 5
#* To Append file-  powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 append C:\PS\Test\[filename] C:\PS\Test2\[filename]
#* To Delete file-  powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 tidy C:\PS\Test tsk 5
#* To zip file-     powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 archive C:\PS\Test C:\PS\Test csv (File type i.e csv ot txt)
# =============================================================================

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
#$time=(get-date).hour
### Webload incrementals
       switch ($ndump) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($tail) {
                       'i' {$global:Fname="$jdate$sep$ndump$inc";$global:logpath="O:\auto\logs\$ndump$inc" ;write-host inc exists!;
                                }
                       '' {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$ndump";$global:logpath="O:\auto\logs\$ndump"}
                       }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
        $global:thestr = $args[0]
        $d = (Get-Date).tostring( "yyyy-MM-dd HH:mm:ss" )
        "$wdate | $thestr" | tee -Variable global:logvalue >> ('O:\AUTO\Tasks\Fixed_Income\CDS\log\'+"$jdate"+'_CDS.log')  #$global:basedir\LogCdsPs.txt
        write-output "$d $thestr"
        if ($args[1] -eq "exit") {exit}
    }

#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\File_Handler.log -Append

$action=$args[0]
$source=$args[1]
$dest=$args[2]
$type=$args[3]
$date=$args[4]

### Logging ###
#$logvalue=''

function Age ($date='0') { 
$input |?{$_.LastWriteTime.Date -le (get-date).Date.AddDays($date)}
}

Function OPS-Copy {
cd $source
#cpi (gci $source -Filter "*.$type") $dest -Force
#cpi (gci $source -Filter "*.$type" | ?{$_.LastWriteTime.Date -lt (get-date).Date.AddDays(-4)}) $dest -Force
#cpi ((gci $source -Filter "*.$type") |?{$_.LastWriteTime.Date -lt (get-date).Date.AddDays(-4)})  $dest -Force
switch ($type) {""{$type='*';}}
cpi ((gci $source -Filter "*.$type") | Age)  $dest -Force
    #switch ($?){
            #'TRUE'{$Global:logvalue="File has been successfully copied!"}
            #}

 #Set-logvalue
}

Function OPS-Append {
cat $source | ac $dest -Force
}

Function OPS-Move {
cd $source
mi ((gci $source -Filter "*.$type") | Age) $dest -Force
}

Function OPS-Tidy { #($type='*'){
cd $source
ri ((gci $source -Filter "*.$type") | Age) -Force
}


Function OPS-Zip {
Set-Alias 7zip "$env:ProgramFiles\7-Zip\7z.exe"
#7zip a "-t$compress" $archive $source
#7zip a "-t$type" $archive,$source | Date #$source,$dest
#$file=(gci $source |? $_.extension -eq "*.$type" ) | Date
#7zip a "-tzip" $file,$archive | Date #$source,$dest
switch ($type) {'' {$type='*';}} #$args[3]=$date}}
cd $source; $file=(gci $source | Age);$file >> C:\PS\Test\kong.txt
7zip a "-tzip" $archive,$file "*.$type" #$source,$dest
#7zip a "-tzip" $archive,$source "*.$type" #$source,$dest
}


switch ($action) {
clone {Write-Host Copying File; OPS-Copy; break}
append {Write-Host Appending to File; OPS-Append; break}
shift {Write-Host Moving File; OPS-Move; break}
tidy {Write-Host Deleting File;$type=$args[2]; OPS-Tidy; break} #$date=$args[3]; OPS-Tidy; break}
archive {write-host Compressing File;$archive=$args[2];$source=$args[1];OPS-Zip;break}#switch ($type) { {'' {$type='*'}}; OPS-Zip;break}
}

$source
$dest
#$Value
#$date
#$input
#$type
$file

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            { write-host No logpath found! Creating Directory; mkdir $logpath }
        Else
            {Write-Host Current logpath exists}    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                { write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$class'>$logvalue</span></p>"}
                Else
                { write-host Appending to file!; ac -Path "$logpath\$Fname.html" -Value "<p><span class='$class'>$logvalue</span></p>" -Force}
}

Get-Increment
Set-Logpath

exit $LASTEXITCODE


### Execute logging ###
#get-increment
#set-logpath

#Stop-Transcript
######################################################################################################
#Function Date-Pick ($date=(Get-Date).Date) {
#$_ | ?{$_.LastWriteTime.Date -le (get-date).Date.AddDays("-$date")}
#}

#Function Date-Pick {
#switch ($date) {
#$null {write-host No date selected;Out-Default; break}
#$null {Write-Host No date specified; break}
#(!($null)) {?{$_.LastWriteTime.Date -le (get-date).Date.AddDays("-$date")}}
# }
#}

#Function Date ($date=(get-date).Date) {
#switch { 
# $_ | ?{$_.LastWriteTime.Date -le (get-date).AddDays("-$date")}
#}

#Function Date ($date=(Get-Date).Date) {
#switch ($date) { 
#default {$_ | ?{$_.LastWriteTime.Date -le (get-date).Date}}#.AddDays("-$date")};break}
#(!($null)) {Write-Host 'date specified'; ?{$_.LastWriteTime.Date -le (get-date).AddDays("-$date")}}
#}
#}

#Function Date ($date=(Get-Date).Date) {
#process {switch ($date) { 
#default {$_ | ?{$_.LastWriteTime.Date -eq (get-date).Date}}#.AddDays("-$date")};break}
#(!((get-date).Date)) {$_ | ?{$_.LastWriteTime.Date -le (get-date).AddDays("-$date");Write-Host 'date specified'}}
#        }
#    }
#}

#Function Date
#{
#process {$_ | ?{$_.LastWriteTime.Date -lt (get-date).Date.AddDays(-8)}}
#}

#Function Date {
#$input | ?{$_.LastWriteTime.Date -lt (get-date).Date.AddDays(-4)}
#}
#?{$_.LastWriteTime.Date -lt (get-date).Date.AddDays(-4)}

#Function Tdate {
#if ($date -eq $null) {
#?{$_.LastWriteTime.Date -le (get-date).Date}
#}
#}

#function Days {
#param ($days)
#?{$_.LastWriteTime.Date -lt (get-date).Date.AddDays(-$Days)}
#(get-date).Date.AddDays(-$Days)
#}

#function Date {
#$input |?{$_.LastWriteTime.Date -le (get-date).Date.AddDays(-$date)}
 #}
#}