#########################
### Format Rates File ###
# Need a format step to do the following
# Add 3 columns - 1) Date; from filename#, 2) Source; from filename, 3) Base; always USD.
# Add field hearders; 1) currency, 2) ra#te 3) feeddate, 4) base, 5) src
# Add Euro legacy currencies - List:

## Set path variables
#------------------ Live Path ------------------#
$csvpath="O:\Datafeed\Xrates\"
$legacy= "O:\Datafeed\Xrates\USD-Legacy-Curr.txt"
#$csvpath=$args[0]

#------------------ Test Path ------------------#
#$csvpath="c:\pstest\Datafeed\Xrates\"
#$legacy= "c:\pstest\Datafeed\Xrates\USD-Legacy-Curr.txt"

$oer=gci $csvpath -Filter *.csv | ?{$_ -like'oer*'} | %{ $a=$_.BaseName; $b=$_.fullname  
                  $fn=$a.split("_")#; write-output $($fn[0]) ($fn[1])
                                    
                  #Append file content if sourcename matches stated 
                      if ($fn[0] -eq 'oer')
                 {
                  $base="USD"
                  $ap=(gc $legacy)
                  #}#testin remove per section
                  
                  #$FEDFUNDS=diff -ReferenceObject $ap -DifferenceObject $testval;
                  #Write-Output $FEDFUNDS.inputobject
                  #ac $b -Value $FEDFUNDS.InputObject
         
                  ac $b -Value $ap
                  #$ap=$ap[0..12]
                  #$start= ipcsv $b
                  #$append=$fn[0],$Base,$fn[1]
                        
                        #delete header from file
                        #$dh=(gc $b)
                        $dh=(gc $b.Split('*}*') | ?{$_ -notlike '*}*'} | %{$_.replace(': ',',')})
                        #$dh=(gc $b | ?{$_ -notlike '}*'})
                        $dh= $dh[6..($dh.count)]
                        $dh > $b
                        
                        #Append full format to csv file
                        $fc= $b | ipcsv -header Curr,Rate,Feeddate,base,Src 
                        $fc | % {$_.src=$fn[0];$_.Feeddate=$fn[1];$_.base=$base;};
                        
                        ##Export csv/convert to csv without additional formating
                        $b=$fc | convertto-csv -notypeinformation | %{$_.Replace('"','')}| out-file $b -force -en ascii
              
                  }
                    else{}
                }