﻿#################################################################
### Set Connection to MySQL DB and execute query ###

#Set MySQL Connectiion
#[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
#$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection
#$myconnection.Connectionstring = "server=192.168.12.202;database=test;Persist Security Info=false;user id=otrs;pwd=hot"

## Connect to LiquidWeb
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"
#$myconnection.open()

#Set query command
#$Command= New-Object MySql.Data.MySqlClient.MySqlCommand
#$Command.Connection=$myconnection

#$NList='O:\AUTO\Scripts\sql\FTP\CompareFTP_NET.sql'
#$CList='O:\AUTO\Scripts\sql\FTP\CompareFTP_COM.sql'

#Queries
#$show_all= "SHOW TABLES"
#$tclinet="select * from tclient"
#$tfeed="select * from tfeed";
#$tservices="select * from tservices";

#Execute Command
#$command.CommandText = $NList
#$reader=$Command.ExecuteReader()

#$dataset = New-Object System.Data.DataSet
#Write-Output $reader.GetValue(0).ToString()

#while($myreader.Read()){$myreader.GetString(0)}

#while
#($reader.Read()){
#    for ($i=0; $i -lt $reader.FieldCount; $i++ {
#        Write-Output $reader.GetValue($i).ToString()
#    }
#}

#while 
#($reader.Read()) {
#    for ($i=0; $i -lt $reader.FieldCount; $i++) {
#            write-output $reader.GetValue($i).ToString()
#    }
#}

#$myconnection.Close()

#################################################################
### Compare the contents of 2 files and output the difference ###

$date=date -format yyyyMMdd

## Files to be compared
#$COM=type o:\AUTO\ftp_dir\$date'_COM.txt'
#$NET=type o:\AUTO\ftp_dir\$date'_NET.txt'

$Taskfile=type 'C:\cfeed_task.csv'
$Database=type 'C:\cfeed_database.csv'

## Output results to Reports folder
$Report='O:\AUTO\FTP_DIR\Reports\'+$date+'_OPs.csv'

## Set labels for Table 
#$Head=$COM[0,1]
$labelA='Extra on Taskfiles'
$labelB='Missing from Taskfiles' 
$labelC='Consistant on both'

## Output and Capture difference
$diff0=diff -ReferenceObject $Taskfile -DifferenceObject $Database 
diff -ReferenceObject $Taskfile -DifferenceObject $Database -PassThru > $diff3
$verified=$diff0[1..($diff0.Count)]

$field=%{($verified.InputObject).split(",")}

$verified | Add-Member -MemberType NoteProperty -Name User -Value $field[0]
$verified | Add-Member -MemberType NoteProperty -Name Remote -Value $field[1]
$verified | Add-Member -MemberType NoteProperty -Name Prefix -Value $field[2]
$verified | Add-Member -MemberType NoteProperty -Name Suffix -Value $field[3]
$verified | Add-Member -MemberType NoteProperty -Name Filename -Value $field[4]
$verified | Add-Member -MemberType NoteProperty -Name Ext -Value $field[5]

## Capture file differences and edit file properties
$neutral=%{$verified} | ?{$_.SideIndicator -eq '=='};
        %{$neutral} | add-member -MemberType NoteProperty -Name Inconsistency -Value $labelC

$kane=%{$verified} | ?{$_.SideIndicator -eq '<='};
        %{$kane} | add-member -MemberType NoteProperty -Name Inconsistency -Value $labelA

$able=%{$verified} | ?{$_.SideIndicator -eq '=>'};
        %{$able} | add-member -MemberType NoteProperty -Name Inconsistency -Value $labelB

$NewTotals=$verified[1] | Select-Object -property @{label="Total Files"; Expression={$neutro.Count}}, @{label="Missing from .NET"; Expression={$kane.Count}},@{label="Extra on .NET"; Expression={$able.Count}}

## Output results to Tables
$verified | ft -Property User,Remote,Prefix,Filename,Suffix #@{header='Total Files='+$neutro.count,'Total Files Missing from .NET='+$able.count, 'Total Files Extra on .NET='+$kane.count} -AutoSize

## Export file in CSV format
$verified | %{$_.User=($_.InputObject).split(",")[0];
                $_.Remote=($_.InputObject).split(",")[1];
                $_.Prefix=($_.InputObject).split(",")[2];
                $_.Filename=($_.InputObject).split(",")[4];
                $_.Suffix=($_.InputObject).split(",")[3];
                $_.Ext=($_.InputObject).split(",")[5]}

$x_verified=$verified | select * -ExcludeProperty InputObject,SideIndicator;

$x_verified | Select-Object -property * | ft
#$x_verified | Select-Object -property User,Remote,Prefix,Filename,Suffix,Ext, 'Total Files','Missing from Taskfiles','Extra on Taskfiles' | epcsv -NoTypeInformation $Report
$x_verified | epcsv -notypeinformation -Append -force $Report

date