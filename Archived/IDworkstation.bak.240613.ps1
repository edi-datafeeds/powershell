﻿############################################################################################################
####Script detects which workstation it is being executed on, results get appended to OP's index logfile####

$OFS=''
##Working output directory passed via argument
$ndump=$args[0]
$cat=$args[1]

##Testpath
#$ndump='damartest48'
#$cat="i"

##Fixed variables
#$logpath="O:\auto\logs\$ndump$inc"
$time=(get-date).hour
$sep='_'

#Get PC name
$SID=$(Get-WmiObject Win32_Computersystem).name

##Date values used
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G

##determine increment
IF ($cat -eq "i")
{
### Webload incrementals
    IF ($time -ge 12 -and $time -le 16)
    {
        $inc='_2'
    }
    Elseif ($time -ge 17 -and $time -le 23)
    {
        $inc='_3'
    }
    Else 
    {
        $inc='_1'
    }

### SMF incrementals
    #IF ($time -ge 12 -and $time -le 16)
    #{
    #    $inc='_2'
    #}
    #Elseif ($time -ge 17 -and $time -le 23)
    #{
    #    $inc='_3'
    #}
    #Else 
    #{
    #    $inc='_1'
    #}

### 123Trans incrementals
    #IF ($time -ge 12 -and $time -le 14)
    #{
    #    $inc='_1'
    #}
    #Elseif ($time -ge 17 -and $time -le 23)
    #{
    #    $inc='_2'
    #}
    #Else 
    #{
    #    $inc='_1'
    #}

### CABTrans incrementals
    #IF ($time -ge 11 -and $time -le 13)
    #{
    #    $inc='_1'
    #}
    #Elseif ($time -ge 14 -and $time -le 15)
    #{
    #    $inc='_2'
    #}
    #Elseif ($time -ge 16 -and $time -le 17)
    #{
    #    $inc='_3'
    #
    #Elseif ($time -ge 16 -and $time -le 17)
    #{
    #    $inc='_3'
    #}
    #Elseif ($time -ge 18 -and $time -le 19)
    #{
    #    $inc='_4'
    #}
    #Else 
    #{
    #    $inc='_5'
    #}
#}

    write-host "cuntface";$Fname=$jdate,$sep,$ndump,$inc;$logpath="O:\auto\logs\$ndump$inc"
}
Else
{
    write-host "lopsided";$Fname=$jdate,$sep,$ndump;$logpath="O:\auto\logs\$ndump"
}

##Set output path
IF (!(Test-Path -Path $logpath))
{
    mkdir $logpath
}
Else
{
}    

#$Fname=$jdate,$sep,$ndump
#$PCname >> $File
#ni -Name 'dude.txt' -itemtype "file" -Value "<p>spanclassnote<$PCname></p>"

##Create file or append if exist   
IF (Test-Path $logpath -include "$Fname$inc.html")
{
ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>"
}
Else
{
Add-Content -Path "$logpath\$Fname.html" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>" -Force;
}

#Output test
$cat
Test-Path -Path $logpath
$logpath
$Fname
$inc
"$logpath\$Fname.html"