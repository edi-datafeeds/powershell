﻿#Set MySQL Connectiion
#[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
#$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection
#$myconnection.Connectionstring = "server=192.168.12.202;database=test;Persist Security Info=false;user id=otrs;pwd=hot"

## Connect to LiquidWeb
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"
#$myconnection.open()

#Set query command
#$Command= New-Object MySql.Data.MySqlClient.MySqlCommand
#$Command.Connection=$myconnection

#$NList='O:\AUTO\Scripts\sql\FTP\CompareFTP_NET.sql'
#$CList='O:\AUTO\Scripts\sql\FTP\CompareFTP_COM.sql'

#Queries
#$show_all= "SHOW TABLES"
#$tclinet="select * from tclient"
#$tfeed="select * from tfeed";
#$tservices="select * from tservices";

#Execute Command
#$command.CommandText = $NList
#$reader=$Command.ExecuteReader()

#$dataset = New-Object System.Data.DataSet
#Write-Output $reader.GetValue(0).ToString()

#while($myreader.Read()){$myreader.GetString(0)}

#while
#($reader.Read()){
#    for ($i=0; $i -lt $reader.FieldCount; $i++ {
#        Write-Output $reader.GetValue($i).ToString()
#    }
#}

#while 
#($reader.Read()) {
#    for ($i=0; $i -lt $reader.FieldCount; $i++) {
#            write-output $reader.GetValue($i).ToString()
#    }
#}

#$myconnection.Close()

#################################################################
### Compare the contents of 2 files and output the difference ###

#$COM=gc $args[0]
#$NET=gc $args[1]
#$diff=$args[2]
#$COM=gc 'C:\users\d.johnson\Desktop\test_com.txt'
#$NET=gc 'C:\Users\d.johnson\Desktop\test_net.txt'
#$diff3='C:\Users\d.johnson\Desktop\test_diff3.txt'
#$diff5='C:\Users\d.johnson\Desktop\test_diff5.txt'

$date=date -format yyyyMMdd


## Files to be compared
#$COM=type o:\AUTO\ftp_dir\$date'_COM.txt'
#$NET=type o:\AUTO\ftp_dir\$date'_NET.txt'
$COM=gc 'o:\AUTO\ftp_dir\20130408_COM.txt'
$NET=gc 'o:\AUTO\ftp_dir\20130408_NET.txt'


## Output results to Reports folder
$Report='O:\AUTO\FTP_DIR\Reports\'+$date+'_ftpcompare.csv'

## Set labels for Table 
#$Head=$COM[0,1]
$labelA='EXTRA on .NET'
$labelB='MISSING from .NET' 

## Output and Capture difference
$diff0=diff -ReferenceObject $NET -DifferenceObject $COM;
diff -ReferenceObject $NET -DifferenceObject $COM -PassThru > $diff3
$neutro=$diff0[0..($diff0.Count)]

$puppy=%{($neutro.InputObject).split("`t")}

$neutro | Add-Member -MemberType NoteProperty -Name LoginID -Value $puppy[0]
$neutro | Add-Member -MemberType NoteProperty -Name Dir -Value $puppy[1]
$neutro | Add-Member -MemberType NoteProperty -Name VirtualPath -Value $puppy[2]

## Capture file differences and edit file properties
$kane=%{$neutro} | ?{$_.SideIndicator -eq '<='};
        %{$kane} | add-member -MemberType NoteProperty -Name Inconsistency -Value $labelA

$able=%{$neutro} | ?{$_.SideIndicator -eq '=>'};
        %{$able} | add-member -MemberType NoteProperty -Name Inconsistency -Value $labelB

## Output results to Tables
#$neutro | ft -Property Inputobject,Inconsistency -AutoSize
$neutro | ft -Property LoginID,Dir,VirtualPath,Inconsistency -AutoSize

## Export file in CSV format
$neutro | %{$_.LoginID=($_.InputObject).split("`t")[0]; $_.Dir=($_.InputObject).split("`t")[1]; $_.VirtualPath=($_.InputObject).split("`t")[2]}
#$neutro | export-csv -notypeinformation C:\Users\d.johnson\Desktop\test_diff5.txt

#$neutro | ft -Property LoginID,Dir,VirtualPath,Inconsistency -AutoSize > O:\AUTO\FTP_DIR\Reports\$date'_ftpcompare.txt'
#$neutro | export-csv -excludeproperty InputObject -notypeinformation $Report
$neutrochild=$neutro | select * -ExcludeProperty InputObject,SideIndicator;
$neutrochild | epcsv -notypeinformation $Report

date
