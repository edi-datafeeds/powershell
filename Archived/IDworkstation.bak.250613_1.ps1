﻿#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

############################################################################################################
####Script detects which workstation it is being executed on, results get appended to OP's index logfile####

$OFS=''
##Working output directory passed via argument
#$ndump=$args[0]
#$cat=$args[1]

##Testpath
$ndump='damartest888'
$cat="i"

##Fixed variables
#$logpath="O:\auto\logs\$ndump$inc"
$time=(get-date).hour
$sep='_'

#Get PC name
$SID=$(Get-WmiObject Win32_Computersystem).name

##Date values used
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G

Function Get-Increment {
########################### Determine Increment ###########################
### Webload incrementals
       switch ($ndump) {
    'WCAWebload'
            {switch ($time) {
                ( $_ -ge 12 -and $_ -le 16) {$inc='_2'}
                ($_ -ge 17 -and $_ -le 23) {$inc='_3'}
                default {$inc='_1'}
                }
            }
    ### SMF incrementals
    'SMF'
        {switch ($time) {
            ($_ -ge 12 -and $_ -le 16) {$inc='_2'}
            ($_ -ge 17 -and $_ -le 23) {$inc='_3'}
            default {$inc='_1'}
            }
            }
    ### 123Trans incrementals
    '123Trans' 
        {switch ($time) {
            ($_ -ge 12 -and $_ -le 14) {$inc='_1'}
            ($_ -ge 17 -and $_ -le 23) {$inc='_2'}
            default {$inc='_1'}
            }
        }
    ### CABTrans incrementals
    'CABTrans' 
        {switch ($time) {
            ($_ -ge 11 -and $_ -le 13) {$inc='_1'}
            ($_ -ge 14 -and $_ -le 15) {$inc='_2'}
            ($_ -ge 16 -and $_ -le 17) {$inc='_3'}
            ($_ -ge 18 -and $_ -le 19) {$inc='_4'}
            default {$inc='_5'}
            }
        }
}
    If (!($null)) {
    ### Output incrment number
    write-host "cuntface";$Fname=$jdate,$sep,$ndump,$inc;$logpath="O:\auto\logs\$ndump$inc"
    }
    Else
        {write-host "lopsided";$Fname=$jdate,$sep,$ndump;$logpath="O:\auto\logs\$ndump"}
}
        
Function Set-Logpath {
### Determine file log path
        IF (!(Test-Path -Path $logpath))
            { mkdir $logpath }
        Else
            {}    
        ##Create file or append if exist   
        IF (Test-Path $logpath -include "$Fname$inc.html")
            {ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>"}
        Else
            {ac -Path "$logpath\$Fname.html" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>" -Force;}
}

#Output test
$cat
Test-Path -Path $logpath
$logpath

#Stop-Transcript