#* FileName: File_Handler.ps1
#*=============================================================================
#* Script Name: [File Handler]
#* Created: [25/06/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: All-in-one script for processing any file types
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [25/06/2013]
#* Time: [12:00]
#* Issue: 
#* Solution:
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: OPS-Copy, OPS-Move, OPS-Tidy, OPS-Zip
# Created: [22/06/2013]
# Author: Damar Johnson
# Arguments: Source, destination, filetype, fileage
# =============================================================================
# Purpose: Copy files, Move files, Delet files, Zip files
#
### ----- Short format ----- ##
#* To Copy file-    powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 clone C:\PS\Test C:\PS\Test2 txt 5 (File age i.e 5 days old)
#* To Move file-    powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 shift C:\PS\Test C:\PS\Test2 tsk 5
#* To Append file-  powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 append C:\PS\Test\[filename] C:\PS\Test2\[filename]
#* To Delete file-  powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 tidy C:\PS\Test tsk 5
#* To zip file-     powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 zip C:\PS\Test C:\PS\Test csv (File type i.e csv ot txt)

### ----- Long format ----- ##
#* To Copy file-    powershell.exe .\File_Handler.ps1 -Action clone  -Source C:\PS\Test -Dest C:\PS\Test2 -Type txt -type txt -Age 5
#* To Move file-    powershell.exe .\File_Handler.ps1 -Action shift  -Source C:\PS\Test -Dest C:\PS\Test2 -Type tsk -type txt -Age 5
#* To Append file-  powershell.exe .\File_Handler.ps1 -Action append -Source C:\PS\Test\FileA -Dest C:\PS\Test2\FileB -type txt
#* To Delete file-  powershell.exe .\File_Handler.ps1 -Action tidy   -Source C:\PS\Test -type txt
#* To zip file-     powershell.exe .\File_Handler.ps1 -Action zip    -Source C:\PS\Test -Dest C:\PS\Test -type csv
# =============================================================================

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$true,Position=0,
ParametersetName='Action'
)]
#[switch]

[Parameter(
mandatory=$true
)]
[string]$a, #Action to perform

[Parameter(
Mandatory=$true
)]
[string]$s, #Source path of original file

[Parameter(
#Mandatory=$true
)]
[string]$d, #Destination of original file

[parameter(
mandatory=$true
)]
[string]$t, # Type of file 

[int]$age,

[parameter(
mandatory=$false
)]
[string]$i, #Increment

[parameter(Mandatory=$false)]
[string]$logfile='OPS-test',

[parameter(
mandatory=$false
)]
[string]$alert

)

#------------------------- Set alert function -------------------------#

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

#switch ($alert){
#$true {msgbox}
#default {}
#}

#$global:label='Error'

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

$action=$a
$source=$s
$dest=$d
$type=$t
#$age {$age}

$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'
$global:class='ok'

$global:logvalue=@()

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                #'t15022_inc' 
                'damar-test'
                    {$global:source=$source.replace('????????',$jdate).replace('??????',$fileinc)
                    $global:dest=$dest.replace('????????',$jdate).replace('??????',$fileinc)
                        switch ($time) {
                            { $_ -ge 12 -and $_ -le 16} {$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {;$global:fileinc='203000'}
                            default {$global:fileinc='083000'}
                                }
                        #}
                            }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        #$hold=@()
        #$global:thestr = $args[0]
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output "$d $displaytxt" #;exit
        #$hold.count
        $global:logvalue
        #if ($args[1] -eq "exit") {exit}
    }

#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\File_Handler.log -Append

#------------------------- Set file age -------------------------#

function Age ($age='0') { 
$input |?{$_.LastWriteTime.Date -le (get-date).Date.AddDays($age)}
}

#------------------------- Copy func -------------------------#

    Function OPS-Copy { #($type='zip') {
    $x=Test-Path $source
        switch ($x) {
                'True'
                    {switch ($a){
                    {$source.contains('.')} {cd (split-path -parent $source)
                        }
                    default{cd $source}
                    }
                    $y=Test-Path $dest
                    switch ($y){
                            'True' 
                                {switch ($x){
                                #(!(test-path "$source\*.$type")){
                                (!(test-path $source -Filter *.$type)){
                                    LogThis -displaytxt "[$action] $source`: $type`: File type specified does not exist" #Write-Host 'File type specified does not exist'
                                        msgbox "File type specified does not exist"
                                        $global:class='failed'
                                            }
                                default {
                                    switch ($x){
                                        $source.Contains('.') {gci ((split-path -leaf $source) | tee -Variable item | age) -Filter "*.$type" | cpi -Destination $dest}
                                        default {cpi (gci $source -Filter "*.$type" | tee -Variable item | age) -Destination $dest}
                                                }
                                            #gci ((split-path $source) | tee -Variable item | age) -Filter "*.$type" | cpi -Destination $dest
                                            #cpi (gci (split-path $source) -Filter "*.$type" | tee -Variable item | Age)  $dest -Force
                                            $total=$item.Count;Get-Increment
                                            Logthis -displaytxt "$total`: File(s) successfully copied!";Set-Logpath
                                                foreach ($x in $item){
                                                switch ($total){
                                                    #'1'{logthis "$x deleted";break}
                                                    {$total -gt '1'}{write-host "$total";logthis "$x copied";$count+++1 | tee -Variable list;Write-Host "$list";Set-Logpath
                                                        switch ($list){
                                                            {$list -ge $total}{exit $LASTEXITCODE}
                                                            #default {logthis "$x copied"}#;break}
                                                            default {logthis "Copy- $global:source => $global:dest"}#;break}
                                                                }
                                                        }
                                                    default {logthis "Copy - $global:source => $global:dest"}#;break}
                                                    #default {logthis "$x copied"}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    'False' 
                                        {logthis -displaytxt "[$action] $dest`: Destination path specified does not exist please re-check path!"
                                            msgbox "Destination path specified does not exist please re-check path!"
                                            $global:class='failed'
                                            }
                                }
                            }
                        'False' {logthis -displaytxt "[$action] $source`: Source path specified does not exist please re-check path!"
                                    msgbox "Source Path does not exist please re-check path!"
                                    $global:class='failed'
                                    }
                }
            }

 #------------------------- Append func -------------------------#

    Function OPS-Append {
    $x=Test-Path $source
        switch ($x){
            'True'
                {$y=Test-Path $dest
                switch ($y){
                            'True' 
                                {cat $source | ac $dest -Force;logthis -displaytxt "[$action] File successfully ammended!";Get-Increment;Set-Logpath
                                 logthis "Ammend- $global:source => $global:class"
                                 #logthis "Source $source".replace('????????',$jdate).Replace('??????',$fileinc),
                                    #Target:,"$dest".replace('????????',$jdate).replace('??????',$fileinc)
                                    }   
                            'False'
                                {logthis -displaytxt "[$action] $dest`: End File to be ammended to does not exist please re-check end file!"
                                    msgbox "End File to be ammended to does not exist please re-check end file!"
                                    $global:class='failed'
                                }
                            }
                }
            'False' 
                {logthis -displaytxt "[$action] $source`: Source file does not exist please re-check source path!"
                    msgbox "Source file does not exist please re-check source file!"
                    $global:class='failed';break
                    }
            }
}
#------------------------- Move func -------------------------#

    Function OPS-Move { #($type='*') {
    $x=Test-Path $source
        switch ($x){
            'True'
                {cd (Split-Path -Parent $source)
                $y=Test-Path $dest
                switch ($y){
                            'True' 
                                {cpi ((gci $source -Filter "*.$type" | tee -Variable item) | Age) $dest -Force
                                 $total=$item.Count;Get-Increment
                                 logthis "$total`: File(s) successfully moved!";Set-Logpath
                                     foreach ($x in $item){
                                        switch ($total){
                                            {$total -gt '1'}{logthis "$x moved";$count+++1 | tee -Variable list ;write-host $list ;Set-Logpath
                                            switch ($list){
                                                {$list -ge $total}{exit $LASTEXITCODE}
                                                #default {logthis "$x moved";break}
                                                default {logthis "Move $x => $global:dest";break}
                                                    }
                                                }
                                            #default {logthis "$x moved";break}
                                            default {logthis "Move $x => $global:dest";break}
                                        }
                                    }
                            }
                            'False'
                                {logthis -displaytxt "[$action] $source`: File(s) to be moved does not exist please re-check file/path!"
                                    msgbox "File(s) to be moved does not exist please re-check file/path!"
                                    }
                            }
                }
            'False' 
                {logthis -displaytxt "[$action] $source`: Source file path specified does not exist please re-check file/path!"
                    msgbox "Source file path does not exist please re-check file/path!"
                        }
            }
        }

###########------------------------- Rename func -------------------------#

    Function OPS-Rename { #($type='*') {ni -path $dest -Name (split-path -leaf $dest) -Value (gc $source)
    $x=Test-Path $source
        switch ($x) {
                'True'
                    {switch ($a){
                    {$source.contains('.')} {cd (split-path -parent $source)
                        }
                    default{cd $source}
                    }
                    $y=Test-Path $dest
                    switch ($y){
                            'True' 
                                {switch ($x){
                                #(!(test-path "$source\*.$type")){
                                (!(test-path $source -Filter *.$type)){
                                    LogThis -displaytxt "[$action] $source`: $type`: File type specified does not exist" #Write-Host 'File type specified does not exist'
                                        msgbox "File type specified does not exist"
                                        $global:class='failed'
                                            }
                                default {
                                    switch ($x){
                                        $source.Contains('.') {gci ((split-path -leaf $source) | tee -Variable item | age) -Filter "*.$type" | rni -NewName $dest
                                        default {rni (gci $source -Filter "*.$type" | tee -Variable item | age) -NewName $dest}
                                                }
                                            #gci ((split-path $source) | tee -Variable item | age) -Filter "*.$type" | cpi -Destination $dest
                                            #cpi (gci (split-path $source) -Filter "*.$type" | tee -Variable item | Age)  $dest -Force
                                            $total=$item.Count;Get-Increment
                                            Logthis -displaytxt "$total`: File(s) successfully copied!";Set-Logpath
                                                foreach ($x in $item){
                                                switch ($total){
                                                    #'1'{logthis "$x deleted";break}
                                                    {$total -gt '1'}{write-host "$total";logthis "$x copied";$count+++1 | tee -Variable list;Write-Host "$list";Set-Logpath
                                                        switch ($list){
                                                            {$list -ge $total}{exit $LASTEXITCODE}
                                                            #default {logthis "$x copied"}#;break}
                                                            default {logthis "Copy- $global:source => $global:dest"}#;break}
                                                                }
                                                        }
                                                    default {logthis "Copy - $global:source => $global:dest"}#;break}
                                                    #default {logthis "$x copied"}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    'False' 
                                        {logthis -displaytxt "[$action] $dest`: Destination path specified does not exist please re-check path!"
                                            msgbox "Destination path specified does not exist please re-check path!"
                                            $global:class='failed'
                                            }
                                }
                            }
                        'False' {logthis -displaytxt "[$action] $source`: Source path specified does not exist please re-check path!"
                                    msgbox "Source Path does not exist please re-check path!"
                                    $global:class='failed'
                                    }
                }
            }

#------------------------- Delete func -------------------------#

    Function OPS-Tidy { #($type='*') {
    $x=Test-Path $source
        switch ($x) {
            'True'{
                cd $source
                switch ($type){
                {(!(test-path "$source\*.$type"))}{
                    LogThis -displaytxt "[$action] $type`: File type specified does not exist" #Write-Host 'File type specified does not exist
                    msgbox "File type specified does not exist"
                    $global:class='failed'
                            }
                        default {
                            ri ((gci $source -Filter "*.$type" | tee -Variable item) | Age) -Force
                            #ri "$source\*.$type" | tee -Variable item | Age -Force
                            $total=$item.count;get-increment
                            logthis "$total`: File(s) successfully deleted!";Set-Logpath
                            foreach ($x in $item){
                                switch ($total){
                                    #'1'{logthis "$x deleted";break}
                                    {$total -gt '1'}{logthis "$x deleted";$count+++1 | tee -Variable list ;write-host $list ;Set-Logpath
                                    switch ($total){
                                        {$list -gt $total}{exit $LASTEXITCODE}
                                        default {logthis "$x deleted";break}
                                        }
                                    }
                                    default {logthis "$x deleted"}
                                    }
                                }
                            }
                        }
                }
            'False' {
                    Logthis -displaytxt "[$action] $source`: Source file path specified does not exist please re-check file/path!"
                        msgbox "Source file path does not exist please re-check file/path!"
                        $global:class='failed'
                            }
                        }
            }
        
#------------------------- Zip func -------------------------#

    Function OPS-Zip { #($type='*'){
    Set-Alias 7zip "$env:ProgramFiles\7-Zip\7z.exe"
        switch ($source){
        {(!(Test-Path $source))}{
            LogThis -displaytxt "[$action] $source`: Source file path specified does not exist please re-check file/path!"
                Msgbox "File path specified does not exist please re-check file/path!"
                $global:class='failed'
                    }
        default {
            cd $source
            $file=(gci $source | Age) | ?{$_.mode -like '-a---'} | tee -Variable item;$file #>> C:\PS\Test\test_exist.txt
            $total=$item.Count
                switch ($x){
                {(!(test-path "$source\*.$type"))}{
                    LogThis -displaytxt "[$action] $type`:  File type specified does not exist" #| TF: $logfile"
                        msgbox "File type specified does not exist" #| TF: $logfile"  
                        $global:class='failed'
                    }
                default{
                    7zip a "-tzip" $archive,$file "*.$type" #$source,$dest
                    LogThis -displaytxt "[$action] $total`: File(s) successfully zipped! | ARC:$archive.zip";Set-Logpath
                    foreach ($x in $item){
                        switch ($total){
                            {$total -gt '1'}{logthis "$x zipped";$count+++1 | tee -Variable list ;write-host $list ;Set-Logpath
                            switch ($list){
                                {$list -ge $total}{exit $LASTEXITCODE}
                                default {logthis "$x zipped";break}
                                    }
                                }
                            }
                        }
                    }
                }
            }
    }
}

#------------------------- Action script -------------------------#
    
    If ((!($a))){
            Write-Host No action specified!
                    msgbox 'No action specified!'
    }
    Else{

        switch ($a){
            clone {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                #$dest=$null
                    Write-Host Copying File; OPS-Copy; break
                }
            append {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$false,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    Write-Host Appending to File A; OPS-Append; break
                }
            Rename {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    Write-Host Moving File; OPS-Move; break
                }
            shift {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    Write-Host Moving File; OPS-Move; break
                }
            tidy {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$type
                [Parameter(mandatory=$true,Position=3
                )]
                [int]$age
                    Write-Host Deleting File;OPS-Tidy; break #$type=$args[2]; OPS-Tidy; break
                }
            zip {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    write-host Compressing File;$archive=$dest;OPS-Zip;break
                }
            Default {#[System.Windows.Forms.MessageBox]::Show("$action $stop",'Error: Command not found '+"$logfile")
                    Write-Host Action specified is not valid! Please review Options
                    $global:label='Error Command not found';#write-host $label
                    $alert='y'
                    $global:class='failed'
                        msgbox "Action specified is not valid! Please review options:`r
    [  clone  ] - Copy file(s) from 1 location to another`n
    [append] - Appends content of source to target file `n
    [   shift  ] - Moves file(s) from 1 location to another `n
    [   tidy   ] - Deletes file(s) from source location `n
    [    zip    ] - Compresses file(s) from source location"
                            logthis "[$action] action not found, please review options | Taskfile: $logfile"
            }
    }
 }
#$source | ?{$_ -like '*?'}

$Fname
$logpath
$item
$global:source
$global:dest
#$source | gm

Get-Increment
Set-Logpath

exit $LASTEXITCODE