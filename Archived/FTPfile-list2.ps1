﻿
$FTP_dir=gci '\\192.168.12.4\ops\AUTO\Tasks\' -Directory FTP -Recurse

$FTP_file=gci -Recurse (%{
    $FTP_dir.fullname
        }
            ) | ?{
                $_.mode -eq '-a---' -and $_.Extension -eq '.tsk'
                    }

$FTP_fileitem=gc (%{
    $FTP_File.fullname
        }
)

$New_FTP=$FTP_fileitem | ?{
    $_.Contains('o:\auto\scripts\vbs\generic\ftps.vbs')
        }

$legacy=$FTP_fileitem | ?{
    $_.contains('o:\auto\scripts\vbs\generic\ftp.vb')
        }

$Dead=$FTP_fileitem | ?{
    $_.contains('o:\auto\scripts\vbs\generic\pushftp.vbs')
        }

$Current_FTP=%{
    $New_FTP | ?{
        $_.startswith('FTP')
            }
}

#$unseditems=%{
    #$FTPs | ?{
        #$_.startswith(!('FTP'))
            #}
#}

############### Clean working copies ###############
#$parent= ($FTPs[0] | Split-Path -NoQualifier | Split-Path -Parent) -split " " | ?{$_ -ne "\auto\scripts\vbs\generic\ftps.vbs"} 
#$child=(-split (Split-Path $FTPs -Leaf | tee -Variable child) | %{$_ -split ".",0, "simplematch"} | %{$_ -split '(:)'})
###################################################

#$parent= ($FTPs[0] | Split-Path -NoQualifier | Split-Path -Parent) -split " " | ?{$_ -ne "\auto\scripts\vbs\generic\ftps.vbs"} 
#$child=(-split (Split-Path $FTPs -Leaf | tee -Variable child) | %{$_ -split ".",0, "simplematch"} | %{$_ -split '(:)'})

#$Filename=@(foreach ($x in $FTPs){-split (Split-Path $x -Leaf ) | %{$_ -split ".",0, "simplematch"} | %{$_ -split '(:)'} |
#Select-Object -Index 0})
#$Ext=@(foreach ($x in $FTPs){-split (Split-Path $x -Leaf ) | %{$_ -split ".",0, "simplematch"} | %{$_ -split '(:)'} |
#Select-Object -Index 1})
#$Prefix=@(foreach ($x in $FTPs){-split (Split-Path $x -Leaf ) | %{$_ -split ".",0, "simplematch"} | %{$_ -split '(:)'} |
#Select-Object -Index 5})
#$Suffix=@(foreach ($x in $FTPs){-split (Split-Path $x -Leaf ) | %{$_ -split ".",0, "simplematch"} | %{$_ -split '(:)'} |
#Select-Object -Index 7})


################################################### FTP Child Item ###################################################

$cur_parent= (
    $Current_FTP | Split-Path -NoQualifier | Split-Path -Parent
        ) -split " " | ?{
            $_ -ne "\auto\scripts\vbs\generic\ftps.vbs"
                } 

$cur_child=(-split (
    Split-Path $Current_FTP -Leaf | tee -Variable child) | %{
        $_ -split ".",0, "simplematch"
        } | %{
            $_ -split '(:)'
                }
    )

$Filename=@(
    foreach ($x in $Current_FTP){
        -split (Split-Path $x -Leaf) | %{
            $_ -split ".",0, "simplematch"
                } | %{
                    $_ -split '(:)'
                        } |
                            Select-Object -Index 0
                                }
)

$Ext=@(
    foreach ($x in $Current_FTP){
    -split (Split-Path $x -Leaf) | %{
        $_ -split ".",0, "simplematch"
            } | %{
                $_ -split '(:)'
                    } |
                        Select-Object -Index 1
                            }
)

$Prefix=@(
    foreach ($x in $Current_FTP){
            -split (Split-Path $x -Leaf) | %{
        $_ -split ".",0, "simplematch"
            } | %{
                $_ -split '(:)'
                    } |
                        Select-Object -Index 5
                            }
)

$Suffix=@(
    foreach ($x in $Current_FTP){
        -split (Split-Path $x -Leaf) | %{
            $_ -split ".",0, "simplematch"
                } | %{
                    $_ -split '(:)'
                        } |
                            Select-Object -Index 7
                                }
)

$children=@()

for ($i=0; $i -lt $Current_FTP.Count; $i++){
    
    $children+= "" | Select-Object -Property  @{
        label='Filename';expression={$Filename[$i]}
            },@{
                label='Ext';expression={$Ext[$i]}
                    },@{
                        label='Pre';expression={$Prefix[$i]}
                            },@{label='Suf';expression={$Suffix[$i]}
                                }
}

$children | epcsv -NoTypeInformation -append -path c:\childout_010813.csv

################################################### FTP Parent/Path Item ###################################################

$source=$null
$remote=$null

$source=@(
    foreach ($y in $Current_FTP){
        ($y |Split-Path -Parent | Split-Path -NoQualifier)-split " " | ?{
            $_ -ne "\auto\scripts\vbs\generic\ftps.vbs"
                } |
                    Select-Object -Index 0
    }
)

#$source.replace("o:","O:")

$remote=@(
    foreach ($x in $Current_FTP){
        ($x | Split-Path -Parent | Split-Path -NoQualifier)-split " " | ?{
            $_ -ne "\auto\scripts\vbs\generic\ftps.vbs"
                } |
                    Select-Object -Index 1
        }
)

$parents=@()

for ($i=0; $i -lt $Current_FTP.Count; $i++){

    #$Filename[$i]
    
  $parents+= "" | Select-Object -Property  @{
    label='Source';expression={$source[$i]}
        },@{
            label='Remote';expression={$remote[$i]}
                }
}

$parents | epcsv -NoTypeInformation -path c:\parentout_010813.csv

$list=@()

for ($i=0; $i -lt $Current_FTP.Count; $i++){

        
  $list+= "" | Select-Object -Property  @{
    label='Source';expression={
        $source[$i]}
            },@{
                label='Remote';expression={$remote[$i]+"\"}
                    },@{
                        label='Filename';expression={$Filename[$i]}
                            },@{
                                label='Ext';expression={$Ext[$i]}
                                    },@{
                                        label='Prefix';expression={$Prefix[$i]}
                                            },@{
                                                label='Suffix';expression={$Suffix[$i]}
                                                    }
}

$list | epcsv -NoTypeInformation -path c:\ftplistout_010813.csv

$source=$null
$parents=$null
$list=$null
