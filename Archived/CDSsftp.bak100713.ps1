﻿#* FileName: CDSsftp.ps1
#*=============================================================================
#* Script Name: [CDS SFTP]
#* Created: [25/06/2013]
#* Author: Seb Sharr, logging - Damar Johnson
#* Company: Exchange Data International
#* Email: s.sharr@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: Listen for CDS file from and upload to .COM
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [03/07/2013]
#* Time: [12:00]
#* Issue: Original logging set by Seb from file server, need OP's solution
#* Solution: Ammend logging to point to OP's log centre for Fixed Income
#* Issue: No error handling or retry attempt's
#* Solution: Add retry attempt and correct upload errors monthly vs daily
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: LOGFILE, LOGTHIS, THISFTP, UPLOADFTP, DOWNLOADFTP, PROCESSTHISFILE
# Created: [....]
# Author: Seb Sharr, Damar Johnson
# Arguments:
# =============================================================================
# Purpose: 
#
#* Set OPslog centre logging
#* Create FTP connection
#* Upload File
#* Download File
#* Copy or move file to correct path
# =============================================================================

############## Local desktop paths set ####################### ---10/07/2013

#$ndump=$args[0]
#$tail=$args[1]

$ndump='CDS'
$tail=''

$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$yd = (Get-Date).AddDays(-1).tostring( "yyMMdd" )
$yyyyd = (Get-Date).AddDays(-1).tostring( "yyyyMMdd" )
$sep='_'
$global:logvalue="$wdate | $global:thestr"
#$global:basedir='O:\AUTO\Tasks\Fixed_Income\CDS'
$global:basedir='C:\cds'

switch ($LASTEXITCODE) {
'0' {$class='ok'}
'1' {$class='failed'}
}

#$Start=(Get-Date)
#$finish=$Start.AddMinutes(10)
#$class

### determine INC and create logpath ###
Function Get-Increment {
#$time=(get-date).hour
### Webload incrementals
       switch ($ndump) {
       
                ### SMF incrementals values
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                ### SMF incrementals values
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                ### 123Trans incrementals values
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                ### CABTrans incrementals values
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                ### Default incremental values
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($tail) {
                       'i' {$global:Fname="$jdate$sep$ndump$inc";$global:logpath="O:\auto\logs\$ndump$inc" ;write-host inc exists!;
                                }
                       '' {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$ndump";$global:logpath="O:\auto\logs\$ndump"}
                       }
}


    function LogThis()
    {
        $global:thestr = $args[0]
        $d = (Get-Date).tostring( "yyyy-MM-dd HH:mm:ss" )
        "$wdate | $thestr" | tee -Variable global:logvalue >> ('C:\cds\'+($jdate)+'_CDSlog.txt')  #$global:basedir\LogCdsPs.txt
        write-output "$d $thestr"
        if ($args[1] -eq "exit") {exit}
        #(Get-Date).tostring( "yyyyMMdd" )
        #(Get-Date).AddDays(-1).tostring( "yyyyMMdd" )  #yesteday date
    }
    
    function thisftp()
    {
        $thisfile = $args[0]
        LogThis "$thisfile.gz FTP started"
        if (test-path "$global:basedir\resultftp.log") { remove-item  $global:basedir\resultftp.log }
        set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
        $a = cFtp -OA -B -u  "$thisfile" "sftp://cds_trans:496h5wFs@109.235.145.77" -log $global:basedir\resultftp.log
        $x = Select-String -Path $global:basedir\resultftp.log -pattern "Total uploaded files:  1"
        if ( $x -eq $null ) 
        {
            LogThis "ERROR $thisfile SFTP unsuccessful"
            exit 0
        }
        LogThis "$thisfile SFTP successful"
    }
    function uploadftp()
    {
        $thisfile = $args[0]
        #LogThis "$thisfile Upload FTP started"
        if (test-path "$global:basedir\resultftp.log") { remove-item  $global:basedir\resultftp.log }
        set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
        cFtp -OA -B -u  "$thisfile" "sftp://cds_trans:496h5wFs@109.235.145.77/Test" -log $global:basedir\resultftp.log
        #Select-String -Path $global:basedir\resultftp.log -pattern "Total uploaded files:  1" | tee -Variable global:x
        Select-String -Path $global:basedir\resultftp.log -pattern "Total uploaded files:  1" | tee -Variable global:uploadx
        switch ($global:uploadx) {
        {$_ -like "C:\cds\resultftp.log:21:Total uploaded files:  0"} {logthis "ERROR $thisfile SFTP Unsuccessfully Uploaded";}            
        {$_ -like "C:\cds\resultftp.log:21:Total uploaded files:  1"} {logthis "$thisfile SFTP Successfully Uplaoded";$global:occur='Uploads:'+$global:count++}
        $null {'No data found'}
}
    }
    function downloadftp() {
        $global:thisfile = $args[0]
            #LogThis "$thisfile Download FTP started"
            if (test-path "$global:basedir\resultftp.log") { remove-item  $global:basedir\resultftp.log }
            set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
            #cFtp -s -d "sftp://edi-cds:3Qhh243n@exchange-data.com/SM7031F_130522.gz" -p F:\cds -log $global:basedir\resultftp.log
            #cFtp -s -d "sftp://edi-cds:3Qhh243n@exchange-data.com/EV7041F_130522.gz" -p F:\cds -log $global:basedir\resultftp.log
            cFtp -s -d "sftp://edi-cds:3Qhh243n@exchange-data.com/$thisfile" -p C:\cds -log $global:basedir\resultftp.log
            Select-String -Path $global:basedir\resultftp.log -pattern "Total downloaded files:  1" | tee -Variable global:x
            switch ($x) {
            $null {LogThis "ERROR $thisfile SFTP download unsuccessful";}            
            (!($null)) {LogThis "$thisfile SFTP Download successful";}
            }
            
            #if ( $x -eq $null ) 
            #{
            #LogThis "ERROR $thisfile SFTP download unsuccessful"
            #exit 1
            #}
            #LogThis "$thisfile SFTP Download successful"
    }

    function processthisfile(){
        $thisf = $args[0]
        Copy-Item d:\inetpub\ftproot\CDS\$thisf d:\inetpub\ftproot\custom\bahar\cds
        if (-not( $? )) {logthis "ERROR $thisf file not copied to \inetpub\ftproot\custom\bahar\cds" "exit"}  
        thisftp "d:\inetpub\ftproot\CDS\$thisf" 
        move-Item d:\inetpub\ftproot\CDS\$thisf $global:basedir\archive
        if (-not( $? )) {logthis "ERROR $thisf file not moved to $global:basedir\archive" "exit"}
        logthis "$thisf processed"
    } 
# ******************** Main ***************************************************************
#****Monthly Files*******
If ((get-date).Day -eq 02) {
    downloadftp (("SBUR_" + $yyyyd + ".TXT.gz")  | tee -variable downval)
    $downres=(Test-Path "'$global:basedir\'$downval")
    switch ($x) {
    {Test-Path "$global:basedir\$thisfile"}  {logthis 'SBUR file Successfully Downloaded'}
    {(!(Test-Path "$global:basedir\$thisfile"))} {"SBUR Download Fail";Do {downloadftp ("SBUR" + $yd + ".gz"); "'Retrying download:'$downval' 'Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 05} until($downres -eq 'true')}
    }
    downloadftp (("SBCR_" + $yyyyd + ".TXT.gz") | tee -Variable downval)
    $downres=(Test-Path "'$global:basedir\'$downval")
    switch ($x) {
    {Test-Path "$global:basedir\$thisfile"}  {logthis 'SBCR file Successfully Downloaded'}
    {(!(Test-Path "$global:basedir\$thisfile"))} {'Download Fail';Do {downloadftp ("SBCR" + $yd + ".gz"); "Retrying download:'$downval Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 05} until($downres -eq 'true')}
    }
        uploadftp ("C:\cds\SBCR_" + $yyyyd + ".TXT")
        uploadftp ("C:\cds\SBUR_" + $yyyyd + ".TXT")
    }
Else
    {
    'No monthly files to be downloaded' | tee -variable thestr
    }

#****Daily Files*******
downloadftp (("EV7041F_" + $yd + ".gz") | tee -variable downval)
$downres=(Test-Path "'$global:basedir\'$downval")
switch ($x) {
{Test-Path "$global:basedir\$thisfile"}  {logthis "$downval file Successfully Downloaded"}
{(!(Test-Path "$global:basedir\$thisfile"))} {'Download Fail';Do {downloadftp ("EV7041F_" + $yd + ".gz"); "Retrying download:$downval Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 05} until($downres -eq 'true')}
}

downloadftp (("SM7031F_" + $yd + ".gz") | tee -variable downval)
$downres=(Test-Path "'$global:basedir\'$downval")
switch ($x) {
{Test-Path "$global:basedir\$thisfile"}  {logthis "$downval file Successfully Downloaded"}
{(!(Test-Path "$global:basedir\$thisfile"))} {write-host Download Fail;Do {downloadftp ("SM7031F" + $yd + ".gz"); "Retrying download:$downval Attempt:"+$count++ | tee -variable downlog;logthis $downlog | Out-Null; start-sleep -Seconds 05 } until($downres -eq 'true')}
}
uploadftp ("c:\cds\EV7041F_" + $yd + ".gz")
uploadftp ("c:\cds\SM7031F_" + $yd + ".gz")

#switch ($uploadx) {
#$null {LogThis "ERROR $thisfile SFTP upload unsuccessful";}            
#(!($null)) {LogThis "$thisfile SFTP uplaod successful";}
#        }

#downloadftp (("SBUR_" + $yyyyd + ".TXT.gz")  | tee -variable downval)

#uploadftp ("f:\cds\SBCR_" + $yyyyd + ".TXT")
#uploadftp ("f:\cds\SBUR_" + $yyyyd + ".TXT")

$global:logvalue="$wdate | $global:thestr" #| Out-Null

switch ($occur) {
('Uploads:1') {logthis 'Both Daily files uploaded successfully'}
Uploads:0 {logthis '1  of 2 Daily files uploaded successfully'}
}

Function Set-Logpath {
### Determine file log path
        IF (!(Test-Path -Path $logpath))
            { write-host No logpath found! Creating Directory; mkdir $logpath }
        Else
            {Write-Host Current logpath exists}    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                { write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$class'>$logvalue</span></p>"}
                Else
                { write-host Appending to file!; ac -Path "$logpath\$Fname.html" -Value "<p><span class='$class'>$logvalue</span></p>" -Force}
}
#OP's logging
Get-Increment
Set-Logpath

exit $LASTEXITCODE