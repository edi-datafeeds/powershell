﻿#* FileName: SLA_check.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose:  Periodically check DB for feed SLA/ETA state
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [28/08/2013]
#* Time: [12:00]
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Determine which incrment to apply
#
#
# =============================================================================

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$false,Position=0,
ParametersetName='DataBase'
)]
#[switch]

[Parameter(
mandatory=$true
)]
[string]$h='MyDev', #Server to Check

[Parameter(
mandatory=$true,Position=1
)]
[string]$dbs='wca', #Database to Check

[parameter(
mandatory=$false
)]
[string]$i, #Increment

[parameter(Mandatory=$false)]
[string]$logfile='OPS-test',

[parameter(
mandatory=$false
)]
[string]$alert

)

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

#------------------------- Check Argument -------------------------#
    If ((!($h))){
            Write-Host 'No DB Server Specified!'
                    msgbox 'No DB Server Specified!'
    }
    ElseIf ((!($dbs))){
            Write-Host No Database specified!
                    msgbox 'No Database Specified!'
    }
    Else{}

    switch($dbs){
    'smf4'{$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"}
    'wca'{$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"}
    default {$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"}
    }

#------------------------- Set alert function -------------------------#


#$time=(get-date).hour
$time=(get-date).ToShortTimeString()
$hmtime=$time.Split(':')
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'
$global:class='ok'

### Logs step and output of entire script
#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

$global:logvalue=@()

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       #switch ($logfile) {
       switch ($h) {
       
                #------------------------- WCA incrementals values -------------------------#
                'HAT_MY_Diesel'
                    {switch ($time) {
                    #{switch ($hmtime[0]) {
                        { $_ -ge '13:30' -and $_ -le '17:00'} {$global:inc='_2'}
                        {$_ -ge '17:30' -and $_ -le '00:00'} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- WCA incrementals values -------------------------#
                'MyDev'
                    {switch ($time) {
                    #{switch ($hmtime[0]) {
                        { $_ -ge '13:30' -and $_ -le '17:00'} {$global:inc='_2'}
                        {$_ -ge '17:30' -and $_ -le '00:00'} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF4'
                    {switch ($time) {
                        {$_ -ge '12:05' -and $_ -le '18:00'} {write-host 'SMF Inc2';$global:inc='_1'}
                        {$_ -ge '18:00' -and $_ -le '00:00'} {write-host 'SMF Inc3';$global:inc='_2'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge '12:00' -and $_ -le '14:00'} {$global:inc='_1'}
                        {$_ -ge '17:00' -and $_ -le '00:00'} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge '11:00' -and $_ -le '13:00'} {$global:inc='_1'}
                        {$_ -ge '14:00' -and $_ -le '15:00'} {$global:inc='_2'}
                        {$_ -ge '16:00' -and $_ -le '17:00'} {$global:inc='_3'}
                        {$_ -ge '18:00' -and $_ -le '19:00'} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                #'t15022_inc' 
                'damar-test'
                    {
                        switch ($time) {
                            {$_ -ge '12:00' -and $_ -le '16:00'} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge '11:00' -and $_ -le '17:00'} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                        #}
                            }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        #{ $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge '14:45' -and $_ -le '17:00'} {$global:inc='_2'}
                        {$_ -ge '17:30' -and $_ -le '00:00'} {$global:inc='_3'}
                        #default {Write-host 'default inc1';$global:inc='_1'}
                        default {$global:inc='_1'}
                        }
                    }
                     
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       #default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       default {$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        #$hold=@()
        #$global:thestr = $args[0]
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output "$d $displaytxt" #;exit
        #$hold.count
        $global:logvalue
        #if ($args[1] -eq "exit") {exit}
    }

#################################################################
### Set Connection to MySQL DB and execute query ###

## Set MySQL Connectiion
[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection

#------------------------- NET FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith('DOTNET')}
#$c=$b.split("`t")
#$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
#$NETpass=$d
#$NETcred=New-Object System.Management.Automation.PsCredential “channel7“,$NETpass

#------------------------- COM FTP Credentials -------------------------#
#$password=gc 'O:\AUTO\Scripts\Powershell\PSProfile\Passwords\CDSFTP.txt' | ConvertTo-SecureString
#$b=gc 'O:\auto\Configs\DBServers.cfg' | ?{$_.startswith("$s")}
$b=gc '\\192.168.2.163\ops\auto\Configs\DBServers.cfg' | ?{$_.startswith("$h")}
$c=$b.split("`t")
$d=ConvertTo-SecureString $c[3] -AsPlainText -Force
$COMpass=$d
#$COMcred=New-Object System.Management.Automation.PsCredential $d[2],$d[3]
$server=$c[4]
$id=$c[2]
$pwd=$c[3]
#$id='auto_ops'
#$pwd='AK47ops;'
#$server='192.168.12.160'
#$server='192.168.12.109'

#$server='67.227.167.146'
#$db='OpsSLA'
#$db='smf4'
#$id='sa'
#$id='auto_ops'
#$pwd='AK47ops;'
#$pwd='K376:lcnb'

#------------------------- DB connections -------------------------#

## Default
$myconnection.Connectionstring="server=$server;database='';Persist Security Info=false;user id=$id;pwd=$pwd"

## OTRS
#$myconnection.Connectionstring = "server=192.168.12.202;database=test;Persist Security Info=false;user id=otrs;pwd=hot"
## Diesel
#$myconnection.Connectionstring="server=$server;database=$db;Persist Security Info=false;user id=$id;pwd=$pwd"
## LiquidWeb
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"
## iCLOUD
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"

$myconnection.open()

#Set query command
#$Command= New-Object MySql.Data.MySqlClient.MySqlCommand
#$Command.Connection=$myconnection

#------------------------- Queries -------------------------#

$show_all= "SHOW TABLES"
#$SMF="SELECT * FROM smf4.tbl_opslog order by acttime desc;"
$SMF="SELECT Feeddate FROM smf4.tbl_opslog order by acttime asc;"
$WCA="SELECT feeddate FROM wca.tbl_opslog order by acttime asc;"
$test="SELECT feeddate,seq FROM wca.tbl_opslog order by acttime asc;"
#$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"

$command=$myconnection.CreateCommand()
#$command.CommandText=$join
#$command.CommandText=$SMF
#$command.CommandText=$WCA
#$command.CommandText=$test

switch($dbs){
    'smf4'{$run="SELECT feeddate FROM $dbs.tbl_opslog order by acttime asc;";
            #$feedate=$collection[-2];
            #$seq=$collection[-1]
                #switch($collection[-1]){
                    #'12:00:00 '{$feed_inc='12:00'}
                    #'18:00:00 '{$feed_inc='18:00'}
                #}
        }
    'wca'{$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"
            #$feedate=$collection[-3]
        }
    default {$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"
        $feedate=$collection[-3]
        }
}

$command.CommandText=$run
#------------------------- Output Results -------------------------#

$reader=$Command.ExecuteReader()

$dataset=New-Object System.Data.DataSet
#$dataset = New-Object System.Data.DataSet
#Write-Output $reader.GetValue(0).ToString()

$dataset.Tables[0]
#while($myreader.Read()){$myreader.GetString(0)}

$collection=@()

while 
($reader.Read()) {
    for ($ii=0; $ii -lt $reader.FieldCount; $ii++) {
              #$reader.GetValue($i).ToString() | tee -Variable value
              $collection+=$reader.GetValue($ii).ToString().Split(' ') #;Write-Host $reader.GetValue($ii).ToString() #-ea SilentlyContinue
    }
}

$myconnection.Close()

$today=(get-date -Format g).Split(' ')
#$feedate=$value.Split(' ')
#$feedate=$collection[-3]

switch($dbs){
    'smf4'{$run="SELECT feeddate FROM $dbs.tbl_opslog order by acttime asc;";
            $feedate=$collection[-2];
            $seq=$collection[-1]
                switch($collection[-1]){
                    '12:00:00'{$feed_inc='12:00:00'}
                    '18:00:00'{$feed_inc='18:00:00'}
                        }
        }
    'wca'{$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"
            $feedate=$collection[-3]
        }
    default {$run="SELECT feeddate,seq FROM $dbs.tbl_opslog order by acttime asc;"
        $feedate=$collection[-3]
        }
}
Get-Increment
#Set-Logpath

#switch($collection[-1]){
#$global:inc[1]{Write-Host 'correct inc'}
#default {write-host 'No Value'}
#}

switch ($feedate){
    $today[0] {switch($dbs){
                #$global:inc[1] {write-host 'Dance'}
                #$_{write-host "$s/$dbs Database upto date! DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                'smf4'{switch($x){
                        {$collection[-1] -eq $feed_inc}{write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                         default{'SMF Fail'}
                            }
                        }
                'wca'{switch($collection[-1]){
                        {$_ -eq $Global:inc[1]}{write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                         default{'WCA Fail'}
                            }
                        }
                'wca2'{switch($collection[-1]){
                        {$_ -eq $Global:inc[1]}{write-host "DB OK - $h/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                        default{'WCA2 Fail'}
                #{$_ -eq $Global:inc[1]}{write-host "DB OK - $s/$dbs Database upto date - DB FeedDate:" $feedate "Seq:"$collection[-1]; exit 0} #$global:inc[1]}
                #default{write-host 'Feed Inc not upto date'}
                        }
                default {write-host "Errror! Incorrect Increment:" "DB Feeddate:"$feedate "Increment:" $collection[-1] ;exit 2}
                    }
                }
            }
    #default {write-host "Error! $s/$dbs Database is not Upto date: Loader expects file for -"$today[0]; exit 2}
    #default {write-host "Error! $s/$dbs Database is not Upto date: Last file loaded -"$feedate "Seq -" $collection[-1]; exit 2}
    #default {write-host "Error! $s/$dbs Database is not Upto date - Last file loaded:"$feedate "Seq:"$collection[-1]; exit 2}
    default {write-host "Error! $h/$dbs Database is not Upto date - DB Feeddate:"$feedate "Seq:"$collection[-1]; exit 2}
}    
        #write-host "WCA Feed successfully updated! DB FeedDate:" $feedate "Seq:"$collection[-1]} #$global:inc[1]}
    #default {write-host "Todays feed not found:"$today[0] "Last Feed available:" $feedate[0] `n"Please Download Today's file before proceeding"
    #default {write-host "Todays feed not found! Last Feed available -" $feedate `n"Please Download Today's feed before proceeding -" $today[0]

#------------------------- Null Results -------------------------#
#$collection=$null
#$dbs=$null
#$ii=$null
#$pwd=$null