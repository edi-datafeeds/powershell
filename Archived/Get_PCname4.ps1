﻿$OFS='_'

#Working output dir 
#$ndump=$args[0]
$ndump='damartest16'
#$Tfile=$args[1]
$logpath="O:\auto\logs\$ndump"

#Set output path
IF (!(Test-Path -Path $logpath))
{
    mkdir $logpath
}
Else
{
}    
 
cd $logpath

#Test output dir
#$dest="C:\Users\d.johnson\Desktop\dest\name.txt"

#Get PC name
$SID=$(Get-WmiObject Win32_Computersystem).name

#Todays Date & Time
$Date=get-date -Format yyyyMMdd

#$File | Rename-Item -NewName {$_.name + '.html'}
#$File=$File + '.html'
$Fname=$Date,$ndump

#$PCname >> $File

#ni -Name 'dude.txt' -itemtype "file" -Value "<p>spanclassnote<$PCname></p>"

IF (!(Test-Path $logpath -include $Fname))
{
ni -Name "$Fname.html" -itemtype "file" -Value "<p><span class='note'><$SID> </span></p>"
}
Else
{
Add-Content -Path "$Fname.html" -Value $SID -Force;
}