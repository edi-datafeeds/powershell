﻿#* FileName: check-loader.ps1
#*=============================================================================
#* Script Name: [Check Loader]
#* Created: [25/06/2013]
#* Author:  Damar Johnson
#* Company: Exchange Data International
#* Email: s.sharr@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: Listen for CDS file from .COM FTP server and upload to .NET
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [10/07/2013]
#* Time: [12:00]
#* Issue: Original logging set by Seb from file server, need OP's solution
#* Solution: Ammend logging to point to OP's log centre for Fixed Income
#* Issue: No error handling or retry attempt's
#* Solution: Add retry attempt and correct upload errors monthly vs daily
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: LOGFILE, LOGTHIS, THISFTP, UPLOADFTP, DOWNLOADFTP, PROCESSTHISFILE
# Created: [....]
# Author: Seb Sharr, Damar Johnson
# Arguments:
# =============================================================================
# Purpose: 
#
#* Set OPslog centre logging
#* Create FTP connection
#* Upload File
#* Download File
#* Copy or move file to correct path
# =============================================================================

########## Script to determine if x process has run on schedule ##########

#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\PSloader.log -Append -Force

####### Range & Treshold settings ##################################
#Metric={time},ok={05},warn={10},crit={15},unit={Minute},prefix={}

### Threshold Range ###
$Start=get-date
$Finish=$Start.AddMinutes(15)
##Test Time spam##
#$Start=get-date
#$Finish=$Start.AddMinutes(02)

### Set functions ###
function Get-OpsState ($x='wscript') {
        Get-WmiObject win32_process -Filter "Name like '%$x%'" #| Select-Object CommandLine
}

### Captures commandline of running process ###
#$Tcommandline=Get-OpsState
#$Goods=$args[1]
$Goods=Get-OpsState
$Tcommandline=$Goods | Select-Object CommandLine

#if ($Tcommandline -eq $null) {
#write-host 'Loader has failed to start';exit 1
#else
#{}
#}

### threshold functions ###
function NOVALUE {
    If ($Dur -ge $Start.AddMinutes(05))
    {
    Write-Host 'No Loader Found'; exit 1
    }
    elseif ($Finish -ge $Start.AddMinutes(10))
    {
    Write-Host 'No Loader Found'; exit 2
    }
    else
    {Write-Host 'TEST:No Loader Found'; exit 2}
}

function WCA {
    If ($Dur -ge $Start.AddMinutes(10))
    {
    Write-Host 'WCA Loader is delayed'; exit 1
    }
    elseif ($Dur -ge $Start.AddMinutes(14))
    {
    Write-Host 'WCA Loader has failed to Initialise'; exit 2
    }
    elseif ($Dur -lt $Start.AddMinutes(14))
    {
    Write-Host 'WCA Loader Successfully Initiated!'; #exit $LASTEXITCODE
    }
}

Function MyWCA {
            If ($Dur -ge $Start.AddMinutes(10))
    {
    Write-Host 'MyWCA Loader is delayed'; exit 1
    }
    elseif ($Dur -ge $Start.AddMinutes(14))
    {
    Write-Host 'MyWCA Loader has failed to Initialise'; exit 2
    }
    elseif ($Dur -lt $Start.AddMinutes(14))
    {
    Write-Host 'MyWCA Loader has successfully Initiated!'; #exit $LASTEXITCODE
    }
}

Function MyDEV_WCA {
            If ($Dur -ge $Start.AddMinutes(10))
    {
    Write-Host 'MyDEV_WCA Loader is delayed'; exit 1
    }
    elseif ($Dur -ge $Start.AddMinutes(14))
    {
    Write-Host 'MyDEV_WCA Loader has failed to Initialise'; exit 2
    }
    elseif ($Dur -lt $Start.AddMinutes(14))
    {
    Write-Host 'MyDEV_WCA Loader has successfully Initiated!'; #exit $LASTEXITCODE
    }
}
 
$Start

Do {
    ### Set process/loader run times ###
    $Dur=Get-Date
    #switch ($BBBcommandline) {
    
    if ($Tcommandline -eq $null)
    {NOVALUE}
    else {

    switch -Wildcard ($Tcommandline) {
    #$null {write-host 'WCA Loader has failed to start';exit 1;break}
    "*WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i*" {WCA; break}
    "*WCA a MyDev WcaWebload MyDev i*" {MyDEV_WCA; break}
    "*WCA a HAT_MY_Diesel MyWcaWebload_1 DIESEL*" {MyWCA_1; break}
    "*WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL*" {MyWCA_2; break}
    "*WCA a HAT_MY_Diesel MyWcaWebload_3 DIESEL*" {MyWCA_3; break}
    }
         }   ### Verify process/loader is in a running state ### 
            #switch ($Goods) {
            #$null {write-host 'WCA Loader has failed to start';exit 1;break}
            #$Goods.CommandLine.Contains('WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i') {write-host 'WCA Loader Successfully Initiated!'; exit $LASTEXITCODE}
            #$Goods.CommandLine.Contains('WCA a MyDev WcaWebload MyDev i') {write-host 'MyDev_WCA Loader Successfully Initiated!'; exit $LASTEXITCODE}
            #$Goods.CommandLine.Contains('WCA a HAT_MY_Diesel MyWcaWebload_1 DIESEL') {write-host 'MyWCA_1 Loader Successfully Initiated!'; exit $LASTEXITCODE}
            #$Goods.CommandLine.Contains('WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL') {write-host 'MyWCA_2 Loader Successfully Initiated!'; exit $LASTEXITCODE}
            #$Goods.CommandLine.Contains('WCA a HAT_MY_Diesel MyWcaWebload_3 DIESEL') {write-host 'MyWCA_3 Loader Successfully Initiated!'; exit $LASTEXITCODE}
            #}
Start-Sleep -Seconds 10
}
Until ($Dur -ge $Finish) 

#Stop-Transcript

            #if ($Tcommandline -eq $null)
            #{
            #;write-host 'WCA Loader has failed to start';exit 1 #exit $LASTEXITCODE
            #}
            #elseif ($Tcommandline.CommandLine.Contains('WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i'))
            #{
            #write-host 'WCA Loader Successfully Initiated!'; exit $LASTEXITCODE
            #}
            
            #{
            #;write-host 'MyDev_WCA Loader has failed to start';exit 1 #exit $LASTEXITCODE
            #}
            #elseif ($Tcommandline.CommandLine.Contains('WCA a MyDev WcaWebload MyDev i'))
            #{
            #write-host 'MyDev_WCA Loader Successfully Initiated!'; exit $LASTEXITCODE
            #}

            #{
            #;write-host 'MyWCA_1 Loader has failed to start';exit 1 #exit $LASTEXITCODE
            #}
            #elseif ($Tcommandline.CommandLine.Contains('WCA a HAT_MY_Diesel MyWcaWebload_1 DIESEL'))
            #{
            #write-host 'MyWCA_1 Loader Successfully Initiated!'; exit $LASTEXITCODE
            #}

            #{
            #;write-host 'MyWCA_2 Loader has failed to start';exit 1 #exit $LASTEXITCODE
            #}
            #elseif ($Tcommandline.CommandLine.Contains('WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL'))
            #{
            #write-host 'MyWCA_2 Loader Successfully Initiated!'; exit $LASTEXITCODE
            #}

            #{
            #;write-host 'MyWCA_3 Loader has failed to start';exit 1 #exit $LASTEXITCODE
            #}
            #elseif ($Tcommandline.CommandLine.Contains('WCA a HAT_MY_Diesel MyWcaWebload_3 DIESEL'))
            #{
            #write-host 'MyWCA_3 Loader Successfully Initiated!'; exit $LASTEXITCODE
            #}
            
            #{
            #;write-host 'MyWCA_3 Loader has failed to start';exit 1 #exit $LASTEXITCODE
            #}
            #elseif ($Tcommandline.CommandLine.Contains('WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL'))
            #{
            #write-host 'MyWCA_3 Loader Successfully Initiated!'; exit $LASTEXITCODE
            #}

#Switch ($Tcommandline) {
#WCA {'WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i'; break}
#MyDEV_WCA {'WCA a MyDev WcaWebload MyDev i'; break}
#MyWCA_1 {'WCA a HAT_MY_Diesel MyWcaWebload_1 DIESEL'; break}
#MyWCA_2 {'WCA a HAT_MY_Diesel MyWcaWebload_2 DIESEL'; break}
#MyWCA_3 {'WCA a HAT_MY_Diesel MyWcaWebload_3 DIESEL'; break}
#}