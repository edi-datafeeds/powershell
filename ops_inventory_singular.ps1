﻿
#* FileName: ops_inventory.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/09/2014]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose:  Hardware & Software inventory of OP's machines
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: 
#* Time: 
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================

## Static
#Get-WmiObject win32_computersystem -ComputerName OPSVR001 -Credential ws123b\administrator

$pword= ConvertTo-SecureString –String "0verlordx" –AsPlainText -Force
$credential= New-Object –TypeName System.Management.Automation.PSCredential –ArgumentList $User, $PWord

$comp=gc 'O:\AUTO\Scripts\Powershell\Files\inventory\opsservers.txt'
$outfile='O:\AUTO\Scripts\Powershell\Files\inventory\ops_invent.txt'
$x='ws123b'

#foreach ($x in $comp)
Invoke-Command -ScriptBlock{
    $user="$x\administrator"
        $computerSystem = get-wmiobject Win32_ComputerSystem -Computername $x
        $computerBIOS = get-wmiobject Win32_BIOS -Computername $x
        $computerOS = get-wmiobject Win32_OperatingSystem -Computername $x
        $computerCPU = get-wmiobject Win32_Processor -Computername $x
        $computerHDD = Get-WmiObject Win32_LogicalDisk -ComputerName $x -Filter drivetype=3
        $Software=Get-WmiObject -Class Win32_Product -ComputerName $x
            write-host "System Information for: " $computerSystem.Name
            "-------------------------------------------------------" | Out-File -Append $outfile
            "Computer ID: " + $computerSystem.Name | Out-File -Append $outfile
            "Manufacturer: " + $computerSystem.Manufacturer | Out-File -Append $outfile
            "Model: " + $computerSystem.Model | Out-File -Append $outfile
            "Serial Number: " + $computerBIOS.SerialNumber | Out-File -Append $outfile
            "CPU: " + $computerCPU.Name | Out-File -Append $outfile
            "HDD Capacity: "  + "{0:N2}" -f ($computerHDD.Size/1GB) + "GB" | Out-File -Append $outfile
            "HDD Space: " + "{0:P2}" -f ($computerHDD.FreeSpace/$computerHDD.Size) + " Free (" + "{0:N2}" -f ($computerHDD.FreeSpace/1GB) + "GB)" | Out-File -Append $outfile
            "RAM: " + "{0:N2}" -f ($computerSystem.TotalPhysicalMemory/1GB) + "GB" | Out-File -Append $outfile
            "Operating System: " + $computerOS.caption + ", Service Pack: " + $computerOS.ServicePackMajorVersion | Out-File -Append $outfile
            #"User logged In: " + $computerSystem.UserName
            #"Last Reboot: " + $computerOS.ConvertToDateTime($computerOS.LastBootUpTime)
            "-------------------------------------------------------"  | Out-File -Append $outfile
            "Software:" | Out-File -Append $outfile
            $Software.Name | Out-File -Append $outfile
            "-------------------------------------------------------" | Out-File -Append $outfile
}