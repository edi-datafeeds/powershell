﻿<#
Created By: Christian Dark ^_^

Improvements:
 - Listbox lists the subfolders in the source directory
 - Listbox has an auto refresh feature

+-----------------------+--------------+
¦Version                ¦ Compatiable  ¦
+-----------------------+--------------+
¦Windows 10	            ¦ No           ¦
¦Windows Server 2012    ¦ Yes          ¦
¦Windows Server 2012 R2 ¦ No           ¦
¦Windows Server 2008 R2 ¦ Yes          ¦
¦Windows Server 2008    ¦ Yes          ¦
¦Windows Server 2003    ¦ No           ¦
¦Windows 8              ¦ Yes          ¦
¦Windows 7              ¦ Yes          ¦
¦Windows Vista          ¦ No           ¦
¦Windows XP             ¦ No           ¦
¦Windows 2000           ¦ No           ¦
+-----------------------+--------------+
#>
#
###Main Variables###

$sourceFolder = "\\192.168.2.163\prices\FeedArch\P04\"

$iconPath = "C:\Users\c.dark\Pictures\Scripts\EDI_Logo_Improved.ico"
$Scriptpath = $myInvocation.InvocationName

$ftpHostCom = "ftp.exchange-data.com"
$ftpUserCom = "newarch"
$ftpPasswordCom = "Pr0T:0coL"
$ftpHostNet = "ftp.exchange-data.net"
$ftpUserNet = "channel7"
$ftpPasswordNet = "1nc3p:T1oN"

#Defining the WebClient#
$webclient = New-Object -TypeName System.Net.WebClient

###End Main Variables###
#
###Font###

$labelFont1 = New-Object System.Drawing.Font("Lucida Sans", 11, [System.Drawing.FontStyle]::Regular)
$labelFont2 = New-Object System.Drawing.Font("Lucida Sans", 9, [System.Drawing.FontStyle]::Regular)
$listBoxFont = New-Object System.Drawing.Font("Lucida Sans", 11, [System.Drawing.FontStyle]::Regular)
$textFont1 = New-Object System.Drawing.Font("Lucida Sans", 11, [System.Drawing.FontStyle]::Regular)
$textFont2 = New-Object System.Drawing.Font("Lucida Sans", 9, [System.Drawing.FontStyle]::Regular)

###End Font###
#
###Functions###

#TestDate Function - Checks the inputted date is valid#
Function TestDate
{
 Param
(
	[Parameter(Mandatory=$true)]
	$Date
)
(	(		$Date -as [DateTime]) -ne $null)
}

#Create-FtpDirectory - Creates a chosen directroy in an FTP environment#
Function Create-FtpDirectory
{
 Param
(
	[Parameter(Mandatory=$true)]
	[string]
	$ftpDirDestination,
	[Parameter(Mandatory=$true)]
	[string]
	$ftpDirUsername,
	[Parameter(Mandatory=$true)]
	[string]
	$ftpDirPassword
)

	if ($ftpDirDestination -match '\\$|\\\w+$')
	{
		$outputBox.Text = "Destination ($ftpDirDestination) should end with a file name"
	}
	$ftprequest = [System.Net.FtpWebRequest]::Create($ftpDirDestination);
	$ftprequest.Method = [System.Net.WebRequestMethods+Ftp]::MakeDirectory
	$ftprequest.UseBinary = $true
	$ftprequest.Credentials = New-Object System.Net.NetworkCredential($ftpDirUsername,$ftpDirPassword)
	
	try
	{
		[void] $ftprequest.GetResponse();
	}
	catch
	{
		$checkDirectory = [System.Net.FtpWebRequest]::Create($ftpDirDestination);
		$checkDirectory.Method = [System.Net.WebRequestMethods+FTP]::PrintWorkingDirectory;
		$checkDirectory.UseBinary = $true
		$checkDirectory.Credentials = New-Object System.Net.NetworkCredential($ftpDirUsername,$ftpDirPassword)
		[void] $checkDirectory.GetResponse();
	}
}

#Robocopy Help Function - Shows the Robocopy help text#
Function robocopyhelp
{
	$help = robocopy.exe /?
	$helpDoc = $help |Out-String
	$outputBox.text = "`r`n" + "For the FTP option, this will upload everything in the destination " + `
	"file not just the newly copied files." + "`r`n" + $helpDoc
}

Function saveoptions
{
 try
{
	$saveadvanced = """" + $AdvancedOptionsTextBox.Text.ToString() + """"
	$savelogpath = """" + $LogFileTextBox.Text.ToString() + """"
	$noerror = $true
	(		Get-Content $Scriptpath) | ForEach-Object {
		if ($_ | Select-String '^.checkboxS.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxS.Checked}
		elseif ($_ | Select-String '^.checkboxE.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxE.Checked}
		elseif ($_ | Select-String '^.checkboxB.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxB.Checked}
		elseif ($_ | Select-String '^.checkboxSEC.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxSEC.Checked}
		elseif ($_ | Select-String '^.checkboxCOPYALL.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxCOPYALL.Checked}
		elseif ($_ | Select-String '^.checkboxNOCOPY.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxNOCOPY.Checked}
		elseif ($_ | Select-String '^.checkboxSECFIX.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxSECFIX.Checked}
		elseif ($_ | Select-String '^.checkboxPURGE.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxPURGE.Checked}
		elseif ($_ | Select-String '^.checkboxMIR.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxMIR.Checked}
		elseif ($_ | Select-String '^.checkboxMOV.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxMOV.Checked}
		elseif ($_ | Select-String '^.checkboxMOVE.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxMOVE.Checked}
		elseif ($_ | Select-String '^.checkboxMT.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxMT.Checked}
		elseif ($_ | Select-String '^.checkboxA.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxA.Checked}
		elseif ($_ | Select-String '^.checkboxM.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxM.Checked}
		elseif ($_ | Select-String '^.checkboxXC.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxXC.Checked}
		elseif ($_ | Select-String '^.checkboxXN.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxXN.Checked}
		elseif ($_ | Select-String '^.checkboxXO.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxXO.Checked}
		elseif ($_ | Select-String '^.checkboxXX.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxXX.Checked}
		elseif ($_ | Select-String '^.checkboxXL.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxXL.Checked}
		elseif ($_ | Select-String '^.checkboxIS.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxIS.Checked}
		elseif ($_ | Select-String '^.checkboxIT.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxIT.Checked}
		elseif ($_ | Select-String '^.checkboxXJ.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxXJ.Checked}
		elseif ($_ | Select-String '^.checkboxXJD.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxXJD.Checked}
		elseif ($_ | Select-String '^.checkboxXJF.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxXJF.Checked}
		elseif ($_ | Select-String '^.checkboxLog.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxLog.Checked}
		elseif ($_ | Select-String '^.checkboxL.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxL.Checked}
		elseif ($_ | Select-String '^.checkboxV.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxV.Checked}
		elseif ($_ | Select-String '^.checkboxTS.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxTS.Checked}
		elseif ($_ | Select-String '^.checkboxX.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxX.Checked}
		elseif ($_ | Select-String '^.checkboxFP.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxFP.Checked}
		elseif ($_ | Select-String '^.checkboxBYTES.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxBYTES.Checked}
		elseif ($_ | Select-String '^.checkboxUnconfirmed.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxUnconfirmed.Checked}
		elseif ($_ | Select-String '^.checkboxFTPEnable.Checked') {$_ -replace ($_ -split " = ")[1].substring(1), $checkboxFTPEnable.Checked}
		elseif ($_ | Select-String '^.AdvancedOptionsTextBox.Text') {$_.Replace($_.Split(" = ")[1], $saveadvanced)}
		elseif ($_ | Select-String '^.LogFileTextBox.Text') {$_.Replace($_.Split(" = ")[1], $savelogpath)}
		else {$_}
	} | Set-Content $Scriptpath -erroraction stop
}
catch
{
	[System.Windows.Forms.MessageBox]::Show("An error occurred while saving your preferences.","Save Preferences", "Ok", "Error")
	$noerror = $false
}
if ($noerror)
{
	[System.Windows.Forms.MessageBox]::Show("Your preferences have been saved.","Save Preferences", "Ok", "Information")
}
}

#Start Robocopy Function#
Function robocopy
{
 begin
{
	#Recommended Options#
	if ($checkboxNP.Checked) {$switchNP = "/NP"} else {$switchNP = $null} #No Progress - don't display percentage copied

	#Copy Options#
	if ($checkboxS.Checked) {$switchS = "/S"} else {$switchS = $null} #/S :: copy Subdirectories, but not empty ones
	if ($checkboxE.Checked) {$switchE = "/E"} else {$switchE = $null} #/E :: copy subdirectories, including Empty ones. /E is including /S
	if ($checkboxB.Checked) {$switchB = "/B"} else {$switchB = $null} #copy files in Backup mode
	if ($checkboxSEC.Checked) {$switchSEC = "/SEC"} else {$switchSEC = $null} #/SEC :: copy files with SECurity (equivalent to /COPY:DATS)
	if ($checkboxCOPYALL.Checked) {$switchCOPYALL = "/COPYALL"} else {$switchCOPYALL = $null} #COPY ALL file info (equivalent to /COPY:DATSOU)
	if ($checkboxNOCOPY.Checked) {$switchNOCOPY = "/NOCOPY"} else {$switchNOCOPY = $null} #COPY NO file info (useful with /PURGE)
	if ($checkboxSECFIX.Checked) {$switchSECFIX = "/SECFIX"} else {$switchSECFIX = $null} #FIX file SECurity on all files, even skipped files
	if ($checkboxPURGE.Checked) {$switchPURGE = "/PURGE"} else {$switchPURGE = $null} #delete dest files/dirs that no longer exist in source
	if ($checkboxMIR.Checked) {$switchMIR = "/MIR"} else {$switchMIR = $null} #MIRror a directory tree (equivalent to /E plus /PURGE)
	if ($checkboxMOV.Checked) {$switchMOV = "/MOV"} else {$switchMOV = $null} #MOVe files (delete from source after copying)
	if ($checkboxMOVE.Checked) {$switchMOVE = "/MOVE"} else {$switchMOVE = $null} #MOVE files AND dirs (delete from source after copying)
	if ($checkboxMT.Checked) {$switchMT = "/MT"} else {$switchMT = $null} #Do multi-threaded copies with n threads (default 8)
	if ($checkboxA.Checked) {$switchA = "/A"} else {$switchA = $null} #copy only files with the Archive attribute set
	if ($checkboxM.Checked) {$switchM = "/M"} else {$switchM = $null} #copy only files with the Archive attribute and reset it
	if ($checkboxXC.Checked) {$switchXC = "/XC"} else {$switchXC = $null} #eXclude Changed files
	if ($checkboxXN.Checked) {$switchXN = "/XN"} else {$switchXN = $null} #eXclude Newer files
	if ($checkboxXO.Checked) {$switchXO = "/XO"} else {$switchXO = $null} #eXclude Older files
	if ($checkboxXX.Checked) {$switchXX = "/XX"} else {$switchXX = $null} #eXclude eXtra files and directories
	if ($checkboxXL.Checked) {$switchXL = "/XL"} else {$switchXL = $null} #eXclude Lonely files and directories
	if ($checkboxIS.Checked) {$switchIS = "/IS"} else {$switchIS = $null} #Include Same files
	if ($checkboxIT.Checked) {$switchIT = "/IT"} else {$switchIT = $null} #Include Tweaked files
	if ($checkboxXJ.Checked) {$switchXJ = "/XJ"} else {$switchXJ = $null} # eXclude Junction points. (normally included by default)
	if ($checkboxXJD.Checked) {$switchXJD = "/XJD"} else {$switchXJD = $null} #eXclude Junction points for Directories
	if ($checkboxXJF.Checked) {$switchXJF = "/XJF"} else {$switchXJF = $null} #eXclude Junction points for Files
	if ($checkboxL.Checked) {$switchL = "/L"} else {$switchL = $null} #List only - don't copy, timestamp or delete any files
	if ($checkboxX.Checked) {$switchX = "/X"} else {$switchX = $null} #report all eXtra files, not just those selected
	if ($checkboxV.Checked) {$switchV = "/V"} else {$switchV = $null} #produce Verbose output, showing skipped files
	if ($checkboxTS.Checked) {$switchTS = "/TS"} else {$switchTS = $null} #include source file Time Stamps in the output
	if ($checkboxFP.Checked) {$switchFP = "/FP"} else {$switchFP = $null} #include Full Pathname of files in the output
	if ($checkboxBYTES.Checked) {$switchBYTES = "/BYTES"} else {$switchBYTES = $null} #Print sizes as bytes
	if ($checkboxR.Checked) {$switchR = "/R:3"} else {$switchR = $null} #number of Retries on failed copies: default is 1 million
	if ($checkboxW.Checked) {$switchW = "/W:1"} else {$switchW = $null} #Wait time between retries: default is 30 seconds

	#Additional Options#
	if ($AdvancedOptionsTextBox.Text)
	{
		$switchAddition = $AdvancedOptionsTextBox.Text.split(' ')
	}
	else
	{
		$switchAddition = $null
	}

	#Log File#
	if (($checkboxLog.Checked -and $LogFileTextBox.Text))
	{
		if(!(Test-Path -Path $LogFileTextBox.Text))
		{
			$checkpath ="`nError: The logfile path " + """" + $LogFileTextBox.Text + `
			"""" + " doesn't exist!`n"
		}
		$logfile = $LogFileTextBox.Text + "\" + ((Get-Date).ToString('yyyy-MM-dd')) + `
		"_" + $sourceTextBox.Text.Split('\')[-1].Replace(" ","_") + ".txt"
		$switchlogfile = "/TEE", "/LOG+:$logfile"
	}
	else
	{
		$switchlogfile = $null
	}
	if (!($logfile))
	{
		$checklog = "  Log File : The logging is not enabled."
	}
	$outputBox.Text = $checklog, $checkpath
}
 process
{
	#Clear the Output Box#
	$outputBox.Text = ""

	#Reset Progress Bars#
	$copyProgressi = 0
	$ftpProgressi = 0
	$ftpProgressNum = 0
	
	#Reset Output Text#
	$ProgressCopy = ""
	$ProgressFTP = ""

	#Set the Arrays#
	$subDirArray = $SubDirListBox.CheckedItems
	$typeArray = $TypeListBox.CheckedItems
	$ftpArray = $FTPListBox.CheckedItems

	#Option to Keep the Unconfirmed Files#
	If ($checkboxUnconfirmed = $True)
	{
		$UCFiles = 1
	}
	else
	{
		$UCFiles = 0
	}
	
	#Set Source and Destination Paths#
	$sourceFolder = $sourceTextBox.Text
	$destinationFolder = $targetTextBox.Text

	#Format Destination Path is Needed#
	If(($destinationFolder -match "\\$") -eq $True)
	{
		$destinationFolder = $destinationFolder -replace ".$"
	}
	
	#Set Start Date#
	$dtmDate = $FromCalendar.Focus
	$dtmDate = $FromCalendar.SelectionStart
	$startDate = $dtmDate
	
	#Set End Date#
	$dtmDate = $ToCalendar.Focus
	$dtmDate = $ToCalendar.SelectionStart
	$endDate = $dtmDate
	
	foreach($countryCode in $subDirArray)
	{
		#-#######
		$countryCodeFolder = $destinationFolder + "\" + $countryCode + "\"
		$folderExists = Test-Path $countryCodeFolder
		If ($folderExists -ne $True)
		{
			md $countryCodeFolder
		}
		#######-#

		foreach($option in $typeArray)
		{
			If($option -eq "Adjusted")
			{
				$fileExt = "txt"
			}
			else
			{
				$fileExt = "zip"
			}

			If($option -ne "Equities")
			{
				$optionFolderTest = $sourceFolder + "\" + $countryCode + "\" + $option
				$optionFolder = $destinationFolder + "\" + $countryCode + "\" + $option + "\"
				$folderPath = $sourceFolder + $countryCode + "\" + $option

				#-#######
				$folderExists = Test-Path $optionFolder
				If ($folderExists -ne $True)
				{
					md $optionFolder
				}
				#######-#
			}
			else
			{
				$optionFolderTest = $sourceFolder + "\" + $countryCode
				$optionFolder = $destinationFolder + "\" + $countryCode + "\"
				$folderPath = $sourceFolder + $countryCode
			}
			
			$optionExists = Test-Path $optionFolderTest
			
			If ($optionExists -eq $True)
			{
				foreach ($num in (0..((New-TimeSpan -Start $startDate -End $endDate).Days)))
				{
					$a = $StartDate.AddDays($num)
					$fileDate = "{0:yMMdd}" -f [datetime]$a
					
					$row = $table.Rows.Add($tablei)
					$row.CountryCode = $countryCode
					$row.Option = $option
					$row.FileDate = $fileDate
					$row.FileExt = $fileExt
					$row.OptionFolder = $optionFolder

					$tablei++
				}
			}
		}
	}
	
	$ds = new-object System.Data.DataSet
	$ds.Tables.Add("TempTable")
	[void]$ds.Tables["TempTable"].Columns.Add("CountryCode",[string])
	[void]$ds.Tables["TempTable"].Columns.Add("Option",[string])
	[void]$ds.Tables["TempTable"].Columns.Add("FileDate",[string])
	[void]$ds.Tables["TempTable"].Columns.Add("FileExt",[string])
	[void]$ds.Tables["TempTable"].Columns.Add("OptionFolder",[string])
	
	$table | foreach {
		$dr = $ds.Tables["TempTable"].NewRow()
		$dr["CountryCode"] = $_.CountryCode
		$dr["Option"] = $_.Option
		$dr["FileDate"] = $_.FileDate
		$dr["FileExt"] = $_.FileExt
		$dr["OptionFolder"] = $_.OptionFolder
		$ds.Tables["TempTable"].Rows.Add($dr)
	}
	
	#Count the Source Files to be Copied#
	$copyFileCount = ($table|Measure).Count
	
	foreach ($row in $ds.Tables[0].Rows)
	{
		#Setting the Main Variables#
		$countryCode = $row.CountryCode
		$option = $row.Option
		$fileDate = $row.FileDate
		$fileExt = $row.FileExt
		$optionFolder = $row.OptionFolder
		
		If ($fileDate.Length -eq 5)
		{
			$fileDate = "0" + $fileDate
		}
		
		#If Equities, fix folder path#
		If ($option -eq "Equities")
		{
			$option = ""
		}
		
		#Setting Options Folders Variables#
		$sourceFolderRobocopy = $sourceFolder + "\" + $countryCode + "\" + $option
		$destinationFolderRobocopy = $destinationFolder + "\" + $countryCode + "\" + $option
		
		#Setting /IF Variables#
		$robocopyIF1 = $countryCode + "_"
		$robocopyIF2 = "_"
		$robocopyIF3 = $fileDate
		$robocopyIF4 = "." + $fileExt
		
		#Run the Robocopy#
		$run = robocopy.exe $sourceFolderRobocopy $destinationFolderRobocopy $switchNP $switchR `
		$switchW $switchS $switchE $switchB $switchSEC $switchCOPYALL $switchNOCOPY $switchSECFIX `
		$switchPURGE $switchMIR $switchMOV $switchMOVE $switchMT $switchA $switchM $switchXC `
		$switchXN $switchXO $switchXX $switchXL $switchIS $switchIT $switchXJ $switchXJD $switchXJF `
		$switchL $switchX $switchV $switchTS $switchFP $switchBYTES $switchAddition $switchLogfile `
		/IF $robocopyIF1*$robocopyIF2*$robocopyIF3*$robocopyIF4

		#Continue if Error#
		$ErrorActionPreference = "silentlycontinue"

		#Calculate Percentage#
		$copyProgressi++
		[int]$cpct = ($copyProgressi / $copyFileCount)*100

		#Update the Progress Bar#
		$copyProgressBar.Value = ($cpct)
		$outputBox.Text = "Copying $cpct %"
		[void] [System.Windows.Forms.Application]::DoEvents()
	}

	#This Will Delete The Unconfirmed Files if The Option Was Selected#
	if($checkboxUnconfirmed.Checked -eq $False)
	{
		foreach($countryCode in $subDirArray)
		{
			foreach($option in $typeArray)
			{
				If($option -eq "Adjusted")
				{
					$fileExt = "txt"
				}
				else
				{
					$fileExt = "zip"
				}
				If($option -ne "Equities")
				{
					$optionFolderTest = $sourceFolder + $countryCode + "\" + $option
					$optionFolder = $destinationFolder + "\" + $countryCode + "\" + $option + "\"
				}
				else
				{
					$optionFolderTest = $sourceFolder + $countryCode
					$optionFolder = $destinationFolder + "\" + $countryCode + "\"
				}
				$optionExists = Test-Path $optionFolderTest
				If ($optionExists -eq $True)
				{
					$uc = "UC"
					Remove-Item $optionFolder * $uc * .$fileExt
				}
			}
		}
	}
	
	#Copy Progress Finished#
	$CopyProgressBar.Value = 100
	$ProgressCopy = "Copying is Complete..." + "`r`n"
	$outputBox.Text = $ProgressCopy
	[void] [System.Windows.Forms.Application]::DoEvents()
	
	#This Will FTP The Files if The Option Was Selected#
	if($checkboxFTPEnable.Checked -eq $True)
	{
		foreach($ftpHostCheck in $ftpArray)
		{
			#Setting Progress Bar#
			$ftpProgressNum = $ftpProgressNum + 1
		}
		
		#Setting the File Count#
		$ftpFileCount = (Get-ChildItem $destinationFolder -Recurse | Where-Object {!$_.PSIsContainer}| Measure).Count
		$ftpFileCount = $ftpFileCount * $ftpProgressNum
		
		foreach($ftpHostCheck in $ftpArray)
		{
			#Setting the FTP host name#
			if($ftpHostCheck -eq ".Com")
			{
				$ftpHost = $ftpHostCom
				$ftpUser = $ftpUserCom
				$ftpPassword = $ftpPasswordCom
			}
			else
			{
				if($ftpHostCheck -eq ".Net")
				{
					$ftpHost = $ftpHostNet
					$ftpUser = $ftpUserNet
					$ftpPassword = $ftpPasswordNet
				}
			}
			
			#FTP Step#
			$ftpSource = $destinationFolder
			$ftpDestination = "ftp://$ftpHost" + $ftpTargetTextBox.Text
			$webclient.Credentials = New-Object System.Net.NetworkCredential($ftpUser,$ftpPassword)
			$ftpFolders = Get-ChildItem $ftpSource -Recurse | ?{ $_.PSIsContainer } | Select-Object FullName
			
			foreach ($ftpFolderPath in $ftpFolders)
			{
				If($ftpFolderPath.FullName -like "*adjusted*")
				{
					$ftpIncluded = "*.txt"
				}
				else
				{
					$ftpIncluded = "*.zip"
				}
				
				$1source = $ftpSource + "\"
				$2source = $1source -replace ("\\","/")
				$1folderPath = $ftpFolderPath.FullName -replace ("\\","/")
				$2folderPath = $1folderPath -replace ("$2source","")
				$3folderPath = $2folderPath -replace ("/","\")
				$1destination = $ftpDestination + "/" + $2folderPath
				
				Create-FtpDirectory -ftpDirDestination $1destination -ftpDirUsername $ftpUser -ftpDirPassword $ftpPassword
				
				$ftpFiles = Get-ChildItem $ftpFolderPath.FullName -Filter $ftpIncluded
				
				foreach ($ftpFile in $ftpFiles)
				{
					$nsource = $ftpSource -replace ("\\","/")
					$nfile = $ftpFile.FullName -replace ("\\","/")
					$nfile = $nfile -replace ("$nsource","")
					$nfile = $nfile.TrimStart("/")
					$webclient.UploadFile("$ftpDestination/$nfile", $ftpFile.FullName)
					
					#Calculate Percentage#
					$ftpProgressi++
					[int]$fpct = ($ftpProgressi / $ftpFileCount)*100
			
					#Update the Progress Bar#
					$ftpProgressBar.Value = ($fpct)
					$ProgressFTP = "FTPing $fpct %"
					$outputBox.Text = $ProgressCopy + $ProgressFTP
					[void] [System.Windows.Forms.Application]::DoEvents()
				}
			}
			
			#FTP Progress Finished#
			$FTPProgressBar.Value = 100
			$ProgressFTP = "FTPing is Complete..." + "`r`n"
			$outputBox.Text = $ProgressCopy + $ProgressFTP
			[void] [System.Windows.Forms.Application]::DoEvents()
			
			$webclient.Dispose()
		}
	}
}
 end
{
	#Progress Finished#
	$copyProgressBar.Value = 100
	$ftpProgressBar.Value = 100
	$outputBox.Text = $ProgressCopy + $ProgressFTP + "`r`n" + "Process Complete..."
	[void] [System.Windows.Forms.Application]::DoEvents()
}
}

#Stop Robocopy Function#
Function stoprobocopy
{
	if (get-process -Name robocopy -ErrorAction SilentlyContinue)
	{
		Stop-Process -Name robocopy -Force
		$timestamp = (Get-Date).ToString('yyyy/MM/dd hh:mm:ss')
		$outputBox.Text("$timestamp Robocopy process has been terminated.")
	}
	if ($logfile)
	{
		Add-Content $logfile "`n`r$timestamp ERROR Robocopy process has been terminated."
	}
}

#Show Errors Function#
Function showerrors
{
	$logfile = $LogFileTextBox.Text + "\" + ((Get-Date).ToString('yyyy-MM-dd')) + "_" + `
	$sourceTextBox.Text.Split('\')[-1].Replace(" ","_") + ".txt"
	if (!(Test-Path $logfile))
	{
		$outputBox.text = "There is no logfile for the current job."
	}
	else
	{
		$logcontent = Get-Content $logfile
		if ($errors = $logcontent | Select-String -Pattern "ERROR " -Context 0,1 |Out-String)
		{
			$outputBox.text = $errors
		}
		else
		{
			$outputBox.text = "No errors found."
		}
	}
}

#Open Log Function#
Function openlog
{
	$logfile = $LogFileTextBox.Text + "\" + ((Get-Date).ToString('yyyyMMdd')) + "_" + `
	$sourceTextBox.Text.Split('\')[-1].Replace(" ","_") + ".txt"
	if(!(Test-Path $logfile))
	{
		$outputBox.text = "There is no logfile for the current job."
	}
	else
	{
		$openlog = notepad.exe $logfile
	}
}

###End Functions###
#
###MainForm Setup###

[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

$MainForm = New-Object System.Windows.Forms.Form
$MainForm.Size = New-Object System.Drawing.Size(1200,750)
$MainForm.Text = "EDI P04 Application"
$MainForm.StartPosition = "CenterScreen"
#$MainForm.MinimizeBox = $False
$MainForm.MaximizeBox = $False
$MainForm.Icon = $iconPath
$MainForm.BackColor = "#E0FFFF" #Powder blue

###End MainForm Setup###
#
###Group Boxes###

#Path Options Group Box#
$pathGroupBox = New-Object System.Windows.Forms.GroupBox
$pathGroupBox.Location = New-Object System.Drawing.Size(10,15)
$pathGroupBox.size = New-Object System.Drawing.Size(267,115)
$pathGroupBox.text = "Path Options"
$pathGroupBox.Font = $labelFont2
$MainForm.Controls.Add($pathGroupBox)

#   #Note: The Target Path Label and Textbox are above the Source
#      to Focus() on the correct Textbox when the program is
#      first started

#   #Target Path Label#
$targetLabel = New-Object System.Windows.Forms.Label
$targetLabel.Text = "Destination Path:"
$targetLabel.Location = New-Object System.Drawing.Size(10,65)
$targetLabel.Size = New-Object System.Drawing.Size(170,15)
$targetLabel.Font = $labelFont2
$pathGroupBox.Controls.Add($targetLabel)

#   #Target Path Textbox#
$targetTextBox = New-Object System.Windows.Forms.TextBox
$targetTextBox.Text = ""
$targetTextBox.Location = New-Object System.Drawing.Size(10,85)
$targetTextBox.Size = New-Object System.Drawing.Size(250,30)
$targetTextBox.Font = $textFont2
$pathGroupBox.Controls.Add($targetTextBox)

#   #Source Path Label#
$sourceLabel = New-Object System.Windows.Forms.Label
$sourceLabel.Text = "Source Path:"
$sourceLabel.Location = New-Object System.Drawing.Size(10,20)
$sourceLabel.Size = New-Object System.Drawing.Size(170,15)
$sourceLabel.Font = $labelFont2
$pathGroupBox.Controls.Add($sourceLabel)

#   #Source Path Textbox#
$sourceTextBox = New-Object System.Windows.Forms.TextBox
$sourceTextBox.Text = $sourceFolder
$sourceTextBox.Location = New-Object System.Drawing.Size(10,40)
$sourceTextBox.Size = New-Object System.Drawing.Size(250,20)
$sourceTextBox.ReadOnly = $True
$sourceTextBox.Font = $textFont2
$pathGroupBox.Controls.Add($sourceTextBox)

#Copy Options Group Box#
$copyGroupBox = New-Object System.Windows.Forms.GroupBox
$copyGroupBox.Location = New-Object System.Drawing.Size(287,15) 
$copyGroupBox.size = New-Object System.Drawing.Size(220,110) 
$copyGroupBox.text = "Copy Options" 
$copyGroupBox.Font = $labelFont2
$MainForm.Controls.Add($copyGroupBox)

#   #Copy Options#

#   ##
$checkboxS = New-Object System.Windows.Forms.checkbox
$checkboxS.Location = New-Object System.Drawing.Size(10,20)
$checkboxS.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxS.Checked = $True
$checkboxS.Text = "/S"
$checkboxS.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxS)

#   ##
$checkboxE = New-Object System.Windows.Forms.checkbox
$checkboxE.Location = New-Object System.Drawing.Size(10,40)
$checkboxE.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxE.Checked = $False
$checkboxE.Text = "/E"
$checkboxE.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxE)

#   ##
$checkboxB = New-Object System.Windows.Forms.checkbox
$checkboxB.Location = New-Object System.Drawing.Size(10,60)
$checkboxB.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxB.Checked = $False
$checkboxB.Text = "/B"
$checkboxB.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxB)

#   ##
$checkboxSEC = New-Object System.Windows.Forms.checkbox
$checkboxSEC.Location = New-Object System.Drawing.Size(10,80)
$checkboxSEC.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxSEC.Checked = $False
$checkboxSEC.Text = "/SEC"
$checkboxSEC.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxSEC)

#   #COPY ALL file info (equivalent to /COPY:DATSOU)#
$checkboxCOPYALL = New-Object System.Windows.Forms.checkbox
$checkboxCOPYALL.Location = New-Object System.Drawing.Size(70,20)
$checkboxCOPYALL.Size = New-Object System.Drawing.Size(85,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxCOPYALL.Checked = $False
$checkboxCOPYALL.Text = "/COPYALL"
$checkboxCOPYALL.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxCOPYALL)

#   #COPY NO file info (useful with /PURGE)#
$checkboxNOCOPY = New-Object System.Windows.Forms.checkbox
$checkboxNOCOPY.Location = New-Object System.Drawing.Size(70,40)
$checkboxNOCOPY.Size = New-Object System.Drawing.Size(85,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxNOCOPY.Checked = $False
$checkboxNOCOPY.Text = "/NOCOPY"
$checkboxNOCOPY.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxNOCOPY)

#   #FIX file SECurity on all files, even skipped files#
$checkboxSECFIX = New-Object System.Windows.Forms.checkbox
$checkboxSECFIX.Location = New-Object System.Drawing.Size(70,60)
$checkboxSECFIX.Size = New-Object System.Drawing.Size(85,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxSECFIX.Checked = $False
$checkboxSECFIX.Text = "/SECFIX"
$checkboxSECFIX.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxSECFIX)

#   #delete dest files/dirs that no longer exist in source#
$checkboxPURGE = New-Object System.Windows.Forms.checkbox
$checkboxPURGE.Location = New-Object System.Drawing.Size(70,80)
$checkboxPURGE.Size = New-Object System.Drawing.Size(85,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxPURGE.Checked = $False
$checkboxPURGE.Text = "/PURGE"
$checkboxPURGE.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxPURGE)

#   #MIRror a directory tree (equivalent to /E plus /PURGE)#
$checkboxMIR = New-Object System.Windows.Forms.checkbox
$checkboxMIR.Location = New-Object System.Drawing.Size(157,20)
$checkboxMIR.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxMIR.Checked = $False
$checkboxMIR.Text = "/MIR"
$checkboxMIR.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxMIR)

#   #MOVE files (delete from source after copying)#
$checkboxMOV = New-Object System.Windows.Forms.checkbox
$checkboxMOV.Location = New-Object System.Drawing.Size(157,40)
$checkboxMOV.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxMOV.Checked = $False
$checkboxMOV.Text = "/MOV"
$checkboxMOV.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxMOV)

#   #MOVE files AND dirs (delete from source after copying)#
$checkboxMOVE = New-Object System.Windows.Forms.checkbox
$checkboxMOVE.Location = New-Object System.Drawing.Size(157,60)
$checkboxMOVE.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxMOVE.Checked = $False
$checkboxMOVE.Text = "/MOVE"
$checkboxMOVE.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxMOVE)

#   #Do multi-threaded copies with n threads (default 8)#
$checkboxMT = New-Object System.Windows.Forms.checkbox
$checkboxMT.Location = New-Object System.Drawing.Size(157,80)
$checkboxMT.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxMT.Checked = $False
$checkboxMT.Text = "/MT:8"
$checkboxMT.Font = $labelFont2
$copyGroupBox.Controls.Add($checkboxMT)

#File Selection Options Group Box#
$FileSelectionGroupBox = New-Object System.Windows.Forms.GroupBox
$FileSelectionGroupBox.Location = New-Object System.Drawing.Size(517,15)
$FileSelectionGroupBox.size = New-Object System.Drawing.Size(185,110)
$FileSelectionGroupBox.text = "File Selection Options"
$FileSelectionGroupBox.Font = $labelFont2
$MainForm.Controls.Add($FileSelectionGroupBox)

#   #File Selection Options#

#   #copy only files with the Archive attribute set#
$checkboxA = New-Object System.Windows.Forms.checkbox
$checkboxA.Location = New-Object System.Drawing.Size(10,20)
$checkboxA.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxA.Checked = $False
$checkboxA.Text = "/A"
$checkboxA.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxA)

#   #copy only files with the Archive attribute and reset it#
$checkboxM = New-Object System.Windows.Forms.checkbox
$checkboxM.Location = New-Object System.Drawing.Size(10,40)
$checkboxM.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxM.Checked = $False
$checkboxM.Text = "/M"
$checkboxM.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxM)

#   #eXclude changed files#
$checkboxXC = New-Object System.Windows.Forms.checkbox
$checkboxXC.Location = New-Object System.Drawing.Size(10,60)
$checkboxXC.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxXC.Checked = $False
$checkboxXC.Text = "/XC"
$checkboxXC.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxXC)

#   #eXclude Newer files#
$checkboxXN = New-Object System.Windows.Forms.checkbox
$checkboxXN.Location = New-Object System.Drawing.Size(10,80)
$checkboxXN.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxXN.Checked = $False
$checkboxXN.Text = "/XN"
$checkboxXN.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxXN)

#   #eXclude Older files#
$checkboxXO = New-Object System.Windows.Forms.checkbox
$checkboxXO.Location = New-Object System.Drawing.Size(70,20)
$checkboxXO.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxXO.Checked = $False
$checkboxXO.Text = "/XO"
$checkboxXO.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxXO)

#   #eXclude eXtra files and directories#
$checkboxXX = New-Object System.Windows.Forms.checkbox
$checkboxXX.Location = New-Object System.Drawing.Size(70,40)
$checkboxXX.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxXX.Checked = $False
$checkboxXX.Text = "/XX"
$checkboxXX.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxXX)

#   #eXclude Lonely files and directories#
$checkboxXL = New-Object System.Windows.Forms.checkbox
$checkboxXL.Location = New-Object System.Drawing.Size(70,60)
$checkboxXL.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxXL.Checked = $False
$checkboxXL.Text = "/XL"
$checkboxXL.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxXL)

#   #Include Same files#
$checkboxIS = New-Object System.Windows.Forms.checkbox
$checkboxIS.Location = New-Object System.Drawing.Size(70,80)
$checkboxIS.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxIS.Checked = $False
$checkboxIS.Text = "/IS"
$checkboxIS.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxIS)

#   #Include Tweaked files#
$checkboxIT = New-Object System.Windows.Forms.checkbox
$checkboxIT.Location = New-Object System.Drawing.Size(130,20)
$checkboxIT.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxIT.Checked = $False
$checkboxIT.Text = "/IT"
$checkboxIT.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxIT)

#   #eXclude Junction points#
$checkboxXJ = New-Object System.Windows.Forms.checkbox
$checkboxXJ.Location = New-Object System.Drawing.Size(130,40)
$checkboxXJ.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxXJ.Checked = $False
$checkboxXJ.Text = "/XJ"
$checkboxXJ.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxXJ)

#   #eXclude Junction points for Directories#
$checkboxXJD = New-Object System.Windows.Forms.checkbox
$checkboxXJD.Location = New-Object System.Drawing.Size(130,60)
$checkboxXJD.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxXJD.Checked = $False
$checkboxXJD.Text = "/XJD"
$FileSelectionGroupBox.Controls.Add($checkboxXJD)

#   #eXclude Junction points for Files#
$checkboxXJF = New-Object System.Windows.Forms.checkbox
$checkboxXJF.Location = New-Object System.Drawing.Size(130,80)
$checkboxXJF.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxXJF.Checked = $False
$checkboxXJF.Text = "/XJF"
$checkboxXJF.Font = $labelFont2
$FileSelectionGroupBox.Controls.Add($checkboxXJF)

#Recommended Options Group Box#
$RecommendedGroupBox = New-Object System.Windows.Forms.GroupBox
$RecommendedGroupBox.Location = New-Object System.Drawing.Size(712,15)
$RecommendedGroupBox.size = New-Object System.Drawing.Size(223,50)
$RecommendedGroupBox.text = "Recommended Options"
$RecommendedGroupBox.Font = $labelFont2
$MainForm.Controls.Add($RecommendedGroupBox)

#   #Recommended Options#

#   #No Progress - don't display percentage copied
$checkboxNP = New-Object System.Windows.Forms.checkbox
$checkboxNP.Location = New-Object System.Drawing.Size(10,20)
$checkboxNP.Size = New-Object System.Drawing.Size(50,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxNP.Checked = $True
$checkboxNP.Text = "/NP"
$RecommendedGroupBox.Controls.Add($checkboxNP)

#   #number of Retries on failed copies: default 1 million (this is setting it to 3)
$checkboxR = New-Object System.Windows.Forms.checkbox
$checkboxR.Location = New-Object System.Drawing.Size(70,20)
$checkboxR.Size = New-Object System.Drawing.Size(50,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxR.Checked = $True
$checkboxR.Text = "/R:3"
$RecommendedGroupBox.Controls.Add($checkboxR)

#   #Wait time between retries: default is 30 seconds (this is setting it to 1)
$checkboxW = New-Object System.Windows.Forms.checkbox
$checkboxW.Location = New-Object System.Drawing.Size(130,20)
$checkboxW.Size = New-Object System.Drawing.Size(55,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxW.Checked = $True
$checkboxW.Text = "/W:1"
$RecommendedGroupBox.Controls.Add($checkboxW)

#Advanced Options Group Box#
$AdvancedGroupBox = New-Object System.Windows.Forms.GroupBox
$AdvancedGroupBox.Location = New-Object System.Drawing.Size(712,75)
$AdvancedGroupBox.Size = New-Object System.Drawing.Size(223,50)
$AdvancedGroupBox.Text = "Advanced Options:"
$AdvancedGroupBox.Font = $labelFont2
$MainForm.Controls.Add($AdvancedGroupBox)

#   #Advanced Options Input#
$AdvancedOptionsTextBox = New-Object System.Windows.Forms.TextBox
$AdvancedOptionsTextBox.Text="/LEV:1"
$AdvancedOptionsTextBox.Location = New-Object System.Drawing.Size(10,20)
$AdvancedOptionsTextBox.Size = New-Object System.Drawing.Size(203,30)
$AdvancedGroupBox.Controls.Add($AdvancedOptionsTextBox)

#Log File Path Group Box#
$LogFileGroupbox = New-Object System.Windows.Forms.GroupBox
$LogFileGroupbox.Location = New-Object System.Drawing.Size(945,15)
$LogFileGroupbox.Size = New-Object System.Drawing.Size(225,50)
$LogFileGroupbox.Text="Logfile Path"
$LogFileGroupbox.Font = $labelFont2
$MainForm.Controls.Add($LogFileGroupbox)

#   #Log File Path Input#
$LogFileTextBox = New-Object System.Windows.Forms.TextBox
$LogFileTextBox.Text = ""
$LogFileTextBox.Location = New-Object System.Drawing.Size(10,20) 
$LogFileTextBox.Size = New-Object System.Drawing.Size(205,30) 
$LogFileGroupbox.Controls.Add($LogFileTextBox)

#Logging Options Group Box#
$LoggingGroupBox = New-Object System.Windows.Forms.GroupBox
$LoggingGroupBox.Location = New-Object System.Drawing.Size(945,75)
$LoggingGroupBox.Size = New-Object System.Drawing.Size(225,90)
$LoggingGroupBox.Text = "Logging Options"
$LoggingGroupBox.Font = $labelFont2
$MainForm.Controls.Add($LoggingGroupBox)

#   #Logging Options#

#   #Enable Logging checkbox#
$checkboxLog = New-Object System.Windows.Forms.checkbox
$checkboxLog.Location = New-Object System.Drawing.Size(10,20)
$checkboxLog.Size = New-Object System.Drawing.Size(115,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxLog.Checked = $False
$checkboxLog.Text = "Enable Logging"
$checkboxLog.Font = $labelFont2
$LoggingGroupBox.Controls.Add($checkboxLog)

#   #List only - don't copy, timestamp or delete any files#
$checkboxL = New-Object System.Windows.Forms.checkbox
$checkboxL.Location = New-Object System.Drawing.Size(10,40)
$checkboxL.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxL.Checked = $False
$checkboxL.Text = "/L"
$LoggingGroupBox.Controls.Add($checkboxL)

#   #produce Verbose output, showing skipped files#
$checkboxV = New-Object System.Windows.Forms.checkbox
$checkboxV.Location = New-Object System.Drawing.Size(70,40)
$checkboxV.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxV.Checked = $False
$checkboxV.Text = "/V"
$LoggingGroupBox.Controls.Add($checkboxV)

#   #include source file Time Stamps in the output#
$checkboxTS = New-Object System.Windows.Forms.checkbox
$checkboxTS.Location = New-Object System.Drawing.Size(130,40)
$checkboxTS.Size = New-Object System.Drawing.Size(90,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxTS.Checked = $False
$checkboxTS.Text = "/TS"
$LoggingGroupBox.Controls.Add($checkboxTS)

#   #report all eXtra files, not just those selected
$checkboxX = New-Object System.Windows.Forms.checkbox
$checkboxX.Location = New-Object System.Drawing.Size(10,60)
$checkboxX.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxX.Checked = $False
$checkboxX.Text = "/X"
$LoggingGroupBox.Controls.Add($checkboxX)

#   #include Full Pathname of files in the output#
$checkboxFP = New-Object System.Windows.Forms.checkbox
$checkboxFP.Location = New-Object System.Drawing.Size(70,60)
$checkboxFP.Size = New-Object System.Drawing.Size(60,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxFP.Checked = $False
$checkboxFP.Text = "/FP"
$LoggingGroupBox.Controls.Add($checkboxFP)

#   #Print sizes as bytes#
$checkboxBYTES = New-Object System.Windows.Forms.checkbox
$checkboxBYTES.Location = New-Object System.Drawing.Size(130,60)
$checkboxBYTES.Size = New-Object System.Drawing.Size(90,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxBYTES.Checked = $False
$checkboxBYTES.Text = "/BYTES"
$LoggingGroupBox.Controls.Add($checkboxBYTES)

#P04 Date Picker Group Box#
$P04DateGroupBox = New-Object System.Windows.Forms.GroupBox
$P04DateGroupBox.Location = New-Object System.Drawing.Size(287,135)
$P04DateGroupBox.Size = New-Object System.Drawing.Size(648,190)
$P04DateGroupBox.Text = "P04 Date Picker"
$P04DateGroupBox.Font = $labelFont2
$MainForm.Controls.Add($P04DateGroupBox)

#   #From Label
$FromDateLabel = New-Object System.Windows.Forms.Label
$FromDateLabel.Text = "From:"
$FromDateLabel.Location = New-Object System.Drawing.Size(10,90)
$FromDateLabel.Size = New-Object System.Drawing.Size(50,15)
$FromDateLabel.Font = $labelFont1
$P04DateGroupBox.Controls.Add($FromDateLabel)

#   #To Label
$ToDateLabel = New-Object System.Windows.Forms.Label
$ToDateLabel.Text = "To:"
$ToDateLabel.Location = New-Object System.Drawing.Size(330,90)
$ToDateLabel.Size = New-Object System.Drawing.Size(30,15)
$ToDateLabel.Font = $labelFont1
$P04DateGroupBox.Controls.Add($ToDateLabel)

#   #From Date Picker
$FromCalendar = New-Object System.Windows.Forms.MonthCalendar
#$FromCalendar.Location = New-Object System.Drawing.Size(99,20)
$FromCalendar.Location = New-Object System.Drawing.Size(69,20)
$FromCalendar.ShowTodayCircle = $False
$FromCalendar.MaxSelectionCount = 1
$P04DateGroupBox.Controls.Add($FromCalendar)

#   #To Date Picker
$ToCalendar = New-Object System.Windows.Forms.MonthCalendar
#$ToCalendar.Location = New-Object System.Drawing.Size(339,20)
$ToCalendar.Location = New-Object System.Drawing.Size(369,20)
$ToCalendar.ShowTodayCircle = $False
$ToCalendar.MaxSelectionCount = 1
$P04DateGroupBox.Controls.Add($ToCalendar)

#   #P04 Country Code Options#

#	#P04 Sub Directory Options Group Box#
$P04SubDirGroupBox = New-Object System.Windows.Forms.GroupBox
$P04SubDirGroupBox.Location = New-Object System.Drawing.Size(10,175)
$P04SubDirGroupBox.Size = New-Object System.Drawing.Size(267,275)
$P04SubDirGroupBox.Text = "P04 Sub Directory Options"
$P04SubDirGroupBox.Font = $labelFont2
$MainForm.Controls.Add($P04SubDirGroupBox)

#   #Check All Button#
$ChkButton = New-Object System.Windows.Forms.Button
$ChkButton.Location = New-Object System.Drawing.Size (160,180)
$ChkButton.Size = New-Object System.Drawing.Size (100,40)
$ChkButton.Text = "Check All"
$ChkButton.Font = $textFont2
$ChkButton.Add_Click({
		For ($i = 0;$i -lt $SubDirListBox.Items.count;$i++) {
			$SubDirListBox.SetItemchecked($i,$True)
		}
	})
$P04SubDirGroupBox.Controls.Add($ChkButton)

#   #Un-Check All Button#
$UnChkButton = New-Object System.Windows.Forms.Button
$UnChkButton.Location = New-Object System.Drawing.Size (160,225)
$UnChkButton.Size = New-Object System.Drawing.Size (100,40)
$UnChkButton.Text = "Un-Check All"
$UnChkButton.Font = $textFont2
$UnChkButton.Add_Click({
		For ($i = 0;$i -lt $SubDirListBox.Items.count;$i++) {
			$SubDirListBox.SetItemchecked($i,$False)
		}
	})
$P04SubDirGroupBox.Controls.Add($UnChkButton)

#P04 Type Options Group Box#
$P04TypeGroupBox = New-Object System.Windows.Forms.GroupBox
$P04TypeGroupBox.Location = New-Object System.Drawing.Size(10,460)
$P04TypeGroupBox.Size = New-Object System.Drawing.Size(267,115)
$P04TypeGroupBox.Text = "P04 Type Options"
$P04TypeGroupBox.Font = $labelFont2
$MainForm.Controls.Add($P04TypeGroupBox)

#   #P04 Type Options#

#   #Check All Button#
$ChkButton2 = New-Object System.Windows.Forms.Button
$ChkButton2.Location = New-Object System.Drawing.Size (160,20)
$ChkButton2.Size = New-Object System.Drawing.Size (100,40)
$ChkButton2.Text = "Check All"
$ChkButton2.Font = $textFont2
$ChkButton2.Add_Click({
		For ($i = 0;$i -lt $TypeListBox.Items.count;$i++) {
			$TypeListBox.SetItemchecked($i,$True)
		}
	})
$P04TypeGroupBox.Controls.Add($ChkButton2)

#   #Un-Check All Button#
$UnChkButton2 = New-Object System.Windows.Forms.Button
$UnChkButton2.Location = New-Object System.Drawing.Size (160,65)
$UnChkButton2.Size = New-Object System.Drawing.Size (100,40)
$UnChkButton2.Text = "Un-Check All"
$UnChkButton2.Font = $textFont2
$UnChkButton2.Add_Click({
		For ($i = 0;$i -lt $TypeListBox.Items.count;$i++) {
			$TypeListBox.SetItemchecked($i,$False)
		}
	})
$P04TypeGroupBox.Controls.Add($UnChkButton2)

#P04 Options Group Box#
$P04GroupBox = New-Object System.Windows.Forms.GroupBox
$P04GroupBox.Location = New-Object System.Drawing.Size(10,585)
$P04GroupBox.Size = New-Object System.Drawing.Size(267,50)
$P04GroupBox.Text = "P04 Options"
$P04GroupBox.Font = $labelFont2
$MainForm.Controls.Add($P04GroupBox)

#   #P04 Options#

#   #Keep Unconfirmed Files checkbox#
$checkboxUnconfirmed = New-Object System.Windows.Forms.checkbox
$checkboxUnconfirmed.Location = New-Object System.Drawing.Size(10,20)
$checkboxUnconfirmed.Size = New-Object System.Drawing.Size(180,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxUnconfirmed.Checked = $True
$checkboxUnconfirmed.Text = "Keep Unconfirmed Files"
$checkboxUnconfirmed.Font = $labelFont2
$P04GroupBox.Controls.Add($checkboxUnconfirmed)

#FTP Files Options Group Box#
$FTPGroupBox = New-Object System.Windows.Forms.GroupBox
$FTPGroupBox.Location = New-Object System.Drawing.Size(945,175)
$FTPGroupBox.Size = New-Object System.Drawing.Size(225,150)
$FTPGroupBox.Text = "FTP Options"
$FTPGroupBox.Font = $labelFont2
$MainForm.Controls.Add($FTPGroupBox)

#	#FTP Options#

#	#Enable FTP checkbox#
$checkboxFTPEnable = New-Object System.Windows.Forms.checkbox
$checkboxFTPEnable.Location = New-Object System.Drawing.Size(10,20)
$checkboxFTPEnable.Size = New-Object System.Drawing.Size(200,20)
#This line is out of place due to the way the configuration is saved
#(it won't see the 4 spaces in front of the line)
$checkboxFTPEnable.Checked = $False
$checkboxFTPEnable.Text = "Enable FTP for processed files"
$checkboxFTPEnable.Font = $labelFont2
$FTPGroupBox.Controls.Add($checkboxFTPEnable)

#   #FTP Target Path Label#
$ftpTargetLabel = New-Object System.Windows.Forms.Label
$ftpTargetLabel.Text = "Destination Path:"
$ftpTargetLabel.Location = New-Object System.Drawing.Size(10,100)
$ftpTargetLabel.Size = New-Object System.Drawing.Size(170,15)
$ftpTargetLabel.Font = $labelFont2
$FTPGroupBox.Controls.Add($ftpTargetLabel)

#   #FTP Target Path Textbox#
$ftpTargetTextBox = New-Object System.Windows.Forms.TextBox
$ftpTargetTextBox.Text = "/Prices/P04"
#$ftpTargetTextBox.Text = "/Test/Chris_Test/1"
$ftpTargetTextBox.Location = New-Object System.Drawing.Size(10,120)
$ftpTargetTextBox.Size = New-Object System.Drawing.Size(205,30)
$ftpTargetTextBox.Font = $textFont2
$FTPGroupBox.Controls.Add($ftpTargetTextBox)

###End Group Boxes###
#
###List Boxes###

#FTP List Box#

$FTPListBox = New-Object System.Windows.Forms.CheckedListBox
$FTPListBox.Location = New-Object System.Drawing.Size(10,50)
$FTPListBox.Size = New-Object System.Drawing.Size(205,50)
$FTPListBox.Font = $listBoxFont
$FTPListBox.CheckOnClick = $true
$FTPListBox.ClearSelected()

[void] $FTPListBox.Items.Add(".Com")
[void] $FTPListBox.Items.Add(".Net")

$FTPGroupBox.Controls.Add($FTPListBox)

#Create the FTP Array#
$ftpArray = New-Object System.Collections.ArrayList

#Type List Box#

$TypeListBox = New-Object System.Windows.Forms.CheckedListBox
$TypeListBox.Location = New-Object System.Drawing.Size(10,20)
$TypeListBox.Size = New-Object System.Drawing.Size(130,100)
$TypeListBox.Font = $listBoxFont
$TypeListBox.CheckOnClick = $true
$TypeListBox.ClearSelected()

[void] $TypeListBox.Items.Add("Adjusted")
[void] $TypeListBox.Items.Add("Bond")
[void] $TypeListBox.Items.Add("Indices")
[void] $TypeListBox.Items.Add("Equities")

$P04TypeGroupBox.Controls.Add($TypeListBox)

#Create the Type Array#
$typeArray = New-Object System.Collections.ArrayList

#Country Code List Box#

$SubDirListBox = New-Object System.Windows.Forms.CheckedListBox
$SubDirListBox.Location = New-Object System.Drawing.Size(10,20)
$SubDirListBox.Size = New-Object System.Drawing.Size(130,250)
$SubDirListBox.Font = $listBoxFont
$SubDirListBox.CheckOnClick = $true
$SubDirListBox.ClearSelected()

[void] $SubDirListBox.Items.Add("AE")
[void] $SubDirListBox.Items.Add("AM")
[void] $SubDirListBox.Items.Add("AR")
[void] $SubDirListBox.Items.Add("AT")
[void] $SubDirListBox.Items.Add("AU")
[void] $SubDirListBox.Items.Add("AZ")
[void] $SubDirListBox.Items.Add("BA")
[void] $SubDirListBox.Items.Add("BB")
[void] $SubDirListBox.Items.Add("BD")
[void] $SubDirListBox.Items.Add("BE")
[void] $SubDirListBox.Items.Add("BG")
[void] $SubDirListBox.Items.Add("BH")
[void] $SubDirListBox.Items.Add("BM")
[void] $SubDirListBox.Items.Add("BO")
[void] $SubDirListBox.Items.Add("BR")
[void] $SubDirListBox.Items.Add("BS")
[void] $SubDirListBox.Items.Add("BT")
[void] $SubDirListBox.Items.Add("BW")
[void] $SubDirListBox.Items.Add("BY")
[void] $SubDirListBox.Items.Add("CA")
[void] $SubDirListBox.Items.Add("CH")
[void] $SubDirListBox.Items.Add("CI")
[void] $SubDirListBox.Items.Add("CL")
[void] $SubDirListBox.Items.Add("CM")
[void] $SubDirListBox.Items.Add("CN")
[void] $SubDirListBox.Items.Add("CO")
[void] $SubDirListBox.Items.Add("CR")
[void] $SubDirListBox.Items.Add("CV")
[void] $SubDirListBox.Items.Add("CY")
[void] $SubDirListBox.Items.Add("CZ")
[void] $SubDirListBox.Items.Add("DE")
[void] $SubDirListBox.Items.Add("DK")
[void] $SubDirListBox.Items.Add("DO")
[void] $SubDirListBox.Items.Add("DZ")
[void] $SubDirListBox.Items.Add("EC")
[void] $SubDirListBox.Items.Add("EE")
[void] $SubDirListBox.Items.Add("EG")
[void] $SubDirListBox.Items.Add("ES")
[void] $SubDirListBox.Items.Add("EU")
[void] $SubDirListBox.Items.Add("FI")
[void] $SubDirListBox.Items.Add("FJ")
[void] $SubDirListBox.Items.Add("FR")
[void] $SubDirListBox.Items.Add("GB")
[void] $SubDirListBox.Items.Add("GE")
[void] $SubDirListBox.Items.Add("GG")
[void] $SubDirListBox.Items.Add("GH")
[void] $SubDirListBox.Items.Add("GR")
[void] $SubDirListBox.Items.Add("GT")
[void] $SubDirListBox.Items.Add("GY")
[void] $SubDirListBox.Items.Add("HK")
[void] $SubDirListBox.Items.Add("HR")
[void] $SubDirListBox.Items.Add("HU")
[void] $SubDirListBox.Items.Add("ID")
[void] $SubDirListBox.Items.Add("IE")
[void] $SubDirListBox.Items.Add("IL")
[void] $SubDirListBox.Items.Add("IN")
[void] $SubDirListBox.Items.Add("IQ")
[void] $SubDirListBox.Items.Add("IR")
[void] $SubDirListBox.Items.Add("IS")
[void] $SubDirListBox.Items.Add("IT")
[void] $SubDirListBox.Items.Add("JM")
[void] $SubDirListBox.Items.Add("JO")
[void] $SubDirListBox.Items.Add("JP")
[void] $SubDirListBox.Items.Add("KE")
[void] $SubDirListBox.Items.Add("KG")
[void] $SubDirListBox.Items.Add("KH")
[void] $SubDirListBox.Items.Add("KN")
[void] $SubDirListBox.Items.Add("KR")
[void] $SubDirListBox.Items.Add("KW")
[void] $SubDirListBox.Items.Add("KY")
[void] $SubDirListBox.Items.Add("KZ")
[void] $SubDirListBox.Items.Add("LA")
[void] $SubDirListBox.Items.Add("LB")
[void] $SubDirListBox.Items.Add("LK")
[void] $SubDirListBox.Items.Add("LT")
[void] $SubDirListBox.Items.Add("LU")
[void] $SubDirListBox.Items.Add("LV")
[void] $SubDirListBox.Items.Add("LY")
[void] $SubDirListBox.Items.Add("MA")
[void] $SubDirListBox.Items.Add("MD")
[void] $SubDirListBox.Items.Add("ME")
[void] $SubDirListBox.Items.Add("MK")
[void] $SubDirListBox.Items.Add("MN")
[void] $SubDirListBox.Items.Add("MT")
[void] $SubDirListBox.Items.Add("MU")
[void] $SubDirListBox.Items.Add("MV")
[void] $SubDirListBox.Items.Add("MW")
[void] $SubDirListBox.Items.Add("MX")
[void] $SubDirListBox.Items.Add("MY")
[void] $SubDirListBox.Items.Add("MZ")
[void] $SubDirListBox.Items.Add("NA")
[void] $SubDirListBox.Items.Add("NG")
[void] $SubDirListBox.Items.Add("NL")
[void] $SubDirListBox.Items.Add("NO")
[void] $SubDirListBox.Items.Add("NP")
[void] $SubDirListBox.Items.Add("NZ")
[void] $SubDirListBox.Items.Add("OM")
[void] $SubDirListBox.Items.Add("PA")
[void] $SubDirListBox.Items.Add("PE")
[void] $SubDirListBox.Items.Add("PH")
[void] $SubDirListBox.Items.Add("PK")
[void] $SubDirListBox.Items.Add("PL")
[void] $SubDirListBox.Items.Add("PS")
[void] $SubDirListBox.Items.Add("PT")
[void] $SubDirListBox.Items.Add("PY")
[void] $SubDirListBox.Items.Add("QA")
[void] $SubDirListBox.Items.Add("RO")
[void] $SubDirListBox.Items.Add("RS")
[void] $SubDirListBox.Items.Add("RU")
[void] $SubDirListBox.Items.Add("RW")
[void] $SubDirListBox.Items.Add("SA")
[void] $SubDirListBox.Items.Add("SC")
[void] $SubDirListBox.Items.Add("SD")
[void] $SubDirListBox.Items.Add("SE")
[void] $SubDirListBox.Items.Add("SG")
[void] $SubDirListBox.Items.Add("SI")
[void] $SubDirListBox.Items.Add("SK")
[void] $SubDirListBox.Items.Add("SV")
[void] $SubDirListBox.Items.Add("SY")
[void] $SubDirListBox.Items.Add("SZ")
[void] $SubDirListBox.Items.Add("TH")
[void] $SubDirListBox.Items.Add("TN")
[void] $SubDirListBox.Items.Add("TR")
[void] $SubDirListBox.Items.Add("TT")
[void] $SubDirListBox.Items.Add("TW")
[void] $SubDirListBox.Items.Add("TZ")
[void] $SubDirListBox.Items.Add("UA")
[void] $SubDirListBox.Items.Add("UG")
[void] $SubDirListBox.Items.Add("US")
[void] $SubDirListBox.Items.Add("UY")
[void] $SubDirListBox.Items.Add("VE")
[void] $SubDirListBox.Items.Add("VN")
[void] $SubDirListBox.Items.Add("ZA")
[void] $SubDirListBox.Items.Add("ZM")
[void] $SubDirListBox.Items.Add("ZW")

$P04SubDirGroupBox.Controls.Add($SubDirListBox)

#Create the Sub Directory Array#
$subDirArray = New-Object System.Collections.ArrayList

###End List Boxes###
#
###Buttons###

#Show Robocopy Help Button#
$HelpButton = New-Object System.Windows.Forms.Button
$HelpButton.Location = New-Object System.Drawing.Size(8,135)
$HelpButton.Size = New-Object System.Drawing.Size(270,30)
$HelpButton.Text = "Show Robocopy Help"
$HelpButton.Font = $textFont2
$HelpButton.Add_Click({robocopyhelp})
$MainForm.Controls.Add($HelpButton)

#Save Robocopy Options Button#
$SaveButton = New-Object System.Windows.Forms.Button
$SaveButton.Location = New-Object System.Drawing.Size(945,452)
$SaveButton.Size = New-Object System.Drawing.Size(225,30)
$SaveButton.Text = "Save Options"
$SaveButton.Font = $textFont2
$SaveButton.Add_Click({saveoptions})
$MainForm.Controls.Add($SaveButton) 

#Open Log Button#
$OpenLogButton = New-Object System.Windows.Forms.Button
$OpenLogButton.Location = New-Object System.Drawing.Size(945,662)
$OpenLogButton.Size = New-Object System.Drawing.Size(225,30)
$OpenLogButton.Text = "Open Logfile"
$OpenLogButton.Font = $textFont2
$OpenLogButton.Add_Click({openlog})
$MainForm.Controls.Add($OpenLogButton)

#Show Errors Button#
$ErrorsButton = New-Object System.Windows.Forms.Button
$ErrorsButton.Location = New-Object System.Drawing.Size(945,622)
$ErrorsButton.Size = New-Object System.Drawing.Size(225,30)
$ErrorsButton.Text = "Show Errors"
$ErrorsButton.Font = $textFont2
$ErrorsButton.Add_Click({showerrors})
$MainForm.Controls.Add($ErrorsButton)

#Stop Robocopy Button#
$StopButton = New-Object System.Windows.Forms.Button
$StopButton.Location = New-Object System.Drawing.Size(945,582)
$StopButton.Size = New-Object System.Drawing.Size(225,30)
$StopButton.Text = "Stop Robocopy"
$StopButton.Font = $textFont2
$StopButton.Add_Click({stoprobocopy})
$MainForm.Controls.Add($StopButton)

#Start Robocopy Button#
$StartButton = New-Object System.Windows.Forms.Button
$StartButton.Location = New-Object System.Drawing.Size(945,492)
$StartButton.Size = New-Object System.Drawing.Size(225,80)
$StartButton.Text = "START"
$StartButton.Font = $textFont1
$StartButton.Add_Click({robocopy})
$MainForm.Controls.Add($StartButton)

###End Buttons###
#
###Output Box###

#Output Box#
$outputBox = New-Object System.Windows.Forms.RichTextBox
$outputBox.Location = New-Object System.Drawing.Size(287,335)
$outputBox.Size = New-Object System.Drawing.Size(648,316)
$outputBox.ReadOnly = $True
$outputBox.BackColor = '#FFFFFF' #White
$outputBox.MultiLine = $True
$outputBox.ScrollBars = "Both"
$outputBox.Font = $textFont2
$MainForm.Controls.Add($outputBox)

###End Output Box###
#
###Progress Bars###

#Copy Progress Bar#
$CopyProgressBar = New-Object System.Windows.Forms.ProgressBar
$CopyProgressBar.Name = 'CopyProgressBar'
$CopyProgressBar.Value = 0
$CopyProgressBar.Style = "Continuous"
$CopyProgressBar.Location = New-Object System.Drawing.Size(287,663)
$CopyProgressBar.Size = New-Object System.Drawing.Size(648,14)
#initialize a counter
$copyProgressi = 0
$MainForm.Controls.Add($CopyProgressBar)

#FTP Progress Bar#
$ftpProgressBar = New-Object System.Windows.Forms.ProgressBar
$ftpProgressBar.Name = 'FTPProgressBar'
$ftpProgressBar.Value = 0
$ftpProgressBar.Style = "Continuous"
$ftpProgressBar.Location = New-Object System.Drawing.Size(287,677)
$ftpProgressBar.Size = New-Object System.Drawing.Size(648,14)
#initialize a counter
$ftpProgressi = 0
$MainForm.Controls.Add($ftpProgressBar)

###End Progress Bars###
#
###Temporary Table###

#Creating the Table#
$tableName = “TempTable”
$table = New-Object system.Data.DataTable “$tableName”

#Setting the Start Row#
$startRow = 1

#Setting the Columns#
$colCountryCode = New-Object system.Data.DataColumn CountryCode,([string])
$colOption = New-Object system.Data.DataColumn Option,([string])
$colFileDate = New-Object system.Data.DataColumn FileDate,([string])
$colFileExt = New-Object system.Data.DataColumn FileExt,([string])
$colOptionFolder = New-Object system.Data.DataColumn OptionFolder,([string])

#Setting the Column Numbers#
$colNoCountryCode = 1
$colNoOption = 2
$colNoFileDate = 3
$colNoFileExt = 4
$colNoOptionFolder = 5

#Adding the Columns#
$table.columns.add($colCountryCode)
$table.columns.add($colOption)
$table.columns.add($colFileDate)
$table.columns.add($colFileExt)
$table.columns.add($colOptionFolder)

#Setting Table Counter#
$tablei = 0

###End Temporary Table###
#
###The###

$MainForm.Add_Shown({$MainForm.Activate()})
[void] $MainForm.ShowDialog()

###End###