﻿#* FileName: SLA_check.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose:  Periodically check DB for feed SLA/ETA state
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [28/08/2013]
#* Time: [12:00]
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Determine which incrment to apply
#
#
# =============================================================================

### Logs step and output of entire script
#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                #'t15022_inc' 
                'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 12 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                        #}
                            }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        #$hold=@()
        #$global:thestr = $args[0]
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output "$d $displaytxt" #;exit
        #$hold.count
        $global:logvalue
        #if ($args[1] -eq "exit") {exit}
    }

#################################################################
### Set Connection to MySQL DB and execute query ###

## Set MySQL Connectiion
[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection

#------------------------- DB connections -------------------------#
## Default
#$myconnection.Connectionstring="server=$server;database=$db;Persist Security Info=false;user id=$id;pwd=$pwd"
## OTRS
$myconnection.Connectionstring = "server=192.168.12.202;database=test;Persist Security Info=false;user id=otrs;pwd=hot"
## Diesel
$myconnection.Connectionstring="server=$server;database=$db;Persist Security Info=false;user id=$id;pwd=$pwd"
## LiquidWeb
$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"
## iCLOUD
$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"


$myconnection.open()

#Set query command
$Command= New-Object MySql.Data.MySqlClient.MySqlCommand
$Command.Connection=$myconnection

#$NList='O:\AUTO\Scripts\sql\FTP\CompareFTP_NET.sql'
#$CList='O:\AUTO\Scripts\sql\FTP\CompareFTP_COM.sql'

#Queries
$show_all= "SHOW TABLES"
$tclinet="select * from tclient"
$tfeed="select * from tfeed";
$tservices="select * from tservices";

#Execute Command
$command.CommandText = $NList
$reader=$Command.ExecuteReader()

$dataset = New-Object System.Data.DataSet
Write-Output $reader.GetValue(0).ToString()

while($myreader.Read()){$myreader.GetString(0)}

while
($reader.Read()){
    for ($i=0; $i -lt $reader.FieldCount; $i++ {
        Write-Output $reader.GetValue($i).ToString()
    }
}

while 
($reader.Read()) {
    for ($i=0; $i -lt $reader.FieldCount; $i++) {
            write-output $reader.GetValue($i).ToString()
    }
}

$myconnection.Close()
