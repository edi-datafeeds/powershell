﻿#* FileName: SLA_check.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose:  Periodically check DB for feed SLA/ETA state
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [28/08/2013]
#* Time: [12:00]
#* Issue: 
#* Solution: 
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment,set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Determine which incrment to apply
#
#
# =============================================================================

### Logs step and output of entire script
#Start-Transcript \\192.168.12.4\ops\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

Param(
  [Parameter (
  Mandatory = $false,
  ParameterSetName = '',
  ValueFromPipeline = $true)]
  [string]$Query
  )

  $env:psmodulepath=$env:psmodulepath +=";\\192.168.12.4\ops\AUTO\Scripts\Powershell\PSProfile\Modules"
  Import-Module "\\192.168.12.4\ops\Auto\Scripts\Powershell\PSProfile\Modules\PSFTP"

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                #'t15022_inc' 
                'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 12 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                        #}
                            }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="\\192.168.12.4\ops\auto\logs\$logfile$inc" ;write-host inc exists!;
                            }
                       default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="\\192.168.12.4\ops\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        #$hold=@()
        #$global:thestr = $args[0]
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output "$d $displaytxt" #;exit
        #$hold.count
        $global:logvalue
        #if ($args[1] -eq "exit") {exit}
    }

#------------------------- Set NET&COM FTP Credentials -------------------------#

#$COM_cred=Get-Credential
#$COM_cred.Password | ConvertFrom-SecureString -Key (1..24) | sc '\\192.168.12.4\ops\AUTO\Scripts\Powershell\PSProfile\Passwords\COM_FTP.txt'

#$NET_cred=Get-Credential
#$NET_cred.Password | ConvertFrom-SecureString -Key (1..24) | sc '\\192.168.12.4\ops\AUTO\Scripts\Powershell\PSProfile\Passwords\NET_FTP.txt'

#------------------------- Get NET&COM FTP Credentials -------------------------#

$NET_pwd=gc '\\192.168.12.4\ops\AUTO\Scripts\Powershell\PSProfile\Passwords\NET_FTP.txt' | ConvertTo-SecureString -Key (1..24)
$NET_cred=New-Object System.Management.Automation.PsCredential “channel7“,$NET_pwd

$COM_pwd=gc '\\192.168.12.4\ops\AUTO\Scripts\Powershell\PSProfile\Passwords\COM_FTP.txt' | ConvertTo-SecureString -key (1..24)
$COM_cred=New-Object System.Management.Automation.PsCredential “ops_ftp“,$COM_pwd

Set-FTPConnection -Credentials $NET_cred -Server ftp://www.exchange-data.net -session NET_session -UsePassive | Out-Null
$Session=Get-FTPConnection -Session $NET_Session

#Set-FTPConnection -Credentials $COM_cred -Server ftp://www.exchange-data.net -session COM_session -UsePassive 
#$Session=Get-FTPConnection -Session $COM_Session

#------------------------- Static Variables -------------------------#

$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'

$global:logvalue=@()

#------------------------- Upload FTP Function -------------------------#
    function uploadftp()
    {
        $thisfile = $args[0]
        if (test-path "$global:logdir\resultftp.txt") { remove-item "$global:logdir\resultftp.txt"}
        else {}
        #set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
        set-alias cftp "c:\Program Files\coreftp\corecmd.exe"
        cFtp -OA -B -u  "$thisfile" "sftp://cds_trans:496h5wFs@109.235.145.77/Test" -log "$global:logdir\resultftp.txt"
        #Select-String -Path $global:logdir\resultftp.txt -pattern "Total uploaded files:  1" | tee -Variable global:uploadx
}

#------------------------- Download FTP Function -------------------------#
    function downloadftp() {
        $global:thisfile = $args[0]
            if (test-path "$global:logdir\resultftp.txt") { remove-item "$global:logdir\resultftp.txt"}
            else {}
            #set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
            set-alias cftp "c:\Program Files\coreftp\corecmd.exe"
            cFtp -s -d "sftp://edi-cds:3Qhh243n@exchange-data.com/$thisfile" -p '\\192.168.12.4\ops\Datafeed\CDS' -log "$global:logdir\resultftp.txt"
            #Select-String -Path "$global:logdir\resultftp.txt" -pattern "Total downloaded files:  1" | tee -Variable global:x
    }

#################################################################
### Set Connection to MySQL DB and execute query ###

## Set MySQL Connectiion
#[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
#$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection

#$server='192.168.12.160'
#$db='OpsSLA'
#$id='sa'
#$pwd='K376:lcnb'

#------------------------- DB connections -------------------------#

## Default
#$myconnection.Connectionstring="server=$server;database=$db;Persist Security Info=false;user id=$id;pwd=$pwd"

## OTRS
#$myconnection.Connectionstring = "server=192.168.12.202;database=test;Persist Security Info=false;user id=otrs;pwd=hot"
## Diesel
#$myconnection.Connectionstring="server=$server;database=$db;Persist Security Info=false;user id=$id;pwd=$pwd"
## LiquidWeb
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"
## iCLOUD
#$myconnection.Connectionstring="server=67.227.167.146;database=;Persist Security Info=false;user id=auto_ops;pwd=AK47ops"

#$myconnection.open()

#Set query command
#$Command= New-Object MySql.Data.MySqlClient.MySqlCommand
#$Command.Connection=$myconnection

#------------------------- Queries -------------------------#

$show_all= "SHOW TABLES"
$feeds='select Remotepath,Prefix,FileName,Suffix,Extension from Feeds where Feeds.ID = 1'
#$feeds='where ETA.FID = 1 select RemotePath from Feeds'
$eta='select Target,FID from ETA'
#$eta='select * from ETA'
$join='select ETA.Target,ETA.FID,Feeds.RemotePath,Feeds.Prefix,Feeds.Filename,Feeds.Suffix,Feeds.Extension from ETA LEFT OUTER JOIN Feeds ON ETA.FID=Feeds.ID'
#$join='select * from ETA LEFT OUTER JOIN Feeds ON ETA.FID=Feeds.ID'
#$join='select * from ETA where ETA.FID=Feeds.ID'
$contacts='select * from Contacts'
$target='select Target,FID from ETA '
$remote='select Feeds.Remote'
#$feeds='select Remotepath,FileName from Feeds'
#$feeds='select Remotepath,(Prefix IS NOT NULL) AS Prefix,FileName,(Suffix IS NOT NULL) AS Suffix,Extension from Feeds where Feeds.ID = 1'

#------------------------- Diesel -------------------------#
#$server='192.168.12.160'
#$db='OpsSLA'
#$id='sa'
#$pwd='K376:lcnb'

#------------------------- MyDev -------------------------#
$server='192.168.12.109'
$db='opssla'
$id='sa'
$pwd='K376:lcnb'

$ConnectionString = "server=" + $server + ";port=3306;uid=" + $id + ";pwd=" + $pwd + ";database="+$db

Try {
  [void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data")
  $Connection = New-Object MySql.Data.MySqlClient.MySqlConnection
  $Connection.ConnectionString = $ConnectionString
  $Connection.Open()

  $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($join,$Connection)
  $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command)
  $DataSet = New-Object System.Data.DataSet
  $RecordCount = $dataAdapter.Fill($dataSet, "data")
  $DataSet.Tables[0] | Out-Null
  }

Catch {
  Write-Host "ERROR : Unable to run query : $query `n$Error[0]"
 }

Finally {
  $Connection.Close()
  }

#$DataSet.Tables[0].Target[0] | ?{$_.Hours -eq (get-date).addHours(-2).Hour
    #} | ?{$_.Minutes -eq (Get-Date).AddMinutes(5) -or $_.Minutes -eq (Get-Date).AddMinutes(-5)}

## Check SLT
#switch ($DataSet.Tables[0].Target[0]){
#{$_.Hours -eq (get-date).addHours(-4).Hour -and $_.Minutes -eq (Get-Date).AddMinutes(-37).Minute}{write-host 'Bingo!'}
#default {write-host 'dead wrong'}
#}

#switch ($DataSet.Tables[0]){
#{$_.Target.Hours -eq (get-date).addHours(-4).Hour -and $_.Target[0].Minutes -eq (Get-Date).AddMinutes(-37).Minute}{%{$_.RemotePath}}
#default {write-host 'dead wrong'}
#}

#------------------------- Test for Success -------------------------#
#Do {
    (get-date).ToShortTimeString() | Out-Null
        switch ($DataSet.Tables[0]){
        {$_.Target.Hours -eq (get-date).addHours(-0).Hour -and $_.Target.Minutes -gt ((get-date).AddMinutes(-29).Minute) -le ((get-date).AddMinutes(-05).Minute)}{
            'Match Found!' | Out-Null;
                $_.RemotePath+$_.Prefix+$_.FileName+$_.Suffix+'.'+$_.Extension |
                        %{$_ -replace 'YYYYMMDD',$jdate | tee -variable lookup| Out-Null; break} #tee -Variable val
                            }
        #default {write-host 'dead wrong'}
        }

#} Until ((get-date).Minute -eq '56' )#-and (get-date).Minute -eq '30' )

#------------------------- List FTP items -------------------------#
#$listFTP=Get-FTPChildItem -Session $NET_Session -Path $lookup #| ?{$_.Dir -notlike ('d')}

#switch ($x){
#{$listFTP -eq $null}{write-host "$lookup FTP failed!"}
#default{(split-path -leaf "$lookup")+' Upload successfull!'}
#}

#------------------------- Test for failure -------------------------#
#Do {
    #(get-date).ToShortTimeString()
        #switch ($DataSet.Tables[0]){
        #{$_.Target.Hours -eq (get-date).addHours(-4).Hour -and $_.Target.Minutes -gt ((get-date).AddMinutes(-5).Minute) -le ((get-date).Minute)}{
            #'Match Found!';
                #$_.RemotePath+$_.Prefix+$_.FileName+$_.Suffix+'.'+$_.Extension |
                        #%{$_ -replace 'YYYYMMDD',$jdate | tee -variable lookup; break} #tee -Variable val
                            #}
        #default {write-host 'dead wrong'}
        #}

#} Until ((get-date).hour -eq '21' -and (get-date).Minute -eq '30' )

#------------------------- Test -------------------------#
##
#$DataSet.Tables[0] | ?{$_.Target.Hours -eq (get-date).addHours(-2).Hour} | %{$_.RemotePath}
#$DataSet.Tables[0] | ?{$_.Target.Hours -eq '12'} | select -Property RemotePath


#------------------------- List FTP items -------------------------#
$listFTP=Get-FTPChildItem -Session $NET_Session -Path (split-path -Parent "$lookup") #| ?{$_.Dir -notlike ('d')}

switch ($x){
    {$listFTP -eq $null}{
        write-host "$lookup FTP upload failed!";exit 2}
    default{
        (split-path -leaf "$lookup")+' FTP Upload successfull!';exit 0}
        }