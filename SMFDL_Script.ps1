﻿$username = "lsestaging\sedolprod_edi"
$password = "sEp3t3mB3r"
$savePath = "\\192.168.2.163\ops\Datafeed\SMF\smffeed"

$links = @()

$date = Get-Date -Format "yyyyMMdd"
$day = (Get-Date).DayOfWeek

if($day -eq "Monday")
{
	$dateFrom = (Get-Date).AddDays(-3).ToString("yyyyMMdd")
}
else
{
	$dateFrom = (Get-Date).AddDays(-1).ToString("yyyyMMdd")
}

$timeFrom = Get-Date -Format "HH"
$timeFrom += "0000"

$url = ""
$urlIF = "/data-x/SEDOL/Changes/Module%201/Daily/" + $date + "/" + $dateFrom + "180000_" + $date + $timeFrom + "_IDC_1_TAB.zip"
$urlPage = "https://data-x.londonstockexchange.com/data-x/SEDOL/Changes/Module%201/Daily/" + $date + "/"

$pair = "$($username):$($password)"
$bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
$base64 = [System.Convert]::ToBase64String($bytes)
$basicAuthValue = "Basic $base64"
$headers = @{ Authorization = $basicAuthValue }

$counter = 0
$whileX = 0

$wshell = New-Object -ComObject Wscript.Shell

Do
{
	try
	{
		$holder = 0
		$linksRef = (Invoke-WebRequest -Uri $urlPage -Headers $Headers).Links.Href
		$links = $linksRef.Split(" ")
		foreach($i in $links)
		{
			if($i -eq $urlIF)
			{
				$removeZip = $savePath + "\*.zip"
				Remove-Item $removeZip
				
				$url = "https://data-x.londonstockexchange.com$i"
				$outfile = $savePath + "\" + $dateFrom + "180000_" + $date + $timeFrom + "_IDC_1_TAB.zip"
				$whileX = 1
			}
			try
			{
				Invoke-WebRequest $url -Headers $Headers -OutFile $outfile
				$holder = 2
			}
			catch
			{
				if($holder -ne 2)
				{
					$holder = 1
				}
			}
		}
		if($holder -ne 2)
		{
			Invoke-WebRequest $url -Headers $Headers -OutFile $outfile
		}
	}
	catch
	{
		if($holder -ne 2)
		{
			$counter++
			$errorRetry = $wshell.Popup("Today's SMF file was not downloaded, will retry in 2 minuites (Default)",20,"Error",0x12)
			switch($errorRetry)
			{
				3 {$counter = 5}#Abort
				4 {Start-Sleep 100}#Retry
				5 {Start-Sleep 100}#Ignore
				default {Start-Sleep 100}#Canceled
			}
			if($counter -ge 5)
			{
				$errorExit = $wshell.Popup("Today's $timeFrom SMF file could not be downloaded",0,"Error",0x10)
				switch($errorExit)
				{
					1 {$whileX = 1}#OK
					default {$whileX = 1}#Canceled
				}
			}
		}		
	}
}While($whileX -eq 0)