﻿## Script to determine if WCA loader has run on schedule

##Test
#$Tcommandline=Get-WmiObject win32_process -Filter "Name like '%powershell%'" | Select-Object CommandLine

#check wether cscript/loader.vbs is running
$commandline=Get-WmiObject win32_process -Filter "Name like '%wscript%'" | Select-Object CommandLine

if ($commandline -eq $null)
{
write-host 'Loader has failed to start'; exit 1 #exit $LASTEXITCODE
}
elseif ($commandline.CommandLine.Contains('WCA a MyDev WcaWebload MyDev i'))
{
write-host 'WCA Loader Successfully Initiated!'; exit $LASTEXITCODE
}
else
{
Write-Host 'Loader has failed to start'; exit 2
}


#if ($commandline.CommandLine.Contains('WCA a MyDev WcaWebload MyDev i'))
#{
#write-host 'WCA Loader Successfully Initiated!'; exit $LASTEXITCODE
#}
#else
#{
#Write-Host 'Loader has failed to start'; exit 2
#}
#Write-Host $LASTEXITCODE
#exit $LASTEXITCODE

#if ($commandline -contains 'WCA a MyDev WcaWebload MyDev i')
#{
#write-host 'Loader Initiated'; exit $LASTEXITCODE
#}
#else
#{
#Write-Host 'Loader has failed to start'; exit 2
#}
#Write-Host $LASTEXITCODE
#exit $LASTEXITCODE