' 01/03/2013 - DJ - 2013022710000064 
' Script to fetch and output the name of PC current task file is run on

Option explicit

dim i,objShell, src, dest,numArgs

src = WScript.Arguments.Item(0)
dest = WScript.Arguments.Item(1)

Set objShell = CreateObject("Wscript.Shell")


i=objShell.Run("C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe O:\AUTO\Scripts\Powershell\Get_PCname.ps1 " & src & " " & dest,1,true)

if i = 1 then
	msgbox "Workstation listing failed"
else
	wscript.quit(0)
end if 
