﻿$OFS='_'

#Working output dir 
$ndump=$args[0]
#$ndump='damartest35'
$cat=$args[1]
#$cat="i"
$logpath="O:\auto\logs\$ndump"
$time=(get-date).hour
#$exist=Test-Path -Path $logpath

#Set output path
IF (!(Test-Path -Path $logpath))
{
    mkdir $logpath
}
Else
{
}    

#Go to logpath 
cd $logpath

#Test output dir
#$dest="C:\Users\d.johnson\Desktop\dest\name.txt"

#Get PC name
$SID=$(Get-WmiObject Win32_Computersystem).name

#Todays Date & Time
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format g

#$File | Rename-Item -NewName {$_.name + '.html'}
#$File=$File + '.html'
$Fname=$jdate,$ndump


#$PCname >> $File

#ni -Name 'dude.txt' -itemtype "file" -Value "<p>spanclassnote<$PCname></p>"

#determine if inc
#IF ($cat= "i")
#{
#    $Fname="$jdate,$ndump,$inc"
#}
#Else
#{
#    $Fname="$jdate,$ndump"
#}
    

IF (!(Test-Path $logpath -include $Fname))
{
ni -Name "$Fname.html" -itemtype "file" -Value "<p><span class='note'>Workstation: $SID</span></p>"
}
Else
{
Add-Content -Path "$Fname.html" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>" -Force;
}


## Add incremental value based on time
IF ($cat -eq "i" -and $time -ge 12 -and $time -le 14)
{
    $inc=2
}
Elseif ($cat -eq "i" -and $time -ge 17 -and $time -le 23)
{
    $inc=3
}
Elseif ($cat -eq "i" -and $time -gt 23 -and $time -lt 12) 
{
    $inc=1
}
Else
{
    $inc=0
}