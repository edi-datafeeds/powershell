' 01/03/2013 - DJ - 2013022710000064 
' Script to fetch and output the name of PC current task file is run on

Option explicit

dim i,objShell, logfile, inc,numArgs

logfile = WScript.Arguments.Item(0)
inc = WScript.Arguments.Item(1)

if inc = 0 then
	createobject("wscript.shell").popup "Workstation listing failed!", 3, "ID Workstation", 64
	'msgbox "Workstation listing failed"
else
	
	Set objShell = CreateObject("Wscript.Shell")

	i=objShell.Run("C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -noexit O:\AUTO\Scripts\Powershell\IDworkstation.ps1 " & logfile & " " & inc,1,true)

	if i = 1 then
		msgbox "Workstation listing failed"
	else
		wscript.quit(0)
	end if 

end if 
