######################## OP's File Handler ########################
#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\File_Handler.log -Append

######## All-in-one script to for processing of file types ########

###== Example ==######################################################################################

# To Copy file;		O:\AUTO\Scripts\Powershell\File_Handler.ps1	*clone 		C:\PS\Test C:\PS\Test2 tsk

# To Move file;		O:\AUTO\Scripts\Powershell\File_Handler.ps1	*shift 		C:\PS\Test C:\PS\Test2 tsk

# To Delete file;	O:\AUTO\Scripts\Powershell\File_Handler.ps1	*tidy 		C:\PS\Test tsk

# To zip file;		O:\AUTO\Scripts\Powershell\File_Handler.ps1	*archive 	C:\PS\Test C:\PS\Test2 tsk
######################################################################################################

$action=$args[0]
$source=$args[1]
$dest=$args[2]
$type=$args[3]
#$archive=$arg[4]
#$compress=$arg[5]

## Test source,destination,file type
#$action='clone'
#$source='C:\PS\Test\'
#$dest='C:\Users\d.johnson\Desktop\'
#$type='txt'


Function OPS-Copy {
cd $source
cpi (gci $source -Filter "*.$type") $dest -Force
}

Function OPS-Move {
cd $source
mi (gci $source -Filter "*.$type") $dest -Force
}

Function OPS-Tidy {
cd $source
rm (gci $source -Filter "*.$type") -Force
}

Function OPS-Zip ($type='zip'){
Set-Alias 7zip "$env:ProgramFiles\7-Zip\7z.exe"
#7zip a "-t$compress" $archive $source
7zip a "-t$type" $archive,$source #$source,$dest
}

switch ($action) {
clone {Write-Host Copying File; OPS-Copy; break}
shift {Write-Host Moving File; OPS-Move; break}
tidy {Write-Host Deleting File; OPS-Tidy; break}
archive {write-host Compressing File;$archive=$args[1];$source=$args[2]; OPS-Zip;break}
}

#Stop-Transcript
