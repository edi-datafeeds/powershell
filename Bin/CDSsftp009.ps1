﻿#* FileName: CDSsftp.ps1
#*=============================================================================
#* Script Name: [CDS SFTP]
#* Created: [25/06/2013]
#* Author: Seb Sharr, logging - Damar Johnson
#* Company: Exchange Data International
#* Email: s.sharr@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: Listen for CDS file from and upload to .COM
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [03/07/2013]
#* Time: [12:00]
#* Issue: Original logging set by Seb from file server, need OP's solution
#* Solution: Ammend logging to point to OP's log centre for Fixed Income
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: LOGFILE, LOGTHIS, THISFTP, UPLOADFTP, DOWNLOADFTP, PROCESSTHISFILE
# Created: [....]
# Author: Seb Sharr
# Arguments:
# =============================================================================
# Purpose: 
#
#* Set OPslog centre logging
#* Create FTP connection
#* Upload File
#* Download File
#* Copy or move file to correct path
# =============================================================================

#$ndump=$args[0]
#$tail=$args[1]

$ndump='damartest56'
$tail=''

$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
$global:logvalue="$wdate | $global:thestr"
#$global:basedir='O:\AUTO\Tasks\Fixed_Income\CDS'
$global:basedir='C:\cds'

switch ($LASTEXITCODE) {
'0' {$class='ok'}
'1' {$class='failed'}
}

#$Start=(Get-Date)
#$finish=$Start.AddMinutes(10)
#$class

### determine INC and create logpath ###
Function Get-Increment {
#$time=(get-date).hour
### Webload incrementals
       switch ($ndump) {
       
                ### SMF incrementals values
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                ### SMF incrementals values
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                ### 123Trans incrementals values
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                ### CABTrans incrementals values
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                ### Default incremental values
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($tail) {
                       'i' {$global:Fname="$jdate$sep$ndump$inc";$global:logpath="O:\auto\logs\$ndump$inc" ;write-host inc exists!;
                                }
                       '' {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$ndump";$global:logpath="O:\auto\logs\$ndump"}
                       }
}

############################## Determine/Set file log path ##############################        
#Function Set-Logpath {
### Determine file log path
#        IF (!(Test-Path -Path $logpath))
#            { write-host No logpath found! Creating Directory; mkdir $logpath }
#        Else
#            {Write-Host Current logpath exists}    
#                ##Create file or append if exist   
#                IF (!(Test-Path "$logpath\$Fname.html"))
#                { write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$class'>$logvalue</span></p>"}
#               Else
#                { write-host Appending to file!; ac -Path "$logpath\$Fname.html" -Value "<p><span class='$class'>$logvalue</span></p>" -Force}
#}


    function LogThis()
    {
        $global:thestr = $args[0]
        $d = (Get-Date).tostring( "yyyy-MM-dd HH:mm:ss" )
        "$wdate | $thestr" | tee -Variable global:logvalue #$global:basedir\LogCdsPs.txt
        write-output "$d $thestr"
        if ($args[1] -eq "exit") {exit}
        #(Get-Date).tostring( "yyyyMMdd" )
        #(Get-Date).AddDays(-1).tostring( "yyyyMMdd" )  #yesteday date
    }
    
    function thisftp()
    {
        $thisfile = $args[0]
        LogThis "$thisfile.gz FTP started"
        if (test-path "$global:basedir\resultftp.log") { remove-item  $global:basedir\resultftp.log }
        set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
        $a = cFtp -OA -B -u  "$thisfile" "sftp://cds_trans:496h5wFs@109.235.145.77" -log $global:basedir\resultftp.log
        $x = Select-String -Path $global:basedir\resultftp.log -pattern "Total uploaded files:  1"
        if ( $x -eq $null ) 
        {
            LogThis "ERROR $thisfile SFTP unsuccessful"
            exit 0
        }
        LogThis "$thisfile SFTP successful"
    }
    function uploadftp()
    {
        $thisfile = $args[0]
        LogThis "$thisfile Upload FTP started"
        if (test-path "$global:basedir\resultftp.log") { remove-item  $global:basedir\resultftp.log }
        set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
        $a = cFtp -OA -B -u  "$thisfile" "sftp://cds_trans:496h5wFs@109.235.145.77" -log $global:basedir\resultftp.log
        $x = Select-String -Path $global:basedir\resultftp.log -pattern "Total uploaded files:  1"
        if ( $x -eq $null ) 
        {
            LogThis "ERROR $thisfile SFTP upload unsuccessful"
            #exit 1
        }
        LogThis "$thisfile SFTP Upload successful"
    }
    function downloadftp() {
        $global:thisfile = $args[0]
            LogThis "$thisfile Download FTP started"
            if (test-path "$global:basedir\resultftp.log") { remove-item  $global:basedir\resultftp.log }
            set-alias cftp "$env:ProgramFiles\coreftp\corecmd.exe"
            #cFtp -s -d "sftp://edi-cds:3Qhh243n@exchange-data.com/SM7031F_130522.gz" -p F:\cds -log $global:basedir\resultftp.log
            #cFtp -s -d "sftp://edi-cds:3Qhh243n@exchange-data.com/EV7041F_130522.gz" -p F:\cds -log $global:basedir\resultftp.log
            cFtp -s -d "sftp://edi-cds:3Qhh243n@exchange-data.com/tmp/$thisfile" -p C:\cds -log $global:basedir\resultftp.log | tee -Variable j
            Select-String -Path $global:basedir\resultftp.log -pattern "Total downloaded files:  1" | tee -Variable global:x
            switch ($x) {
            $null {LogThis "ERROR $thisfile SFTP download unsuccessful";}            
            (!($null)) {LogThis "$thisfile SFTP Download successful";}
            }
            
            #if ( $x -eq $null ) 
            #{
            #LogThis "ERROR $thisfile SFTP download unsuccessful"
            #exit 1
            #}
            #LogThis "$thisfile SFTP Download successful"
    }

    function processthisfile(){
        $thisf = $args[0]
        Copy-Item d:\inetpub\ftproot\CDS\$thisf d:\inetpub\ftproot\custom\bahar\cds
        if (-not( $? )) {logthis "ERROR $thisf file not copied to \inetpub\ftproot\custom\bahar\cds" "exit"}  
        thisftp "d:\inetpub\ftproot\CDS\$thisf" 
        move-Item d:\inetpub\ftproot\CDS\$thisf $global:basedir\archive
        if (-not( $? )) {logthis "ERROR $thisf file not moved to $global:basedir\archive" "exit"}
        logthis "$thisf processed"
    } 
# ******************** Main ***************************************************************
$yd = (Get-Date).AddDays(-1).tostring( "yyMMdd" )

downloadftp (("EV7041F_" + $yd + ".gz") | tee -variable downval)
$downres=(Test-Path "'$global:basedir\'$downval")
switch ($x) {
{Test-Path "$global:basedir\$thisfile"}  {'EV7041F file Successfully Downloaded'}
{(!(Test-Path "$global:basedir\$thisfile"))} {'Download Fail';Do {downloadftp ("EV7041F_" + $yd + ".gz"); (Write-Host 'Retrying download: Attempt:'($count++) | tee -Variable downlog); start-sleep -Seconds 05 } until($downres -eq 'true')}
}

downloadftp ("SM7031F_" + $yd + ".gz")
$downres=(Test-Path "'$global:basedir\'$downval")
switch ($x) {
{Test-Path "$global:basedir\$thisfile"}  {'EV7041F Success'}
{(!(Test-Path "$global:basedir\$thisfile"))} {'Download Fail';Do {downloadftp ("EV7041F_" + $yd + ".gz"); write-host 'Retrying download: Attempt:'($count++) | tee -Variable downlog; logthis $downloadlog; start-sleep -Seconds 05 } until($downres -eq 'true')}
}
uploadftp ("f:\cds\EV7041F_" + $yd + ".gz")
uploadftp ("f:\cds\SM7031F_" + $yd + ".gz")

$yyyyd = (Get-Date).AddDays(-1).tostring( "yyyyMMdd" )

#downloadftp ("SBCR_" + $yyyyd + ".TXT")
#switch ($?) {
#{$? -eq 'true'} {'Success SB';}
#(!($?)) {Do {downloadftp ("SBCR_" + $yyyyd + ".TXT")}until($?)}
#}
#downloadftp ("SBUR_" + $yyyyd + ".TXT")
#switch (downloadftp) {
#{$? -eq 'true'} {;}
#(!($?)) {Do {downloadftp ("SBCR_" + $yyyyd + ".TXT")}until($?)}
#}
#uploadftp ("f:\cds\SBCR_" + $yyyyd + ".TXT")
#uploadftp ("f:\cds\SBUR_" + $yyyyd + ".TXT")

#exit

$global:logvalue="$wdate | $global:thestr"

Function Set-Logpath {
### Determine file log path
        IF (!(Test-Path -Path $logpath))
            { write-host No logpath found! Creating Directory; mkdir $logpath }
        Else
            {Write-Host Current logpath exists}    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                { write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$class'>$logvalue</span></p>"}
                Else
                { write-host Appending to file!; ac -Path "$logpath\$Fname.html" -Value "<p><span class='$class'>$logvalue</span></p>" -Force}
}

#OP's logging
Get-Increment
Set-Logpath

#none of this gets done
    logthis "-------------------------------- Start CDS processing"
    if ( test-path -Path "d:\inetpub\ftproot\CDS\EV*.gz" ) { 
        get-childitem d:\inetpub\ftproot\CDS\EV*.gz -name | foreach-object { processthisfile $_ }
    }
    if (test-path -Path "d:\inetpub\ftproot\CDS\SM*.gz") { 
        get-childitem d:\inetpub\ftproot\CDS\SM*.gz -name | foreach-object { processthisfile $_ }
    }
    if ( test-path -Path "d:\inetpub\ftproot\CDS\SBCR*.TXT.gz" ) { 
        get-childitem d:\inetpub\ftproot\CDS\SBCR*.TXT.gz -name | foreach-object { processthisfile $_ }
    }
    if ( test-path -Path "d:\inetpub\ftproot\CDS\SBUR*.TXT.gz" ) {  
        get-childitem d:\inetpub\ftproot\CDS\SBUR*.TXT.gz -name | foreach-object { processthisfile $_ }
    }
    logthis "-------------------------------- Finished CDS processing"
exit   

#$arr = New-Object string[] 4
#$arr = "EV*.gz", "SM*.gz", "SBCR*.TXT.gz", "SBUR*.TXT.gz" 
#    For ($i=0; $i -le 4; $i++) {
#        if ( test-path -Path "d:\inetpub\ftproot\CDS\$arr[i]" ) {  
#            get-childitem d:\inetpub\ftproot\CDS\$arr[i] -name | foreach-object { processthisfile $_ }
#        }
#    }
     
#if ev* file exist
#get names of all ev files
#for each name 
#    copy to ftp area for bahar
#    ftp to dotnet cds
#    archive file
#    delete file

# check if cds have uploaded files
    logthis "-------------------------------- Start CDS processing"
    if (-not (test-path -Path "d:\inetpub\ftproot\CDS\EV*.gz")) { logthis "EV file not uploaded by CDS" "exit" }
    if (-not (test-path -Path "d:\inetpub\ftproot\CDS\SM*.gz")) { logthis "SM file not uploaded by CDS" "exit" }
    logthis "EV and SM files have been uploaded by CDS"
    
# copy files uploaded by cds to correct dir
    Copy-Item d:\inetpub\ftproot\CDS\EV*.gz d:\inetpub\ftproot\custom\bahar\cds
    if (-not( $? )) {logthis "ERROR EV file not copied to \inetpub\ftproot\custom\bahar\cds" "exit"}
    Copy-Item d:\inetpub\ftproot\CDS\SM*.gz d:\inetpub\ftproot\custom\bahar\cds
    if (-not( $? )) {logthis "ERROR SM file not copied to \inetpub\ftproot\custom\bahar\cds" "exit"}
    logthis "EV and SM files have been copied to \inetpub\ftproot\custom\bahar\cds"
    
# ftp uploaded file to dot net
    thisftp "d:\inetpub\ftproot\CDS\EV*.gz"
    thisftp "d:\inetpub\ftproot\CDS\SM*.gz"
    
# archive uploaded files
    move-Item d:\inetpub\ftproot\CDS\EV*.gz $global:basedir\archive
    if (-not( $? )) {logthis "ERROR EV file not moved to $global:basedir\archive" "exit"}
    move-Item d:\inetpub\ftproot\CDS\SM*.gz $global:basedir\archive
    if (-not( $? )) {logthis "ERROR SM file not moved to $global:basedir\archive" "exit"}
    logthis "EV and SM files have been archived to $global:basedir\archive"
    
# check if SBUr and SBCR files have been uploaded by cds
    if ((test-path -Path "d:\inetpub\ftproot\CDS\SBCR*.TXT.gz") -and (test-path -Path "d:\inetpub\ftproot\CDS\SBUR*.TXT.gz")) { 
        logthis "SBCR and SBUR files are ready for processing"
        Copy-Item d:\inetpub\ftproot\CDS\SBCR*.TXT.gz d:\inetpub\ftproot\custom\bahar\cds
        if (-not( $? )) {logthis "ERROR d:\inetpub\ftproot\CDS\SBCR*.TXT.gz not copied to \inetpub\ftproot\custom\bahar\cds" "exit"}
        Copy-Item d:\inetpub\ftproot\CDS\SBUR*.TXT.gz d:\inetpub\ftproot\custom\bahar\cds
        if (-not( $? )) {logthis "ERROR d:\inetpub\ftproot\CDS\SBUR*.TXT.gz not copied to \inetpub\ftproot\custom\bahar\cds" "exit"} 
        thisftp "d:\inetpub\ftproot\CDS\SBCR*.TXT.gz"
        thisftp "d:\inetpub\ftproot\CDS\SBUR*.TXT.gz"
        move-Item d:\inetpub\ftproot\CDS\SBCR*.TXT.gz d:\edi_applications\cds_ftp\archive
        if (-not( $? )) {logthis "ERROR d:\inetpub\ftproot\CDS\SBCR*.TXT.gz not moved to d:\edi_applications\cds_ftp\archive" "exit"}
        move-Item d:\inetpub\ftproot\CDS\SBUR*.TXT.gz d:\edi_applications\cds_ftp\archive
        if (-not( $? )) {logthis "ERROR d:\inetpub\ftproot\CDS\SBUR*.TXT.gz not moved to d:\edi_applications\cds_ftp\archive" "exit"}
        logthis "SBCR and SBUR files have been archived to $global:basedir\archive"
    }
    
#Write-Host $LASTEXITCODE
# if (-not (test-path "d:\inetpub\ftproot\CDS\EV*.gz""))
# EV7041F_121221.gz SM7031F_121221.gz SBUR_20121203.TXT.gz SBCR_20121203.TXT.gz
# (Get-Date).tostring( "yyMMdd" ) or (Get-Date).tostring( "yyyyMMdd" ) for 4 digit year   
# "C:\Program Files\CoreFTP\coreftp.exe" -OA -B -u "d:\inetpub\ftproot\CDS\EV*.gz" "ftp://cds_trans:496h5wFs@109.235.145.77/custom/bahar/cds/" 
# "C:\Program Files\CoreFTP\coreftp.exe" -OA -B -u "d:\inetpub\ftproot\CDS\SM*.gz" "ftp://cds_trans:496h5wFs@109.235.145.77/custom/bahar/cds/"
