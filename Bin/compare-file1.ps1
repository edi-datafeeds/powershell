﻿## Compare the contents of 2 files and output the difference

$COM=gc $args[0]
$NET=gc $args[1]
$diff=$args[2]
#$COM=gc 'C:\users\d.johnson\Desktop\test_com.txt'
#$NET=gc 'C:\Users\d.johnson\Desktop\test_net.txt'
$diff='C:\Users\d.johnson\Desktop\test_diff.txt'

$labelA='DOTCOM box'
$labelB='DOTNET box'

diff -ReferenceObject $COM -DifferenceObject $NET 

if ({$_.SideIndicator -eq '=>'})

        {$_.SideIndicator | ac -Value $labelA -Path C:\storage.txt}

elseif ({$_.SideIndicator -eq '<='})

         {$_.SideIndicator | ac -Value $labelB -Path C:\storage.txt}
else
    {}

#?{$_.SideIndicator -eq '=>'ac $labelA;
#} $($_.name) C:\storage.txt}
#diff -ReferenceObject $COM -DifferenceObject $NET -IncludeEqual -PassThru >> $diff
#diff -ReferenceObject $COM -DifferenceObject $NET -PassThru >> $diff
#Compare-Object -ReferenceObject $COM -DifferenceObject $NET -PassThru >> 'C:\testpass.txt'
#Compare-Object -ReferenceObject $COM -DifferenceObject $NET,(get-date).date >> $diff

#$COM
