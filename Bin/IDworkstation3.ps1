﻿############################################################################################################
####Script detects which workstation it is being executed on, results get appended to OP's index logfile####

$OFS=''
##Working output directory passed via argument
#$ndump=$args[0]
#$cat=$args[1]

##Testpath
$ndump='damartest48'
$cat="i"

##Fixed variables
#$logpath="O:\auto\logs\$ndump$inc"
$time=(get-date).hour
$sep='_'

#Get PC name
$SID=$(Get-WmiObject Win32_Computersystem).name

##Date values used
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G

##determine increment

### Webload incrementals
        IF ($ndump -eq 'WCAWebload')
            {
                switch ($time) {
                ($time -ge 12 -and $time -le 16) {$inc='_2'}
                ($time -ge 17 -and $time -le 23) {$inc='_3'}
                default {$inc='_1'}
                }
            }
        ### SMF incrementals
        ElseIF ($ndump -eq 'SMF')
            {
                switch ($time) {
                ($time -ge 12 -and $time -le 16) {$inc='_2'}
                ($time -ge 17 -and $time -le 23) {$inc='_3'}
                default {$inc='_1'}
                }
            }
        ### 123Trans incrementals
        ElseIF ($ndump -eq '123Trans') 
            {
                switch ($time) {
                ($time -ge 12 -and $time -le 14) {$inc='_1'}
                ($time -ge 17 -and $time -le 23) {$inc='_2'}
                default {$inc='_1'}
                }
            }
        ### CABTrans incrementals
            ElseIF ($ndump -eq 'CABTrans') 
            {
                switch ($time) {
                ($time -ge 11 -and $time -le 13) {$inc='_1'}
                ($time -ge 14 -and $time -le 15) {$inc='_2'}
                ($time -ge 16 -and $time -le 17) {$inc='_3'}
                ($time -ge 16 -and $time -le 17) {$inc='_3'}
                ($time -ge 18 -and $time -le 19) {$inc='_4'}
                default {$inc='_5'}
                }
                        ## Apply or do not apply incrment number
                        write-host "cuntface";$Fname=$jdate,$sep,$ndump,$inc;$logpath="O:\auto\logs\$ndump$inc"
                        }
            Else
            {
                write-host "lopsided";$Fname=$jdate,$sep,$ndump;$logpath="O:\auto\logs\$ndump"
            }

### Determine file log path
IF (!(Test-Path -Path $logpath))
    { mkdir $logpath }
Else
    {}    
##Create file or append if exist   
IF (Test-Path $logpath -include "$Fname$inc.html")
{
ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>"
}
Else
{
ac -Path "$logpath\$Fname.html" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>" -Force;
}

#Output test
$cat
Test-Path -Path $logpath
$logpath