﻿## Script to determine if WCA loader has run on schedule

#check wether cscript/loader.vbs is running
$commandline=Get-WmiObject win32_process -Filter "Name like '%wscript%'" | Select-Object CommandLine

if ($commandline -eq $null)
{
write-host 'Loader has failed to start'; #exit $LASTEXITCODE
}
#if ($commandline -contains 'O:\AUTO\Scripts\vbs\GENERIC\Loader.vbs WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i')
Elseif ($commandline.CommandLine.Contains('WCA a HAT_MS_Sqlsvr1 WcaWebload MasterSQLSVR i'))
{
write-host 'WCA Loader Successfully Initiated!'; exit 0
}
else
{
Write-Host 'WCA Loader has failed to start'; exit 2
}
#Write-Host $LASTEXITCODE
#exit $LASTEXITCODE