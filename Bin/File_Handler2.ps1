#* FileName: File_Handler.ps1
#*=============================================================================
#* Script Name: [File Handler]
#* Created: [25/06/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: All-in-one script for processing of any file types
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [25/06/2013]
#* Time: [12:00]
#* Issue: 
#* Solution:
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: OPS-Copy, OPS-Move, OPS-Tidy, OPS-Zip
# Created: [22/06/2013]
# Author: Damar Johnson
# Arguments: Source, destination, filetype, fileage
# =============================================================================
# Purpose: Copy files, Move files, Delet files, Zip files
#
#* To Copy file-    powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 clone C:\PS\Test C:\PS\Test2 txt 5 (File age i.e 5 days old)
#* To Move file-    powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 shift C:\PS\Test C:\PS\Test2 tsk 5
#* To Append file-  powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 append C:\PS\Test\[filename] C:\PS\Test2\[filename]
#* To Delete file-  powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 tidy C:\PS\Test tsk 5
#* To zip file-     powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 archive C:\PS\Test C:\PS\Test csv (File type i.e csv ot txt)

## ----- Long format ----- ##
#* To Copy file-    powershell.exe .\File_Handler.ps1 -Action clone  -Source C:\PS\Test -Dest C:\PS\Test2 -Type txt -Age 5 (File age i.e 5 days old)
#* To Move file-    powershell.exe .\File_Handler.ps1 -Action shift  -Source C:\PS\Test -Dest C:\PS\Test2 -Type tsk -Age 5
#* To Append file-  powershell.exe .\File_Handler.ps1 -Action append -Source C:\PS\Test -Dest C:\PS\Test2\File
#* To Delete file-  powershell.exe .\File_Handler.ps1 -Action tidy   -Source C:\PS\Test -Dest
#* To zip file-     powershell.exe .\File_Handler.ps1 -Action zip    -Source C:\PS\Test -Dest C:\PS\Test csv (File type i.e csv ot txt)
# =============================================================================

#param(
#[string]$action,
#[string]$source,
#[string]$dest,
#[string]$type,
#[string]$date
#)

#[CmdletBinding()]
#Param(
#[Parameter(Mandatory=$True)]
#[string]$logfile
#)

[CmdletBinding()]
Param(

[Parameter(Mandatory=$true)]
[string]$action,

[string]$source,

[Parameter(Mandatory=$true)]
[string]$dest,

[parameter(mandatory=$true)]
[string]$type,

[string]$date,

#[parameter(Mandatory=$true)]
[string]$logfile='ztest060813'

)

switch ($action){
tidy {$dest=$null}
#zip {$dest=$null}
default {[parameter(mandatory=$true)][string]$dest}
}

#$global:logfile
$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$global:wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
#$time=(get-date).hour
### Webload incrementals
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($tail) {
                       'i' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                                }
                       '' {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$global:logfile";$global:logpath='O:\auto\logs\'+"$logfile"}
                       }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
        $global:thestr = $args[0]
        $d = (Get-Date).tostring( "yyyy-MM-dd HH:mm:ss" )
        "$wdate | $thestr" | tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output "$d $thestr"
        if ($args[1] -eq "exit") {exit}
    }

#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\File_Handler.log -Append

#$action=$args[0]
#$source=$args[1]
#$dest=$args[2]
#$type=$args[3]
#$date=$args[4]

### Logging ###
#$logvalue=''

function Age ($date='0') { 
$input |?{$_.LastWriteTime.Date -le (get-date).Date.AddDays($date)}
}

#---------- testing ----------#
#$source='C:\PS\Test1'
#$dest='C:\PS\Test1\NF'
#$type='csv'
#-----------------------------#

Function OPS-Copy { #($type='zip') {
$x=Test-Path $source
switch ($x) {
    'True'
        {cd $source
        $y=Test-Path $dest
        switch ($y){
                'True' 
                    {cpi ((gci $source -Filter "*.$type" | tee -Variable item) | Age)  $dest -Force
                    $total=$item.Count
                    {Logthis 'File(s) successfully copied!'}
                        }
                'False' 
                    {logthis 'Destination specified does not exist please re-check path!'
                        }
                    }
        }
    'False' {logthis 'Source-Path specified does not exist please re-check path!'
            }
    }
}

Function OPS-Append {
$x=Test-Path $source
switch ($x){
    'True'
        {$y=Test-Path $dest
        switch ($y){
                    'True' 
                        {cat $source | ac $dest -Force;logthis 'File(s) successfully ammended!'}   
                            }
                    'False'
                        {logthis 'File to be ammended does not exist please re-check end file!'}    
                        }
        }
    'False' 
        {logthis 'Source-File specified does not exist please re-check path!'
            }
    }

Function OPS-Move { #($type='*') {
$x=Test-Path $source
switch ($x){
    'True'
        {cd $source
        $y=Test-Path $dest
        switch ($y){
                    'True' 
                        {mi ((gci $source -Filter "*.$type" | tee -Variable item) | Age) $dest -Force
                         $total=$item.Count;logthis "$total"+':File(s) successfully moved!'
                            }
                    'False'
                        {logthis 'File(s) to be moved does not exist please re-check file/path!'}    
                        }
        }
    'False' 
        {logthis 'Source-File specified does not exist please re-check file/path!'
            }
    }
}

Function OPS-Tidy { #($type='*') {
$x=Test-Path $source
switch ($x) {
    'True'{
        cd $source
        switch ($type){
        (!(test-path $type)){
            LogThis 'File type specified does not exist' #Write-Host 'File type specified does not exist'
        }
        default {
            ri ((gci $source -Filter "*.$type" | tee -Variable item) | Age) -Force
            $total=$item.Count
            logthis "$total"+':File(s) successfully deleted!'
            }
                'False' {
                        logthis 'Source-File specified does not exist please re-check file/path!'
                            }
                }
            }
    }
}

Function OPS-Zip { #($type='*'){
Set-Alias 7zip "$env:ProgramFiles\7-Zip\7z.exe"
#7zip a "-t$compress" $archive $source
#7zip a "-t$type" $archive,$source | Date #$source,$dest
#$file=(gci $source |? $_.extension -eq "*.$type" ) | Date
#7zip a "-tzip" $file,$archive | Date #$source,$dest
#switch ($type) {'' {$type='*';}} #$args[3]=$date}}
    switch ($source){
    (!(Test-Path $source)){
        LogThis 'Source-File specified does not exist please re-check file/path!'} #write-host 'Source-File specified does not exist please re-check file/path!'}
    default {
        cd $source
        $file=(gci $source | Age);$file #>> C:\PS\Test\test_exist.txt
        7zip a "-tzip" $archive,$file "*.$type" #$source,$dest
        #7zip a "-tzip" $archive,$source "*.$type" #$source,$dest
            }
}

switch ($action) {
clone {Write-Host Copying File; OPS-Copy; break}
append {Write-Host Appending to File; OPS-Append; break}
shift {Write-Host Moving File; OPS-Move; break}
tidy {Write-Host Deleting File;$type=$args[2]; OPS-Tidy; break} #$date=$args[3]; OPS-Tidy; break}
#zip {write-host Compressing File;$archive=$args[2];$source=$args[1];OPS-Zip;break}#switch ($type) { {'' {$type='*'}}; OPS-Zip;break}
zip {write-host Compressing File;$archive=$dest;OPS-Zip;break}
}

$source
$dest
#$Value
#$date
#$input
$type
#$file
$logpath
$Fname
#$logvalue
#$wdate +'|'+ $thestr
$global:thestr
$global:logvalue
#$Logfile
#$global:Logfile
#$Logfile | gm

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            { write-host No logpath found! Creating Directory; mkdir $logpath }
        Else
            {Write-Host Current logpath exists}    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                { write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$class'> file(s) successfully $action'd </span></p>"}
                Else
                { write-host Appending to file!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
}

Get-Increment
Set-Logpath

exit $LASTEXITCODE