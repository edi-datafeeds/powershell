﻿####Script detects which workstation it is being executed on, results get appended to OP's index logfile####

$OFS=''
##Working output directory passed via argument
$ndump=$args[0]
$cat=$args[1]

##Testpath
#$ndump='damartest48'
#$cat="i"

##Fixed variables
$logpath="O:\auto\logs\$ndump$inc"
$time=(get-date).hour
$sep='_'
#Get PC name
$SID=$(Get-WmiObject Win32_Computersystem).name
##Date values used
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G

##determine increment
IF ($cat -eq "i")
{
    IF ($time -ge 12 -and $time -le 14)
    {
        $inc='_2'
    }
    Elseif ($time -ge 17 -and $time -le 23)
    {
        $inc='_3'
    }
    Else 
    {
        $inc='_1'
    }

    write-host "cuntface";$Fname=$jdate,$sep,$ndump,$inc
}
Else
{
    write-host "lopsided";$Fname=$jdate,$sep,$ndump
}


##Set output path
IF (!(Test-Path -Path $logpath))
{
    mkdir $logpath
}
Else
{
}    

#$Fname=$jdate,$sep,$ndump
#$PCname >> $File
#ni -Name 'dude.txt' -itemtype "file" -Value "<p>spanclassnote<$PCname></p>"

##Create file or append if exist   
IF (Test-Path $logpath -include "$Fname$inc.html")
{
ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>"
}
Else
{
Add-Content -Path "$logpath\$Fname.html" -Value "<p><span class='note'>$wdate | Workstation: $SID</span></p>" -Force;
}

#Output test
#$cat
#Test-Path -Path $logpath