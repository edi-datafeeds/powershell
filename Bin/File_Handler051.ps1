#* FileName: File_Handler.ps1
#*=============================================================================
#* Script Name: [File Handler]
#* Created: [25/06/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: All-in-one script for processing any file types
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [25/06/2013]
#* Time: [12:00]
#* Issue: 
#* Solution:
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: OPS-Copy, OPS-Move, OPS-Tidy, OPS-Zip
# Created: [22/06/2013]
# Author: Damar Johnson
# Arguments: Source, destination, filetype, fileage
# =============================================================================
# Purpose: Copy files, Move files, Delet files, Zip files
#
### ----- Short format ----- ##
#* To Copy file-    powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 clone C:\PS\Test C:\PS\Test2 txt 5 (File age i.e 5 days old)
#* To Move file-    powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 shift C:\PS\Test C:\PS\Test2 tsk 5
#* To Append file-  powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 append C:\PS\Test\[filename] C:\PS\Test2\[filename]
#* To Delete file-  powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 tidy C:\PS\Test tsk 5
#* To zip file-     powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 zip C:\PS\Test C:\PS\Test csv (File type i.e csv ot txt)

### ----- Long format ----- ##
#* To Copy file-    powershell.exe .\File_Handler.ps1 -Action clone  -Source C:\PS\Test -Dest C:\PS\Test2 -Type txt -type txt -Age 5
#* To Move file-    powershell.exe .\File_Handler.ps1 -Action shift  -Source C:\PS\Test -Dest C:\PS\Test2 -Type tsk -type txt -Age 5
#* To Append file-  powershell.exe .\File_Handler.ps1 -Action append -Source C:\PS\Test\FileA -Dest C:\PS\Test2\FileB -type txt
#* To Delete file-  powershell.exe .\File_Handler.ps1 -Action tidy   -Source C:\PS\Test -type txt
#* To zip file-     powershell.exe .\File_Handler.ps1 -Action zip    -Source C:\PS\Test -Dest C:\PS\Test -type csv
# =============================================================================


[CmdletBinding()]
Param(

[Parameter(
ParametersetName='clone',Mandatory=$true,Position=0
)]
[string]$clone,

[Parameter(
ParametersetName='clone',Mandatory=$true,Position=1
)]
[string]$sourceA,

[Parameter(
ParametersetName='clone',Mandatory=$true,Position=2
)]
[string]$destA,

[Parameter(
ParametersetName='clone',Mandatory=$true,Position=3
)]
[string]$typeA,


[Parameter(
ParametersetName='append',Mandatory=$true,Position=0
)]
[string]$append,

[Parameter(
ParametersetName='append',Mandatory=$true,Position=1
)]
[string]$sourceB,

[Parameter(
ParametersetName='append',Mandatory=$true,Position=2
)]
[string]$destB,

[Parameter(
ParametersetName='append',Mandatory=$true,Position=3
)]
[string]$typeB,


[Parameter(
ParametersetName='shift',Mandatory=$true,Position=0
)]
[string]$shift,

[Parameter(
ParametersetName='shift',Mandatory=$true,Position=1
)]
[string]$sourceC,

[Parameter(
ParametersetName='shift',Mandatory=$true,Position=2
)]
[string]$destC,

[Parameter(
ParametersetName='shift',Mandatory=$true,Position=3
)]
[string]$typeC,


[parameter(
ParametersetName='tidy',mandatory=$true,Position=0
)]
[string]$tidy,

[Parameter(
ParametersetName='tidy',Mandatory=$true,Position=1
)]
[string]$sourceD,

[Parameter(
ParametersetName='tidy',Mandatory=$false,Position=2
)]
[string]$destD,

[Parameter(
ParametersetName='tidy',Mandatory=$true,Position=3
)]
[string]$typeD,


[Parameter(
ParametersetName='zip',mandatory=$true,Position=0
)]
[DateTime]$zip,

[Parameter(
ParametersetName='zip',Mandatory=$true,Position=1
)]
[string]$sourceE,

[Parameter(
ParametersetName='zip',Mandatory=$true,Position=2
)]
[string]$destE,

[Parameter(
ParametersetName='zip',Mandatory=$true,Position=3
)]
[string]$typeE

)

#switch ($PSCmdlet.ParameterSetName){
    #clone {
        #Write-Host Copying File; OPS-Copy; break
        #}
    #append {
        #Write-Host Appending to File A; OPS-Append; break
        #}
    #shift {
        #Write-Host Moving File; OPS-Move; break
        #}
    #tidy {
        #Write-Host Deleting File; OPS-Tidy; break
        #} 
    #zip {
        #write-host Compressing File;$archive=$dest;OPS-Zip;break
        #}
    #}


    #switch ($PSCmdlet.ParameterSetName){
    #"clone" {}
    #"append" {}
    #}


#$global:logfile
$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($tail) {
                       'i' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc" ;write-host inc exists!;
                                }
                       '' {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$global:logfile";$global:logpath='O:\auto\logs\'+"$logfile"}
                       }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt#=$args[0]
    )
        #$global:thestr = $args[0]
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output "$d $displaytxt"
        if ($args[1] -eq "exit") {exit}
    }

#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\File_Handler.log -Append

function Age ($date='0') { 
$input |?{$_.LastWriteTime.Date -le (get-date).Date.AddDays($date)}
}

#---------- testing ----------#
#$source='C:\PS\Test1'
#$dest='C:\PS\Test1\NF'
#$type='csv'
#-----------------------------#

    Function OPS-Copy { #($type='zip') {
    $x=Test-Path $source
        switch ($x) {
                'True'
                    {cd $source
                    $y=Test-Path $dest
                    switch ($y){
                            'True' 
                                {switch ($x){
                                (!(test-path "$source\*.$type")){
                                    LogThis -displaytxt 'File type specified does not exist' #Write-Host 'File type specified does not exist'
                                        [System.Windows.Forms.MessageBox]::Show("File type specified does not exist",'Error')#;exit 1
                                        $global:class='failed'
                                    }
                                default {
                                cpi ((gci $source -Filter "*.$type" | tee -Variable item) | Age)  $dest -Force
                                $total=$item.Count
                                Logthis -displaytxt "$total`: File(s) successfully copied!"
                                    }
                            }
                                }
                            'False' 
                                {logthis -displaytxt 'Destination path does not exist please re-check path!'
                                    [System.Windows.Forms.MessageBox]::Show("Destination path does not exist please re-check path!",'Error',0)
                                    $global:class='failed'
                                    }
                        }
                    }
                'False' {logthis -displaytxt 'Source path does not exist please re-check path!'
                            [System.Windows.Forms.MessageBox]::Show("Source Path does not exist please re-check path!",'Error')
                            $global:class='failed'
                        }
        }
    }

    Function OPS-Append {
    $x=Test-Path $source
        switch ($x){
            'True'
                {$y=Test-Path $dest
                switch ($y){
                            'True' 
                                {cat $source | ac $dest -Force;logthis -displaytxt 'File(s) successfully ammended!'}   
                                    }
                            'False'
                                {logthis -displaytxt 'File to be ammended does not exist please re-check end file!'}    
                                    [System.Windows.Forms.MessageBox]::Show("File to be ammended does not exist please re-check end file!",'Error')
                                    $global:class='failed'
                                }
                }
            'False' 
                {logthis -displaytxt 'Source file does not exist please re-check path!'
                    [System.Windows.Forms.MessageBox]::Show("File to be ammended does not exist please re-check end file!",'Error')
                    $global:class='failed'
                    }
            }

    Function OPS-Move { #($type='*') {
    $x=Test-Path $source
        switch ($x){
            'True'
                {cd $source
                $y=Test-Path $dest
                switch ($y){
                            'True' 
                                {mi ((gci $source -Filter "*.$type" | tee -Variable item) | Age) $dest -Force
                                 $total=$item.Count;logthis ("$total`: File(s) successfully moved!")
                                    }
                            'False'
                                {logthis -displaytxt 'File(s) to be moved does not exist please re-check file/path!'    
                                    [System.Windows.Forms.MessageBox]::Show("File(s) to be moved does not exist please re-check file/path!",'Error')
                                    }
}
                }
            'False' 
                {logthis -displaytxt 'Source file path does not exist please re-check file/path!'
                    [System.Windows.Forms.MessageBox]::Show("Source file path does not exist please re-check file/path!",'Error')
                        }
            }
        }

    Function OPS-Tidy { #($type='*') {
    $x=Test-Path $source
        switch ($x) {
            'True'{
                cd $source
                switch ($type){
                (!(test-path $type)){
                    LogThis -displaytxt 'File type specified does not exist' #Write-Host 'File type specified does not exist'
                    [System.Windows.Forms.MessageBox]::Show("File type specified does not exist",'Error')
                    $global:class='failed'
                            }
                        default {
                            ri ((gci $source -Filter "*.$type" | tee -Variable item) | Age) -Force
                            $total=$item.Count
                            logthis ("$total`: File(s) successfully deleted!")
                            }
            'False' {
                    Logthis -displaytxt 'Source path specified does not exist please re-check file/path!'
                        [System.Windows.Forms.MessageBox]::Show("Source-File specified does not exist please re-check file/path!",'Error')
                        $global:class='failed'
                            }
                        }
                    }
            }
        }

    Function OPS-Zip { #($type='*'){
    Set-Alias 7zip "$env:ProgramFiles\7-Zip\7z.exe"
        switch ($source){
        (!(Test-Path $source)){
            LogThis -displaytxt 'Source file specified does not exist please re-check file/path!' #write-host 'Source-File specified does not exist please re-check file/path!'}
                [System.Windows.Forms.MessageBox]::Show("Source file specified does not exist please re-check file/path!",'Error')
                $global:class='failed'
                    }
        default {
            cd $source
            $file=(gci $source | Age);$file #>> C:\PS\Test\test_exist.txt
            $total=$item.Count
                switch ($type){
                (!(test-path $type)){
                    LogThis -displaytxt 'File type specified does not exist' #Write-Host 'File type specified does not exist'
                        [System.Windows.Forms.MessageBox]::Show("File type specified does not exist",'Error')
                        $global:class='failed'
                    }
                default{
                    7zip a "-tzip" $archive,$file "*.$type" #$source,$dest
                    LogThis -displaytxt ("$total`: File(s) successfully zipped")
                            }
                        }
                }
            }
    }

    switch ($action){
        clone {[Parameter(mandatory=$true,Position=0
            )]
            [string]$action
            [Parameter(mandatory=$true,Position=1
            )]
            [string]$source
            [Parameter(mandatory=$true,Position=2
            )]
            [string]$dest
            [Parameter(mandatory=$true,Position=3
            )]
            [string]$type
            [Parameter(mandatory=$true,Position=4
            )]
            [Parameter(mandatory=$true,Position=5
            )]
            [DateTime]$date
            #$dest=$null
                Write-Host Copying File; OPS-Copy; break
            }
        append {[Parameter(mandatory=$true,Position=0
            )]
            [string]$action
            [Parameter(mandatory=$true,Position=1
            )]
            [string]$source
            [Parameter(mandatory=$true,Position=2
            )]
            [string]$dest
            [Parameter(mandatory=$true,Position=3
            )]
            [string]$type
            [Parameter(mandatory=$true,Position=4
            )]
            [Parameter(mandatory=$true,Position=5
            )]
            [DateTime]$date
                Write-Host Appending to File A; OPS-Append; break
            }
        shift {[Parameter(mandatory=$true,Position=0
            )]
            [string]$action
            [Parameter(mandatory=$true,Position=1
            )]
            [string]$source
            [Parameter(mandatory=$true,Position=2
            )]
            [string]$dest
            [Parameter(mandatory=$true,Position=3
            )]
            [string]$type
            [Parameter(mandatory=$true,Position=4
            )]
            [Parameter(mandatory=$true,Position=5
            )]
            [DateTime]$date
                Write-Host Moving File; OPS-Move; break
            }
        tidy {[Parameter(mandatory=$true,Position=0
            )]
            [string]$action
            [Parameter(mandatory=$true,Position=1
            )]
            [string]$source
            [Parameter(mandatory=$true,Position=2
            )]
            [string]$dest
            [Parameter(mandatory=$false,Position=3
            )]
            [string]$type
            [Parameter(mandatory=$true,Position=4
            )]
            [Parameter(mandatory=$true,Position=5
            )]
            [DateTime]$date
                Write-Host Deleting File;$type=$args[2]; OPS-Tidy; break
            }
        zip {[Parameter(mandatory=$true,Position=0
            )]
            [string]$action
            [Parameter(mandatory=$true,Position=1
            )]
            [string]$source
            [Parameter(mandatory=$true,Position=2
            )]
            [string]$dest
            [Parameter(mandatory=$true,Position=3
            )]
            [string]$type
            [Parameter(mandatory=$true,Position=4
            )]
            [Parameter(mandatory=$false,Position=5
            )]
            [DateTime]$date
                write-host Compressing File;$archive=$dest;OPS-Zip;break
            }
    #default {[parameter(mandatory=$true)][string]$dest}
    }
    #switch ($action) {
    #clone {
        #Write-Host Copying File; OPS-Copy; break
        #}
    #append {
        #Write-Host Appending to File A; OPS-Append; break
        #}
    #shift {
        #Write-Host Moving File; OPS-Move; break
        #}
    #tidy {
        #Write-Host Deleting File;$type=$args[2]; OPS-Tidy; break
        #} #$date=$args[3]; OPS-Tidy; break}
    #zip {write-host Compressing File;$archive=$args[2];$source=$args[1];OPS-Zip;break}#switch ($type) { {'' {$type='*'}}; OPS-Zip;break}
    #zip {
        #write-host Compressing File;$archive=$dest;OPS-Zip;break
        #}
    #}

$source
$dest
#$Value
#$date
#$input
$type
#$file
$logpath
$Fname
#$logvalue
#$wdate +'|'+ $thestr
#$global:thestr
$global:logvalue
$displaytxt
$class
#$Logfile
#$global:Logfile
#$Logfile | gm

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

Get-Increment
Set-Logpath

exit $LASTEXITCODE