﻿$OFS='_'

#Working output dir 
#$ndump=$args[0]
$ndump='damartest11'
#$Tfile=$args[1]
$logpath="O:\auto\logs\$ndump"

#Set output path
IF (!(Test-Path -Path $logpath))
{
    mkdir $logpath
}
Else
{
}    
 
cd $logpath

#Test output dir
#$dest="C:\Users\d.johnson\Desktop\dest\name.txt"

#Get PC name
$PCname=$(Get-WmiObject Win32_Computersystem).name

#Todays Date & Time
$Date=get-date -Format yyyyMMdd

#$File | Rename-Item -NewName {$_.name + '.html'}
#$File=$File + '.html'
$Filename=$Date,$ndump

#$PCname >> $File

#ni -Name 'dude.txt' -itemtype "file" -Value "<p>spanclassnote<$PCname></p>"

IF (!(Test-Path $logpath -include $Filename))
{
ni -Name "$Filename.html" -itemtype "file" -Value "<p>spanclassnote<$PCname></p>"
}
Else
{
Add-Content -Path $Filename -Value $PCname
}