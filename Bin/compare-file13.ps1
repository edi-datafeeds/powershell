﻿## Compare the contents of 2 files and output the difference

#$COM=gc $args[0]
#$NET=gc $args[1]
#$diff=$args[2]
$COM=gc 'C:\users\d.johnson\Desktop\test_com.txt'
$NET=gc 'C:\Users\d.johnson\Desktop\test_net.txt'
$diff='C:\Users\d.johnson\Desktop\test_diff.txt'
$diff3='C:\Users\d.johnson\Desktop\test_diff3.txt'
$diff4='C:\Users\d.johnson\Desktop\test_diff4.txt'
$diff5='C:\Users\d.johnson\Desktop\test_diff5.txt'

$Head=$COM[0,1]
$labelA='EXTRA on .NET'
$labelB='MISSING from .NET' 


## Output and Capture difference
$diff0=diff -ReferenceObject $NET -DifferenceObject $COM;
diff -ReferenceObject $NET -DifferenceObject $COM -PassThru > $diff3
$gold= gc $diff3
$neutro=$diff0[0..($diff0.Count)]

## Capture file differences and edit file properties
$kane=%{$neutro} | ?{$_.SideIndicator -eq '<='};
        %{$kane} | add-member -MemberType NoteProperty -Name Inconsistency -Value $labelA

$able=%{$neutro} | ?{$_.SideIndicator -eq '=>'};
        %{$able} | add-member -MemberType NoteProperty -Name Inconsistency -Value $labelB


#$neutro | ft -Property Inputobject,Inconsistency -AutoSize
$neutro | ft -Property LoginID,Dir,VirtualPath,Inconsistency -AutoSize

$neutro | %{$_.LoginID=($_.InputObject).split("`t")[0]; $_.Dir=($_.InputObject).split("`t")[1]; $_.VirtualPath=($_.InputObject).split("`t")[2]}
$neutro | export-csv -notypeinformation C:\Users\d.johnson\Desktop\test_diff5.txt

date
