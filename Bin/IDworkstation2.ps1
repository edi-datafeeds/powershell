﻿#* FileName: IDworkstation.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose:  Detects which workstation task file is being executed on, results get appended to OP's index logfile
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [25/06/2013]
#* Time: [12:00]
#* Issue: Increments aside from webload WCA not being accounted for
#* Solution: Add switches for other increments .i.e SMF
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Determine which incrment to apply
#
#
# =============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Create logpath + logfile
#
#
# =============================================================================

### Logs step and output of entire script
#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

$OFS=''
##Working output directory passed via argument
#$ndump=$args[0]
#$tail=$args[1]

##Testpath
$ndump='damartest52'
$cat="i"
$tai="i"

##Fixed variables
#$logpath="O:\auto\logs\$ndump$inc"
$time=(get-date).hour
$sep='_'

#Get PC name
$SID=$(Get-WmiObject Win32_Computersystem).name

##Date values used
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$vallue="$wdate | Workstation: $SID"

############################## Determine Increment ##############################
Function Get-Increment {
#$time=(get-date).hour
#$ndump=$args[0]
#$tail=$args[1]
### Webload incrementals
       switch ($ndump) {
    'WCAWebload'
            {switch ($time) {
                ( $_ -ge 12 -and $_ -le 16) {$inc='_2'}
                ($_ -ge 17 -and $_ -le 23) {$inc='_3'}
                default {$inc='_1'}
                }
            }
    ### SMF incrementals
    'SMF'
        {switch ($time) {
            ($_ -ge 12 -and $_ -le 16) {$inc='_2'}
            ($_ -ge 17 -and $_ -le 23) {$inc='_3'}
            default {$inc='_1'}
            }
            }
    ### 123Trans incrementals
    '123Trans' 
        {switch ($time) {
            ($_ -ge 12 -and $_ -le 14) {$inc='_1'}
            ($_ -ge 17 -and $_ -le 23) {$inc='_2'}
            default {$inc='_1'}
            }
        }
    ### CABTrans incrementals
    'CABTrans' 
        {switch ($time) {
            ($_ -ge 11 -and $_ -le 13) {$inc='_1'}
            ($_ -ge 14 -and $_ -le 15) {$inc='_2'}
            ($_ -ge 16 -and $_ -le 17) {$inc='_3'}
            ($_ -ge 18 -and $_ -le 19) {$inc='_4'}
            default {$inc='_5'}
            }
        }
}
    If (!($tail -eq $null)) {
    ### Output incrment number
    write-host "Incremental steps applied!";$Fname=$jdate,$sep,$ndump,$inc;$logpath="O:\auto\logs\$ndump$inc"
    }
    Else
        {write-host "lopsided: procceding with no incrment!";$Fname=$jdate,$sep,$ndump;$logpath="O:\auto\logs\$ndump"}
}
############################## Determine/Set file log path ##############################        
Function Set-Logpath {
### Determine file log path
        IF (!(Test-Path -Path $logpath))
            { mkdir $logpath }
        Else
            {}    
                ##Create file or append if exist   
                IF (Test-Path $logpath -include "$Fname$inc.html")
                    {ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<p><span class='note'>$value</span></p>"}
                Else
                    {ac -Path "$logpath\$Fname.html" -Value "<p><span class='note'>$vallue</span></p>" -Force;}
}

#Stop-Transcript