﻿## Script to determine if WCA loader has run on schedule

#check wether cscript/loader.vbs is running
$commandline=Get-WmiObject win32_process -Filter "Name like '%wscript%'" | Select-Object CommandLine

if ($commandline -contains 'O:\AUTO\Scripts\vbs\GENERIC\Loader.vbs')
{
write-host 'Loader initiated'; exit $LASTEXITCODE
}
else
{
Write-Host 'Loader has failed to start'; exit $LASTEXITCODE
}
#Write-Host $LASTEXITCODE
#exit $LASTEXITCODE