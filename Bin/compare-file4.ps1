﻿## Compare the contents of 2 files and output the difference

#$COM=gc $args[0]
#$NET=gc $args[1]
#$diff=$args[2]
$COM=gc 'C:\users\d.johnson\Desktop\test_com.txt'
$NET=gc 'C:\Users\d.johnson\Desktop\test_net.txt'
$diff='C:\Users\d.johnson\Desktop\test_diff.txt'
$diff3='C:\Users\d.johnson\Desktop\test_diff3.txt'

$labelA='DOTCOM box'
$labelB='DOTNET box'

$diff0=diff -ReferenceObject $COM -DifferenceObject $NET;
$gold= $diff0[0..($diff0.Count)]

if ({$_.SideIndicator -eq '=>'})

         {$diff0 | %{add-member -InputObject $gold -MemberType NoteProperty -Name Sourcebox -Value $labelA}}
        #{add-member -MemberType AliasProperty -InputObject $diff0 -Name 'Sourcebox' -Value 'putin'}
        #{Format-Table -Property name, Source}

elseif ({$_.SideIndicator -eq '<='})

         {$diff0 | %{add-member -InputObject $gold -MemberType NoteProperty -Name Sourcebox -Value $labelB}}
         #{add-member -MemberType NoteProperty -InputObject $diff0 -Name 'Sourcebox' -Value 'putin'}
         #{$NET | %{add-member -Name 'Sourcebox'};$_.Sourcebox=$labelB}
         #{$_.SideIndicator | ac -Value $labelB -Path C:\storage.txt}
else
    {}

#$diff0=diff -ReferenceObject $COM -DifferenceObject $NET;
$diff0 | Format-Table -Property $_.SideIndicator#, $_.Sourcebox

#?{$_.SideIndicator -eq '=>'ac $labelA;
#} $($_.name) C:\storage.txt}
#diff -ReferenceObject $COM -DifferenceObject $NET -IncludeEqual -PassThru >> $diff
#diff -ReferenceObject $COM -DifferenceObject $NET -PassThru >> $diff
#Compare-Object -ReferenceObject $COM -DifferenceObject $NET -PassThru >> 'C:\testpass.txt'
#Compare-Object -ReferenceObject $COM -DifferenceObject $NET,(get-date).date >> $diff

#$COM



date