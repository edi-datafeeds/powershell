﻿
### 261582 records found
$raw_content=gc 'c:\RAW\all_20130802.feeds'

### 4788 records found
$value3=%{
    $raw_content | ?{$_ -notlike '*Prices*'} | 
                        ?{$_ -notlike '*.p13*'} | 
                        ?{$_ -notlike '*.p14*'}
}

$value3 | Out-File 'c:\OPS\outfile_0808.csv'
$value3= ipcsv -Path 'c:\OPS\outfile_0808.csv' -Header Username,Remote,Filename,MOD_Date -Delimiter "`t"
$value3 | epcsv -NoTypeInformation 'c:\RAW\dbnoprices_0808.csv'

### 3047 records found - No s
$value4=%{
    $raw_content | ?{$_ -notlike '*Prices*'} | 
                   ?{$_ -notlike '*.p13*'} | 
                   ?{$_ -notlike '*.p14*'} | 
                   ?{$_ -notlike '*.pdf*'} | 
                   ?{$_ -notlike '*samples*'}
}
#-------------------------------------- Current #---------------------------------------#
$value4 | Out-File 'c:\OPS\outval4_0908.csv'
$value4= ipcsv -Path 'c:\OPS\outval4_0908.csv' -Header Username,Remote,Filename,MOD_Date -Delimiter "`t"

$test04=%{
    $value4.filename -replace "[0-9][0-9][0-9][0-3][0-9][0-9][0-9][0-9]",'YYYYMMDD' #| %{$_ -replace "2012[0-9][0-9][0-9][0-9]",'YYYYMMDD'}
}

$genericname=$test04 -replace '[0-9][2-3][0-20][0-9[0-9][0-9].','YYMMDD' | 
                    %{$_ -replace '13[0-20][0-9[0-9][0-9].','YYMMDD'} | 
                    #%{$_ -replace '[2012]-[0-9][0-9]-[0-9][0-9].','YYYY-MM-DD'} |
                    %{$_ -replace '[0-9][0-9][0-9][2-3]-[0-9][0-9]-[0-9][0-9]','YYYY-MM-DD'}

$glist=@()

for ($i=0; $i -lt $value4.Count; $i++){

$glist+= "" | Select-Object -Property @{label='Username'; expression={$value4.username[$i]}},@{label='Remote'; expression={$value4.Remote[$i]}},@{label='Filename'; expression={$genericname[$i]}},@{label='Mod Date'; expression={$value4.MOD_Date[$i]}}
}

$glist | epcsv -NoTypeInformation 'c:\RAW\generic_0908.csv'
#---------------------------------------------------------------------------------------#

$genericname=$null

$newlist= 'c:\RAW\generic_0908.csv' | ipcsv -header Username,Remote,Filename

$glist2= 'c:\RAW\generic_0908.csv' | ipcsv -header Username,Remote,Filename

$glist | Get-Unique
($glist.filename | Get-Unique).count #1012
($glist.username | Get-Unique).count #100
($glist.remote | Get-Unique).count #188